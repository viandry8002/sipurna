import React from 'react'
import { StyleSheet } from 'react-native'
import colors from '../Style/Colors';

const Styles = StyleSheet.create({
    container: {
        flex: 1
    },
    image: {
        flex: 1,
        flexDirection:'column',
        marginTop:200
      },
    font:{
        fontSize:16,
        marginVertical:5,
        marginHorizontal:10,
        width:'90%'
    },
    // font:{
    //     fontSize:16,
    //     marginTop:5,
    //     paddingLeft:15
    // },
    contact:{
        flexDirection:'row',
        backgroundColor:'white',
        width:'100%',
        height:45,
        borderRadius:10,
        padding:5,
        marginBottom:10
    },
    contact2:{
        width:'95%',
        flexDirection:'row',
        backgroundColor:'white',
        borderRadius:10,
        padding:10,
        marginHorizontal:20,
        marginBottom:10,
        alignSelf:'center'
    },
    // contact2:{
    //     flexDirection:'row',
    //     backgroundColor:'white',
    //     width:'100%',
    //     height:70,
    //     borderRadius:10,
    //     padding:10,
    //     marginBottom:10
    // },
    contact3:{
        flexDirection:'row',
        backgroundColor:'white',
        width:'100%',
        height:100,
        borderRadius:10,
        padding:15,
        marginBottom:10
    },
    contact4:{
        flexDirection:'row',
        backgroundColor:'white',
        width:'100%',
        height:100,
        borderRadius:10,
        padding:18,
        marginBottom:10
    },
    ImageIcon2:{
        width:30,
        height:30,
        marginTop:5
    },
    ImageIcon:{
        width:30,
        height:30
    },
    cs:{
        width:120,
        height:150,
        marginVertical:20
    }
});

export default Styles