import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View, ActivityIndicator,Alert } from 'react-native';
import HTML from "react-native-render-html";
import api from '../Api';
   
const Berita = ({navigation}) => {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(false);

      useEffect(() => {
          async function getToken() {
              try{
                  const token = await AsyncStorage.getItem("api_token")
                  return getVenue(token)
              }catch(err){
                  console.log(err)
              }
          }
          getToken()
      },[])

      const getVenue = (token) =>{
        setLoading(true);
        setTimeout(() => {

          Axios.get(`${api}/api/activity/all`,{
              headers : {
                  'Authorization' : 'Bearer ' + token,
                  'Accept' : 'application/json'
              }
          }).then((res) =>{
              setData(res.data.data)
              setLoading(false);

          }).catch((err) =>{
            console.log(err.response.data)
            if(err.response.data === 'Unauthorized.'){
                navigation.navigate('OnKick')
              }
          })

        }, 100);
      }

    const Dtl = (route) => {
        navigation.navigate('DtlBerita',route)
      }

    const renderItem = ({item}) => (
        <View style={styles.item}>
        <Image source={item.file === '' ? require('../Assets/drawable-xhdpi/img-notfound.png')
                : {uri: item.file}} style={{width: 140,height: 140,borderRadius:10, marginHorizontal:10,marginTop:10}} />
        <View style={styles.containerflatList}>
        <Text style={styles.title1}>{item.title}</Text>
        <Text style={{paddingBottom:10, fontSize:16,color:'#2C5BA4'}}>{item.type.toUpperCase()}</Text>
        <HTML source={{ html: item.description === '' ? '-' : item.description }} renderers={{
          p: (_, children) => <Text numberOfLines={1} key={item.key}>{children}</Text>,
        }} />
        <TouchableOpacity style={{alignItems:'center',marginVertical:10}} onPress={() => Dtl(item)}>
          <Text style={styles.detail}>Detail</Text>
        </TouchableOpacity>
        </View>
        </View>
    );

    return (
        <SafeAreaView style={styles.container}>
         <View style={{flex:1,flexDirection:'column',justifyContent: 'space-between'}}>

         {loading ? (
          <ActivityIndicator
            visible={loading}
            textContent={'Loading...'}
            style={{alignSelf:'center',marginTop:'50%',position:'absolute'}}
            size="large" 
            color="#2C5BA4"
          />
        ) : (
          <>
         <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
          />
           </>
        )}

         </View>
     </SafeAreaView>
    )
}

export default Berita

const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
      item: {
        backgroundColor: 'white',
        padding: 10,
        marginVertical: 3,
        flexDirection : 'row',
        flex: 1
      },
      title1: {
        fontSize: 16,
        paddingVertical:10
        // fontFamily:'Montserrat'
      },
      containerflatList : {
        flexDirection : 'column',
        flex: 1,
        margin:5
      },
      detail: {
        fontSize: 12,
        color: '#669F16'
      },
})

