import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View, ActivityIndicator, ScrollView, ToastAndroid, Alert } from 'react-native';
import DatePicker from 'react-native-datepicker';
import { Button, Input } from 'react-native-elements';
import DocumentPicker from 'react-native-document-picker';
import api from '../Api';

const FormKorlap = ({ navigation }) => {
  const [data, setData] = useState()
  const [arrayholder, setArrayholder] = useState('')
  const [Tokens, setTokens] = useState('')
  const [loading, setLoading] = useState(false);
  const [colorFile, setColorFile] = useState('#E3E3E3')
  const [btnTouch, setbtnTouch] = useState(false)

  const [form, setForm] = useState({
    npp: '',
    judul: '',
    tanggal: '',
    tempat: '',
    file: ''
  })

  useEffect(() => {

    async function getToken() {
      try {
        const token = await AsyncStorage.getItem("api_token")
        const onpp = await AsyncStorage.getItem("npp")
        return getForm(onpp, token)
      } catch (err) {
        console.log(err)
      }
    }
    getToken()
  }, []);

  const getForm = (onpp, ptoken) => {
    setArrayholder('DATA1')
    Tokens === '' ? setTokens(ptoken) : setTokens(Tokens)
    setForm({
      ...form,
      ['npp']: onpp,
    })
  }

  const getHistori = () => {
    setLoading(true);
    setTimeout(() => {

      Axios.get(`https://sipurna.com/aplikasi/api/ykp/korlap`, {
        headers: {
          // 'Authorization' : 'Bearer ' + Tokens,
          'Accept': 'application/json',
        }
      }).then((res) => {
        setArrayholder('DATA2')
        setData(res.data)
        console.log(res.data)
        setLoading(false);
        // if(res.data){

        // }

      }).catch((err) => {
        console.log(err.response.data)
        if (err.response.data === 'Unauthorized.') {
          navigation.navigate('OnKick')
        }
      })

    }, 100);
  }

  const OpenDocumentFile = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      }).then((res) => {
        let tmpFiles = {
          uri: res.uri,
          type: res.type,
          name: res.name
        }

        console.log(tmpFiles);
        onInputChange(tmpFiles, 'file')
      })

      setColorFile('#2C5BA4')
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  }

  const showToast = () => {
    ToastAndroid.show("Sukses Mengirim Data!", ToastAndroid.LONG);
  };

  const onInputChange = (value, input) => {
    setForm({
      ...form,
      [input]: value,
    })
  }

  const SendForm = () => {
    setbtnTouch(true)
    let formdata = new FormData();

    formdata.append("npp", form.npp)
    formdata.append("judul", form.judul)
    formdata.append("tanggal", form.tanggal)
    formdata.append("tempat", form.tempat)
    formdata.append("file", form.file)

    if (form.npp !== '' && form.judul !== '' && form.tanggal !== '' && form.tempat !== '' && form.file !== '') {
      Axios.post(`https://sipurna.com/aplikasi/api/ykp/korlap`, formdata, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data'
        }
      }).then((res) => {

        console.log('success', res.data)
        showToast()
        setForm('')
        setColorFile('#E3E3E3')
        navigation.navigate('Home')
        setbtnTouch(false)
      }).catch((err) => {
        console.log('gagal', err)
      })
    } else {
      Alert.alert('Peringatan',
        'Form input tidak boleh kosong',
        [{
          text: 'Ya',
          onPress: () => console.log('Tidak')
        }])
    }
  }

  const renderItem = ({ item }) => (
    <View style={styles.item}>
      {/* <Image source={item.file === null ? require('../Assets/drawable-xhdpi/img-notfound.png')
                : {uri: item.file}} style={{width: 123,height: 154, marginHorizontal:10,marginTop:10}} /> */}
      <Image source={require('../Assets/drawable-xhdpi/img-notfound.png')} style={{ width: 123, height: 154, marginHorizontal: 10, marginTop: 10 }} />
      <View style={styles.containerflatList}>
        <Text style={styles.title1}>Judul : {item.judul}</Text>
        <Text style={styles.title1}>Tanggal : {item.tanggal}</Text>
        <Text style={styles.title1}>Tempat : {item.tempat}</Text>
      </View>
    </View>
  );

  var typ = arrayholder == 'DATA1' ? "solid" : "outline"
  var ttl = arrayholder == 'DATA1' ? { color: 'white' } : { color: 'black' }
  var btn = arrayholder == 'DATA1' ? { backgroundColor: '#2C5BA4', paddingHorizontal: 50 } : { paddingHorizontal: 50 }

  var typ1 = arrayholder == 'DATA2' ? "solid" : "outline"
  var ttl1 = arrayholder == 'DATA2' ? { color: 'white' } : { color: 'black' }
  var btn1 = arrayholder == 'DATA2' ? { backgroundColor: '#2C5BA4', paddingHorizontal: 50 } : { paddingHorizontal: 50 }

  return (
    <SafeAreaView style={styles.container}>
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View style={styles.item1}>

          <Button title="Form"
            type={typ}
            titleStyle={ttl}
            buttonStyle={btn}
            onPress={() => { getForm() }} />

          <Button title="Histori"
            type={typ1}
            titleStyle={ttl1}
            buttonStyle={btn1}
            onPress={() => { getHistori() }} />
        </View>

        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between', backgroundColor: 'white' }}>

          {loading ? (
            <ActivityIndicator
              visible={loading}
              textContent={'Loading...'}
              style={{ alignSelf: 'center', marginTop: '40%', position: 'absolute' }}
              size="large"
              color="#2C5BA4"
            />
          ) : (
            arrayholder === 'DATA2' ?
              (<>
                <FlatList
                  data={data}
                  renderItem={renderItem}
                  keyExtractor={item => item.id.toString()}
                />
              </>)
              :
              (
                <ScrollView style={[styles.container, { backgroundColor: 'white' }]}>
                  <View style={{ margin: 20 }}>

                    <View >
                      <Text>Judul Peristiwa</Text>
                      <View style={styles.wrapInput}>
                        <Input
                          inputContainerStyle={{ borderBottomWidth: 0 }}
                          style={{ padding: 10 }}
                          value={form.judul}
                          onChangeText={value => onInputChange(value, 'judul')}

                        />
                      </View>
                    </View>

                    <View >
                      <Text>Tanggal Peristiwa</Text>
                      <View style={styles.wrapInput}>
                        <DatePicker
                          style={{ width: '97%', marginTop: 5 }}
                          // date={date}
                          date={form.tanggal}
                          mode="date"
                          placeholder="select date"
                          format="YYYY-MM-DD"
                          minDate="2020-01-01"
                          maxDate="2050-01-01"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          // iconComponent={<MaterialIcons name={'calendar-today'} size={20} color={colors.grey}/>}

                          customStyles={{
                            dateInput: {
                              borderWidth: 0,
                            },
                            // ... You can check the source to find the other keys.
                          }}
                          // onDateChange={(date) => setDate(date)}
                          onDateChange={value => onInputChange(value, 'tanggal')}

                        />
                      </View>
                    </View>

                    <View >
                      <Text>Uraian Peristiwa</Text>
                      <View style={styles.wrapInput}>
                        <Input
                          inputContainerStyle={{ borderBottomWidth: 0 }}
                          style={{ padding: 10 }}
                          value={form.tempat}
                          onChangeText={value => onInputChange(value, 'tempat')}
                        />
                      </View>
                    </View>

                    <View>
                      <Text>Foto Peristiwa</Text>
                      <Button
                        title="Upload File"
                        buttonStyle={{ marginTop: 10, borderRadius: 10, backgroundColor: colorFile, width: '40%', height: 50 }}
                        onPress={() => OpenDocumentFile()}
                      />
                    </View>

                    <Button
                      title="Kirim"
                      buttonStyle={{ marginTop: 20, borderRadius: 10, backgroundColor: '#2C5BA4', height: 47 }}
                      disabled={btnTouch}
                      // onPress={() =>  
                      //     Alert.alert('Peringatan',
                      //     'Maaf saat ini data belum bisa di kirim',
                      //     [{
                      //         text : 'Ya',
                      //         onPress : () => console.log('Tidak')
                      //     }])}
                      onPress={() => SendForm()}
                    />
                  </View>
                </ScrollView>
              )
          )}

        </View>

      </View>
    </SafeAreaView>
  )
}

export default FormKorlap


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item1: {
    backgroundColor: 'white',
    paddingVertical: 20,
    paddingHorizontal: 10,
    flexDirection: 'row',
    width: '100%',
    height: 86,
    justifyContent: 'space-between',
    marginBottom: 10
  },
  item: {
    paddingHorizontal: 5,
    flexDirection: 'row',
    flex: 1
  },
  title1: {
    fontSize: 16,
    paddingVertical: 10
    // fontFamily:'Montserrat'
  },
  title: {
    color: 'grey',
    fontSize: 12
  },
  containerflatList: {
    flexDirection: 'column',
    flex: 1,
    margin: 5
  },
  detail: {
    fontSize: 12,
    color: '#669F16'
  },
  wrapInput: {
    borderColor: '#C3C3C3',
    borderWidth: 1,
    borderRadius: 10,
    height: 47,
    marginVertical: 10
  },
})

