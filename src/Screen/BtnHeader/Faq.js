import React, { useState, useEffect } from 'react'
import { StyleSheet, View, FlatList } from 'react-native'
import { List } from 'react-native-paper';
import api from '../../Api';
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import HTML from "react-native-render-html";

const Faq = ({ navigation }) => {
    const [data, setData] = useState({})

    useEffect(() => {
        async function getToken() {
            try {
                const token = await AsyncStorage.getItem("api_token")
                return getVenue(token)
            } catch (err) {
                console.log(err)
            }
        }
        getToken()
    }, [])

    const getVenue = (token) => {
        Axios.get(`${api}/api/faq`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Accept': 'application/json'
            }
        }).then((res) => {
            setData(res.data.data)
        }).catch((err) => {
            console.log(err.response.data)
            if (err.response.data === 'Unauthorized.') {
                navigation.navigate('OnKick')
            }
        })
    }

    const renderItem = ({ item }) => {
        return (
            <View style={styles.containlist}>
                <List.Accordion title={item.pertanyaan} titleStyle={{ color: 'black' }}>
                    <View style={styles.listAccor}>
                        <HTML source={{ html: item.jawaban }} allowedStyles={false} ignoredStyles={['font-family']}/>
                    </View>
                </List.Accordion>
            </View>
        );
    }

    return (
        <View style={{ padding: 20, flex: 1, backgroundColor: 'white' }}>
            <FlatList
                data={data}
                renderItem={renderItem}
                keyExtractor={item => item.id.toString()}
                showsVerticalScrollIndicator={false}
            />
        </View>

    );
}

export default Faq;

const styles = StyleSheet.create({
    containlist: {
        marginHorizontal: 10,
        backgroundColor: 'white',
        borderRadius: 10,
        marginVertical: 10,
        elevation: 10,
        zIndex: 1
    },
    listAccor: {
        marginHorizontal: 20,
        marginBottom: 10,
        justifyContent: 'space-between',
        flexDirection: 'row'
    }
})