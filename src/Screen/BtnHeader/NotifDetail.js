import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Image, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import HTML from "react-native-render-html";
import api from '../../Api';


const NotifDetail = ({navigation,route}) => {
    const [pasing, setPasing] = useState(route.params)
    const [data, setData] = useState([])

    useEffect(() => {
        async function getToken() {
            try{
                const token = await AsyncStorage.getItem("api_token")
                return getVenue(token)
            }catch(err){
                console.log(err)
            }
        }
        getToken()
    },[])

    const getVenue = (token) =>{
        Axios.get(`${api}/api/notification/detail/${pasing.id}`,{
            headers : {
                'Authorization' : 'Bearer ' + token,
                'Accept' : 'application/json'
            }
        }).then((res) =>{
            setData(res.data.data)
        }).catch((err) =>{
            console.log(err.response.data)
            if(err.response.data === 'Unauthorized.'){
                navigation.navigate('OnKick')
              }
        })
    }

    return (
        <SafeAreaView style={styles.container}>
        {/* <ScrollView> */}
        
        <View style={styles.item1}>
        <View style={{alignItems:'center'}}> 
        <View style={{width:60,height:60,borderRadius:60,elevation: 5,alignItems:'center',paddingTop:22,backgroundColor:'white',marginVertical:20}}>
        <Image source={data.img ? data.img : require('../../Assets/drawable-xhdpi/btn.png')} style={{width:42,height:16}}/>
        </View>
        </View>

            <View style={{flexDirection:'row',justifyContent: 'space-between',marginTop:20}}>
            <Text style={{fontSize:18}}>{data.title}</Text>
            <Text style={{fontSize:10,color:'#39A95E'}}>{data.created_at}</Text>
            </View>
            <HTML source={{ html: '<p>'+data.description+'</p>'}} />
            {/* <Text>{data.description}</Text> */}
        </View>
        {/* </ScrollView> */}
        </SafeAreaView>
    )
}

export default NotifDetail

const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
    item1: {
        backgroundColor: 'white',
        padding: 20,
        flex:1,
      },
})
