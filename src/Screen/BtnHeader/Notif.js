import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View, Alert, ActivityIndicator } from 'react-native';
// import HTML from "react-native-render-html";
import api from '../../Api';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Fontisto from 'react-native-vector-icons/Fontisto';

const Notif = ({navigation}) => {
    const [data, setData] = useState([])
    const [uid, setUid] = useState('')
    const [Tokens, setTokens] = useState('')
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        async function getToken() {
            try{
                const token = await AsyncStorage.getItem("api_token")
                const id = await AsyncStorage.getItem("user_id")
                return getVenue(token,id)
            }catch(err){
                console.log(err)
            }
        }
        getToken()
    },[])

    const getVenue = (token,id) =>{
        setLoading(true);
        setTimeout(() => {

            setUid(id)
            setTokens(token)

        Axios.get(`${api}/api/notification/${id}`,{
            headers : {
                'Authorization' : 'Bearer ' + token,
                'Accept' : 'application/json'
            }
        }).then((res) =>{
            setData(res.data.data)
            
            setLoading(false);
            // console.log(res.data.data)
        }).catch((err) =>{
            setLoading(false);
             console.log(err.response.data)
            if(err.response.data === 'Unauthorized.'){
                navigation.navigate('OnKick')
              }else if(err.response.data.code === 400){
                Alert.alert('Peringatan',
                'Tidak ada Notifikasi ditemukan',
                [{
                    text : 'OK',
                    onPress : () => console.log('Tidak')
                }])
              }
        })

        }, 100);
    }

    const Dtnotif = (route,uid,nid,isread) => {
        
        if(isread == '1'){
            navigation.navigate('NotifDetail',route)
        }else if(isread == '0'){
            const paramdt = {
                user_id : uid,
                 notification_id : nid
            }
            Axios.post(`${api}/api/notification/read`,paramdt,{
                headers : {
                    'Authorization' : 'Bearer ' + Tokens,
                    'Accept' : 'application/json'
                },
            }).then((res) =>{
                navigation.navigate('NotifDetail',route)
            }).catch((err) =>{
                console.log(err.response.data)
                if(err.response.data === 'Unauthorized.'){
                    navigation.navigate('OnKick')
                  }
            })
          }
        }

        const Dltnotif = (rid) => {
            Axios.delete(`${api}/api/notification/`+rid,{
                headers : {
                    'Authorization' : 'Bearer ' + Tokens,
                    'Accept' : 'application/json'
                },
            }).then((res) =>{
                getVenue(Tokens,uid)
            }).catch((err) =>{
                console.log(err.response.data)
                if(err.response.data === 'Unauthorized.'){
                    navigation.navigate('OnKick')
                  }
            })
          }

        const DltAllnotif = () => {
            Axios.delete(`${api}/api/notification/all/`+uid,{
                headers : {
                    'Authorization' : 'Bearer ' + Tokens,
                    'Accept' : 'application/json'
                },
            }).then((res) =>{
                getVenue(Tokens,uid)
            }).catch((err) =>{
                console.log(err.response.data)
                if(err.response.data === 'Unauthorized.'){
                    navigation.navigate('OnKick')
                  }
            })
        }

    const renderItem = ({item}) =>{
        return(
            item.status !== 'deleted' ?
                (
                <View style={item.is_read === '1'? styles.item1 : styles.item2 }>
                <View style={{width:60,height:60,borderRadius:60,elevation: 5,alignItems:'center',paddingTop:22,backgroundColor:'white'}}>
                <Image source={item.img ? item.img : require('../../Assets/drawable-xhdpi/btn.png') } style={{width:42,height:16}}/>
                </View>
                <TouchableOpacity onPress={() => Dtnotif(item,uid,item.id,item.is_read)}
                style={{width:'80%'}}>
                <View style={{flexDirection:'column',paddingLeft:20,height:25}}>
                <View style={{flexDirection:'row',justifyContent:'space-between',marginBottom:10}}>
                <Text style={{fontSize:16}}>{item.title}</Text>
                {
                item.is_read > 0 ? 
                <TouchableOpacity onPress={
                    () =>
                Alert.alert('Peringatan',
                'Apakah anda ingin menghapus List ini ?',
                [{
                    text : 'Tidak',
                    onPress : () => console.log('Tidak')
                },{
                    text : 'Ya',
                    onPress : () =>  Dltnotif(item.notif_read_id)
                }])
             } ><Fontisto name='close' size={20} style={{color:'red'}}/>
                </TouchableOpacity>
                :
                <View></View>
                }
                </View>
                <Text style={{fontSize:14}}>{item.created_at}</Text>
                </View>
                </TouchableOpacity>
                </View>
                ) : (<View></View>)
            )
        } 

    return (
        <SafeAreaView style={styles.container}>
        <View style={{flex:1,flexDirection:'column',justifyContent: 'space-between'}}>

        {loading ? (
          <ActivityIndicator
            visible={loading}
            textContent={'Loading...'}
            style={{alignSelf:'center',marginTop:'50%',position:'absolute'}}
            size="large" 
            color="#2C5BA4"
          />
        ) : (
          <>
        <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
        />
        
        <View style={{width:'100%',backgroundColor:'white',alignItems:'center',padding:20}}>
        <TouchableOpacity onPress={() =>
             Alert.alert('Peringatan',
             'Apakah anda ingin menghapus semua List ini ?',
             [{
                 text : 'Tidak',
                 onPress : () => console.log('Tidak')
             },{
                 text : 'Ya',
                 onPress : () =>  DltAllnotif()
             }])
             }>
            <Text style={{color:'red'}}><FontAwesome5 name='trash-alt' size={20} style={{color:'red'}}/> Hapus Semua Pesan Terbaca </Text>
        </TouchableOpacity>
        </View>
        </>
        )}

        </View>
    </SafeAreaView>
    )
}

export default Notif

const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
    item1: {
        paddingVertical: 12,
        paddingHorizontal:10,
        flexDirection : 'row',
        // flex: 1,
        backgroundColor:'white',
        // backgroundColor: 'rgba(44,91,163,0.3)',
        marginVertical: 1,
      },
      item2: {
        paddingVertical: 12,
        paddingHorizontal:10,
        flexDirection : 'row',
        // flex: 1,
        // backgroundColor:'white',
        backgroundColor: 'rgba(44,91,163,0.3)',
        marginVertical: 1,
      },
})
