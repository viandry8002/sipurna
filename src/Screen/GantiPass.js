import React, { useState, useEffect } from 'react';
import { ScrollView, StyleSheet, Text, View, Alert, ToastAndroid } from 'react-native';
import { Button, Input } from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import api from '../Api';
import AntDesign from 'react-native-vector-icons/AntDesign';

const GantiPass = ({ navigation }) => {

    const [form, setForm] = useState({
        npp: '',
        pin: '',
        cpin: '',
    })

    const [show1, setShow1] = useState(true)
    const [show2, setShow2] = useState(true)

    useEffect(() => {

        async function getToken() {
            try {
                const onpp = await AsyncStorage.getItem("npp")
                return onInputChange(onpp, 'npp')
            } catch (err) {
                console.log(err)
            }
        }
        getToken()
    }, [])

    const onInputChange = (value, input) => {
        setForm({
            ...form,
            [input]: value,
        })
    }

    const isShow1 = () => {
        setShow1(!show1)
    }

    const isShow2 = () => {
        setShow2(!show2)
    }


    const SendForm = () => {
        if (form.pin === form.cpin) {
            Axios.post(`${api}/api/user/change-password`, form, {
            }).then((res) => {
                console.log(res.data)
                ToastAndroid.show("Sukses Mengubah PIN!", ToastAndroid.LONG)
                navigation.navigate('Profile')
            }).catch((err) => {
                console.log(err)
            })
        } else {
            Alert.alert('Peringatan',
                'PIN Tidak sama',
                [{
                    text: 'Ya',
                    onPress: () => console.log('Tidak')
                }])
        }
    }

    return (
        <ScrollView style={[styles.wrap, { backgroundColor: 'white' }]}>
            <View style={{ margin: 20 }}>

                <View>
                    <Text>Masukan PIN Baru</Text>
                    <View style={styles.wrapInput}>
                        <Input
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            style={{ padding: 10 }}
                            value={form.pin}
                            secureTextEntry={show1}
                            onChangeText={value => onInputChange(value, 'pin')}
                            rightIcon={
                                <AntDesign name='eye' size={20} onPress={() => isShow1()} />
                            }
                        />
                    </View>
                </View>

                <View >
                    <Text>Confirm PIN</Text>
                    <View style={styles.wrapInput}>
                        <Input
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            style={{ padding: 10 }}
                            value={form.cpin}
                            secureTextEntry={show2}
                            onChangeText={value => onInputChange(value, 'cpin')}
                            rightIcon={
                                <AntDesign name='eye' size={20} onPress={() => isShow2()} />
                            }
                        />
                    </View>
                </View>

                <Button
                    title="Ubah PIN"
                    buttonStyle={{ marginTop: 20, borderRadius: 10, backgroundColor: '#2C5BA4', height: 47 }}
                    // onPress={() =>  
                    //     Alert.alert('Peringatan',
                    //     'Maaf saat ini data belum bisa di kirim',
                    //     [{
                    //         text : 'Ya',
                    //         onPress : () => console.log('Tidak')
                    //     }])}
                    onPress={() => SendForm()}
                />
            </View>
        </ScrollView>
    )
}

export default GantiPass

const styles = StyleSheet.create({
    wrap: {
        flex: 1
    },
    wrapInput: {
        borderColor: '#C3C3C3',
        borderWidth: 1,
        borderRadius: 10,
        height: 47,
        marginVertical: 10,
        flexDirection: 'row'
    },
})

