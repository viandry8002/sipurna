import React from 'react'
import { StyleSheet, Text, View, SafeAreaView } from 'react-native'
import colors from '../../Style/Colors'

const AnakPerusahaanDP = () => {
    return (
        <SafeAreaView style={styles.container}>
            <View style={{ flex: 1, margin: 16 }}>
                <Text> - Binayasa Putra Batara</Text>
                <Text> - Bina Karya Pratama</Text>
                <Text> - Binasentra Purna</Text>
                <Text> - Binasentra Purna</Text>
                <Text> - Binasentra Purna</Text>
                <Text> - Hotel Batara Bali
                    Indonesia</Text>
                <Text> - Hotel Batara Bali
                    Indonesia</Text>
                <Text> - Batara Int’l Finansiondo</Text>
            </View>

        </SafeAreaView>
    )
}

export default AnakPerusahaanDP

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
})
