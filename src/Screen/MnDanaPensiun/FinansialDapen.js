import React, { useState, useEffect, Component, PropTypes } from 'react'
import { StyleSheet, Text, View, SafeAreaView, ActivityIndicator, Alert } from 'react-native'
import { Picker } from '@react-native-picker/picker'
import api from '../../Api';
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import PDFView from 'react-native-view-pdf';

const FinansialDapen = ({ navigation }) => {
  const [selectedValue, setSelectedValue] = useState('');
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem("api_token")
        return getVenue(token)
      } catch (err) {
        console.log(err)
      }
    }
    getToken()
  }, [])

  const getVenue = (token) => {
    setLoading(true);
    setTimeout(() => {

      Axios.get(`${api}/api/finansial-dapen`, {
        headers: {
          'Authorization': 'Bearer ' + token,
          'Accept': 'application/json'
        }
      }).then((res) => {
        console.log(res.data.data[0])
        if (res.data.message === 'Berhasil Menampilkan Data') {
          setData(res.data.data[0])
        } else {
          Alert.alert('Peringatan',
            'Maaf data tidak tersedia',
            [{
              text: 'Ya',
              onPress: () => navigation.goBack()
            }])
        }
        // setData(res.data.data)
        // console.log(res.data.data)
        setLoading(false);

      }).catch((err) => {
        setLoading(false);
        console.log(err.response.data)
        if (err.response.data === 'Unauthorized.') {
          navigation.navigate('OnKick')
        }
      })

    }, 100);
  }

  const resources = {
    file: Platform.OS === 'ios' ? 'downloadedDocument.pdf' : '/sdcard/Download/downloadedDocument.pdf',
    url: data.file_path,
    base64: 'JVBERi0xLjMKJcfs...',
  };

  // const resources = 
  //   data.map((datas, index)=>{
  // return{
  // console.log(datas.file_path)
  // file: Platform.OS === 'ios' ? 'downloadedDocument.pdf' : '/sdcard/Download/downloadedDocument.pdf',
  // url: datas.file_path,
  // base64: 'JVBERi0xLjMKJcfs...',
  // }
  // })

  const resourceType = 'url';
  return (
    <SafeAreaView style={styles.container}>
      {/* <View style={{flex:1,flexDirection:'column'}}>
        <View style={styles.item1}>
            <Text>Dana Pensiun BTN</Text>
            <Text>Program pensiun manfaat pasti</Text>
            <Text>LAPORANASET NETO (Rp)</Text>
            <View style={{borderWidth:1,borderColor:'#C3C3C3',borderRadius:10,width:'90%',height:45,marginTop:20}}>
                <Picker
                selectedValue={selectedValue}
                style={{ height: 40, width: '100%'}}
                onValueChange={(itemValue, itemIndex) => {setSelectedValue(itemValue)}}>
                <Picker.Item label=" PER 31 DESEMBER 2019 dan 2018" value=" PER 31 DESEMBER 2019 dan 2018" />
                <Picker.Item label=" PER 31 DESEMBER 2019 dan 2018" value=" PER 31 DESEMBER 2019 dan 2018" />
                <Picker.Item label=" PER 31 DESEMBER 2019 dan 2018" value=" PER 31 DESEMBER 2019 dan 2018" />
                <Picker.Item label=" PER 31 DESEMBER 2019 dan 2018" value=" PER 31 DESEMBER 2019 dan 2018" />
                </Picker>
            </View>
        </View>
        
        <View style={styles.item2}>
            <View style={{flexDirection:'row',justifyContent: 'space-between',padding:10,borderColor:'#C3C3C3',borderWidth:1}}>
                <Text>ASET (Lanjutan)</Text>
                <Text>Catatan</Text>
                <Text>2018</Text>
                <Text>2019</Text>
            </View> 
        </View>

        </View> */}
      {loading ? (
        <ActivityIndicator
          visible={loading}
          textContent={'Loading...'}
          style={{ alignSelf: 'center', marginTop: '50%', position: 'absolute' }}
          size="large"
          color="#2C5BA4"
        />
      ) : (
        <>
          <PDFView
            fadeInDuration={250.0}
            style={{ flex: 1 }}
            resource={resources[resourceType]}
            resourceType={resourceType}
            onLoad={() => console.log(`PDF rendered from ${resourceType}`)}
            onError={(error) => console.log('Cannot render PDF', error)}
          />
        </>
      )}
    </SafeAreaView>
  )
}

export default FinansialDapen


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item1: {
    backgroundColor: 'white',
    paddingVertical: 10,
    paddingHorizontal: 10,
    flexDirection: 'column',
    width: '100%',
    height: 150,
    marginBottom: 5,
    alignItems: 'center'
  },
  item2: {
    backgroundColor: 'white',
    padding: 20,
    flexDirection: 'column',
    width: '100%',
    height: '100%',
    justifyContent: 'space-between',
  },
})

