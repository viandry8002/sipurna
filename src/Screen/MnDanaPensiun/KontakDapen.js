import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Image, Text, View, ScrollView, Linking, TouchableOpacity } from 'react-native';
import api from '../../Api';
import styles from '../../Style/Kontak';

const KontakDapen = ({navigation}) => {
    const [data, setData] = useState([])
    const [KontakPhone, setKontakPhone] = useState([])
    const [KontakMobile, setKontakMobile] = useState([])
    const [Kontakfax, setKontakfax] = useState([])

    useEffect(() => {
        async function getToken() {
             try{
                 const token = await AsyncStorage.getItem("api_token")
                 return getVenue(token)
             }catch(err){
                 console.log(err)
             }
        }
        getToken()
        // getVenue()
    },[])

    const getVenue = (token) =>{
        Axios.get(`${api}/api/configuration/dapen`,{
            timeout : 20000,
            headers : {
                'Authorization' : 'Bearer ' + token,
                'Accept' : 'application/json'
            }
        }).then((res) =>{
            setData(res.data.data)
            setKontakPhone(res.data.data.contact.phone)
            setKontakMobile(res.data.data.contact.mobile)
            setKontakfax(res.data.data.contact.fax)
        }).catch((err) =>{
            console.log(err.response.data)
            if(err.response.data === 'Unauthorized.'){
                navigation.navigate('OnKick')
              }
        })
    }

    return (
        <View style={styles.container}>
         <Image source={require('../../Assets/drawable-xhdpi/Group253.png')} style={{flex:1,width:'100%',height:'100%',position:'absolute',marginTop:100}}/>
         <View style={{backgroundColor:'rgba(255,255,255,0.8)',flex:1}}>

         <ScrollView>
         <View style={{flex:1,flexDirection:'column',justifyContent: 'space-between', alignItems: 'center',paddingHorizontal:20,marginBottom:50}}>

             <Image source={require('../../Assets/drawable-xhdpi/cs.png')} style={styles.cs}/>
             {/* box Kontak */}
             <View style={styles.contact2}>
             <Image source={require('../../Assets/drawable-xhdpi/surface1.png')} style={{width:25,
            height:35}}/>
             <Text style style={styles.font} >{data.address}</Text>
             </View>

             <View style={styles.contact2}>
             <Image source={require('../../Assets/drawable-xhdpi/telephone.png')} style={styles.ImageIcon2}/>
             <View style={{flexDirection:'column'}}>
             {
                KontakPhone  === ''?
                <Text style style={styles.font} ></Text> :
                KontakPhone.map((phone, index1)=>{
                    return (<TouchableOpacity onPress={() => Linking.openURL(`tel:${phone.value}`)}><Text style style={styles.font} key={index1} >{phone.value}</Text></TouchableOpacity>)
                }) 
            }
             </View>
             </View>

             <View style={styles.contact2}>
             <Image source={require('../../Assets/drawable-xhdpi/fax.png')} style={styles.ImageIcon}/>
             <View style={{flexDirection:'column'}}>
             {
                Kontakfax  === 'Tidak Ada Data'?
                <Text style style={styles.font} >{Kontakfax}</Text> :
                Kontakfax.map((fax, index2)=>{
                    return <Text style style={styles.font} key={index2} >{fax.value}</Text>
                })
            }
             </View>
             </View>
             {/* <View style={styles.contact2}>
             <Image source={require('../../Assets/drawable-xhdpi/feather-globe.png')} style={styles.ImageIcon}/>
             <Text style style={styles.font} >www.dapenbtn.com</Text>
             </View> */}
             <View style={styles.contact2}>
             <Image source={require('../../Assets/drawable-xhdpi/email.png')} style={styles.ImageIcon}/>
             <Text style style={styles.font} >{data.email}</Text>
             </View>
             {/* box Kontak */}
             
         </View>
         </ScrollView>
         </View>
     </View>
    )
}

export default KontakDapen
