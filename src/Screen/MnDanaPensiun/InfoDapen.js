import React, { useState, useEffect} from 'react'
import { StyleSheet, Text, View,SafeAreaView,ScrollView } from 'react-native'
import { Button } from 'react-native-elements';
import api from '../../Api';
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';  

const InfoDapen = ({navigation}) => {
  const [data, setData] = useState([])

    useEffect(() => {
        async function getToken() {
             try{
                 const token = await AsyncStorage.getItem("api_token")
                 return getVenue(token)
             }catch(err){
                 console.log(err)
             }
        }
        getToken()
    },[])

    const getVenue = (token) =>{
        Axios.get(`${api}/api/configuration/dapen`,{
            timeout : 20000,
            headers : {
                'Authorization' : 'Bearer ' + token,
                'Accept' : 'application/json'
            }
        }).then((res) =>{
            setData(res.data.data)
        }).catch((err) =>{
          console.log(err.response.data)
          if(err.response.data === 'Unauthorized.'){
              navigation.navigate('OnKick')
            }
        })
    }

    return (
        <View style={styles.container}>
        <View style={{flex:1,flexDirection:'column'}}>
        <View style={styles.item1}>
        <ScrollView horizontal={true}>
        <Button title="Visi & Misi" buttonStyle={{backgroundColor:'#2C5BA4',marginRight:10}} onPress={() => navigation.navigate('InfoDapen')}/>
        <Button title="Daftar Pengurus" type="outline" titleStyle={{color:'black'}} buttonStyle={{marginRight:10}} onPress={() => navigation.navigate('PengurusDapen')}/>
        <Button title="Info Kegiatan" type="outline" titleStyle={{color:'black'}} onPress={() => navigation.navigate('KegiatanDpn')}/>
        </ScrollView>
        </View>


        <ScrollView style={styles.item2}>
        <View style={{paddingVertical:20}} >
          {/* <Text style={styles.fontText}>
          Berdasarkan Ketetapan Direksi No. 8/KD/DIR/HCD/2015 dari
          PT Bank Tabungan Negara (Persero) Tbk. selaku Pendiri 
          Dana Pensiun BTN tentang Kebijakan dan Pedoman  
          Tata Kelola Dana Pensiun BTN, maka Visi dan Misi Dana 
          Pensiun BTN adalah sebagai berikut:
          </Text> */}
          <Text style={styles.title1} >VISI</Text>
          <Text style={styles.fontText}>
          {data.vision}
          </Text>
          <Text style={styles.title1}>Misi</Text>
          <Text style={styles.fontText}>
          {data.mission}
          </Text>
        </View>
        </ScrollView>

        </View>
    </View>
    )
}

export default InfoDapen

const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
      item1: {
        backgroundColor: 'white',
        paddingVertical: 20,
        paddingHorizontal:15,
        flexDirection : 'row',
        width:'100%',
        height: 86,
        justifyContent: 'space-between',
        marginBottom:10
      },
      item2: {
        // flex:1,
        backgroundColor: 'white',
        paddingHorizontal:20,
        paddingVertical:5,
        flexDirection : 'column',
        // width:'100%',
        // justifyContent: 'space-between',
      },
      title1: {
       fontSize:18,
       marginVertical:10,
      },
      fontText: {
        fontSize:15,
        // color:'#707070'
      }
})
