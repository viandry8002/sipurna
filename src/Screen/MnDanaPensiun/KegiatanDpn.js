import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, SafeAreaView, ScrollView, FlatList, Image, TouchableOpacity } from 'react-native'
import { Button } from 'react-native-elements';
import api from '../../Api';
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import HTML from "react-native-render-html";

const KegiatanDpn = ({ navigation }) => {
  const [data, setData] = useState([])

  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem("api_token")
        return getVenue(token)
      } catch (err) {
        console.log(err)
      }
    }
    getToken()
  }, [])

  const getVenue = (token) => {
    Axios.get(`${api}/api/activity/dapen`, {
      timeout: 20000,
      headers: {
        'Authorization': 'Bearer ' + token,
        'Accept': 'application/json'
      }
    }).then((res) => {
      setData(res.data.data)
    }).catch((err) => {
      console.log(err.response.data)
      if (err.response.data === 'Unauthorized.') {
        navigation.navigate('OnKick')
      }
    })
  }

  const DtlInfoKegiatan = (route) => {
    navigation.navigate('DtlKegiatanDpn', route)
  }

  const renderItem = ({ item }) => (
    <View style={styles.item}>
      <Image source={item.file === '' ? require('../../Assets/drawable-xhdpi/img-notfound.png') : { uri: item.file }} style={{ width: 140, height: 140, borderRadius: 10, marginHorizontal: 10, marginTop: 10 }} />
      <View style={styles.containerflatList}>
        <Text style={styles.title1}>{item.title}</Text>
        <HTML source={{ html: item.description === '' ? '-' : item.description }} renderers={{
          p: (_, children) => <Text numberOfLines={1}>{children}</Text>,
        }} />
        <TouchableOpacity style={{ alignItems: 'center', marginVertical: 10 }} onPress={() => DtlInfoKegiatan(item)}>
          <Text style={styles.detail}>Detail</Text>
        </TouchableOpacity>
      </View>
    </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View style={styles.item1}>
          <ScrollView horizontal={true}>
            <Button title="Visi & Misi" type="outline" titleStyle={{ color: 'black' }} buttonStyle={{ marginRight: 10 }} onPress={() => navigation.navigate('InfoDapen')} />
            <Button title="Daftar Pengurus" type="outline" titleStyle={{ color: 'black' }} buttonStyle={{ marginRight: 10 }} onPress={() => navigation.navigate('PengurusDapen')} />
            <Button title="Info Kegiatan" buttonStyle={{ backgroundColor: '#2C5BA4', marginRight: 10 }} onPress={() => navigation.navigate('KegiatanDpn')} />
          </ScrollView>
        </View>


        <View style={styles.item2}>
          <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
          />

        </View>

      </View>
    </SafeAreaView>
  )
}

export default KegiatanDpn

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item1: {
    backgroundColor: 'white',
    paddingVertical: 20,
    paddingHorizontal: 15,
    flexDirection: 'row',
    width: '100%',
    height: 86,
    justifyContent: 'space-between',
    marginBottom: 10
  },
  item2: {
    flexDirection: 'column',
    width: '100%',
    justifyContent: 'space-between',
    marginBottom: 100
  },
  item: {
    backgroundColor: 'white',
    padding: 10,
    marginVertical: 3,
    flexDirection: 'row',
    flex: 1
  },
  title1: {
    fontSize: 16,
    paddingVertical: 10
    // fontFamily:'Montserrat'
  },
  containerflatList: {
    flexDirection: 'column',
    flex: 1,
    margin: 5
  },
  detail: {
    fontSize: 12,
    color: '#669F16'
  },
})
