import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View, ActivityIndicator } from 'react-native';
import { Button } from 'react-native-elements';
import api from '../../Api';

const PengurusDapen = ({ navigation }) => {
  const [data, setData] = useState()
  const [arrayholder, setArrayholder] = useState('')
  const [Tokens, setTokens] = useState('')
  const [loading, setLoading] = useState(false);

  useEffect(() => {

    async function getToken() {
      try {
        const token = await AsyncStorage.getItem("api_token")
        return getKomisaris(token)
      } catch (err) {
        console.log(err)
      }
    }
    getToken()
  }, []);

  const getKomisaris = (token) => {
    setLoading(true);
    setTimeout(() => {

      Axios.get(`${api}/api/ppusat/dewan_pengawas_dapen`, {
        headers: {
          'Authorization': 'Bearer ' + (Tokens === '' ? token : Tokens),
          'Accept': 'application/json'
        }
      }).then((res) => {
        setArrayholder('DATA1')
        setData(res.data.data)
        Tokens === '' ? setTokens(token) : setTokens(Tokens)
        setLoading(false);

      }).catch((err) => {
        setLoading(false);
        console.log(err.response.data)
        if (err.response.data === 'Unauthorized.') {
          navigation.navigate('OnKick')
        }
      })

    }, 100);
  }

  const getDireksi = () => {
    setLoading(true);
    setTimeout(() => {

      Axios.get(`${api}/api/ppusat/direksi_dapen`, {
        headers: {
          'Authorization': 'Bearer ' + Tokens,
          'Accept': 'application/json'
        }
      }).then((res) => {
        setArrayholder('DATA2')
        setData(res.data.data)
        setLoading(false);

      }).catch((err) => {
        console.log(err.response.data)
        if (err.response.data === 'Unauthorized.') {
          navigation.navigate('OnKick')
        }
      })

    }, 100);
  }

  const DtlProfile = (route) => {
    navigation.navigate('DtlKepemimpinan', route)
  }

  const renderItem = ({ item }) => (
    <View style={styles.item}>
      <Image source={item.file === null ? require('../../Assets/drawable-xhdpi/img-notfound.png') : { uri: item.file }} style={{ width: 140, height: 160, borderRadius: 10, marginHorizontal: 10, marginTop: 10 }} />
      <View style={styles.containerflatList}>
        <Text style={styles.title1}>{item.name}</Text>
        <Text style={styles.title2}>{item.position}</Text>
        <Text style={styles.title3}>Handphone : </Text>
        <Text style={styles.title3}>{item.phone}</Text>
      </View>
    </View>
  );

  var typ = arrayholder == 'DATA1' ? "solid" : "outline"
  var ttl = arrayholder == 'DATA1' ? { color: 'white' } : { color: 'black' }
  var btn = arrayholder == 'DATA1' ? { backgroundColor: '#2C5BA4', paddingHorizontal: 50 } : { paddingHorizontal: 50 }

  var typ1 = arrayholder == 'DATA2' ? "solid" : "outline"
  var ttl1 = arrayholder == 'DATA2' ? { color: 'white' } : { color: 'black' }
  var btn1 = arrayholder == 'DATA2' ? { backgroundColor: '#2C5BA4', paddingHorizontal: 50 } : { paddingHorizontal: 50 }

  return (
    <SafeAreaView style={styles.container}>
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View style={styles.item1}>

          <Button title="Pengawas"
            type={typ}
            titleStyle={ttl}
            buttonStyle={btn}
            onPress={() => { getKomisaris() }} />

          <Button title="Direksi"
            type={typ1}
            titleStyle={ttl1}
            buttonStyle={btn1}
            onPress={() => { getDireksi() }} />
        </View>

        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between', backgroundColor: 'white' }}>

          {loading ? (
            <ActivityIndicator
              visible={loading}
              textContent={'Loading...'}
              style={{ alignSelf: 'center', marginTop: '40%', position: 'absolute' }}
              size="large"
              color="#2C5BA4"
            />
          ) : (
            <>
              <FlatList
                data={data}
                renderItem={renderItem}
                keyExtractor={item => item.id.toString()}
              />
            </>
          )}

        </View>

      </View>
    </SafeAreaView>
  )
}

export default PengurusDapen


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item1: {
    backgroundColor: 'white',
    paddingVertical: 20,
    paddingHorizontal: 10,
    flexDirection: 'row',
    width: '100%',
    height: 86,
    justifyContent: 'space-between',
    marginBottom: 10
  },
  item: {
    backgroundColor: 'white',
    padding: 10,
    marginVertical: 3,
    flexDirection: 'row',
    flex: 1
  },
  title1: {
    fontSize: 18,
    marginTop: 10,
    marginBottom: 5
  },
  title2: {
    color: '#669F16',
    marginBottom: 15
  },
  title3: {
    color: '#919191',
  },
  containerflatList: {
    flexDirection: 'column',
    flex: 1,
    margin: 5
  },
})


