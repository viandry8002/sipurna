import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Image, SafeAreaView, StatusBar, Text, View, ScrollView, Linking, TouchableOpacity } from 'react-native';
import api from '../Api';
import styles from '../Style/Kontak';

const HubungiKami = () => {
    const [data, setData] = useState([])
    const [KontakPhone, setKontakPhone] = useState([])
    const [KontakMobile, setKontakMobile] = useState([])
    const [Kontakfax, setKontakfax] = useState([])

    useEffect(() => {
        getVenue()
    },[])

    const getVenue = (token) =>{
        Axios.get(`${api}/api/cs-ikapurna`,{
            timeout : 20000,
            headers : {
                'Accept' : 'application/json'
            }
        }).then((res) =>{
            setData(res.data.data)
            setKontakPhone(res.data.data.contact.phone)
            setKontakMobile(res.data.data.contact.mobile)
            setKontakfax(res.data.data.contact.fax)
        }).catch((err) =>{
            console.log(err)
        })
    }
    
    return (
         <SafeAreaView style={styles.container}>
         <Image source={require('../Assets/drawable-xhdpi/Group253.png')} style={{flex:1,width:'100%',height:'100%',position:'absolute',marginTop:100}}/>
         <View style={{backgroundColor:'rgba(255,255,255,0.8)',flex:1}}>
         <StatusBar translucent backgroundColor='transparent' barStyle="light-content" />
         
         <ScrollView>
         <View style={{flex:1,flexDirection:'column',justifyContent: 'space-between', alignItems: 'center',paddingHorizontal:20,marginBottom:50}}>
        
             <Image source={require('../Assets/drawable-xhdpi/cs.png')} style={styles.cs}/>
             {/* <Text style={[styles.font,{textAlign:'center',marginBottom:20,}]}>{data.vision_mission}</Text> */}
             {/* box */}

            <View style={styles.contact2}>
            <Image source={require('../Assets/drawable-xhdpi/surface1.png')} style={{width:25,
            height:35,marginTop:10}}/>
            <Text style style={styles.font} >{data.address}</Text>
            </View>

            <View style={styles.contact2}>
            <Image source={require('../Assets/drawable-xhdpi/telephone.png')} style={styles.ImageIcon2}/>
            <View style={{flexDirection:'column'}}>
            {
                KontakPhone  === 'Tidak Ada Data'?
                <Text style style={styles.font} >{KontakPhone}</Text> :
                KontakPhone.map((phone, index1)=>{
                    return (<TouchableOpacity onPress={() => Linking.openURL(`tel:${phone.value}`)}><Text style style={styles.font} key={index1} >{phone.value}</Text>
                    </TouchableOpacity>)
                })
            }
            </View>
            </View>

            <View style={styles.contact2}>
            <Image source={require('../Assets/drawable-xhdpi/fax.png')} style={styles.ImageIcon}/>
            <View style={{flexDirection:'column'}}>
            {
                Kontakfax  === 'Tidak Ada Data'?
                <Text style style={styles.font} >{Kontakfax}</Text> :
                Kontakfax.map((fax, index2)=>{
                    return <Text style style={styles.font} key={index2} >{fax.value}</Text>
                })
            }
            </View>
            </View>
            {/* <View style={styles.contact2}>
            <Image source={require('../../Assets/drawable-xhdpi/feather-globe.png')} style={styles.ImageIcon}/>
            <Text style style={styles.font} >www.dapenbtn.com</Text>
            </View> */}
            <View style={styles.contact2}>
            <Image source={require('../Assets/drawable-xhdpi/email.png')} style={styles.ImageIcon}/>
            <Text style style={styles.font} >{data.email}</Text>
            </View>
            </View>
            
             {/* box */}
         </ScrollView>
         </View> 
     </SafeAreaView>
    )
}

export default HubungiKami
