import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { FlatList, SafeAreaView, StyleSheet, Text, TouchableOpacity, View, ActivityIndicator } from 'react-native';

const KeuanganBTN = ({ navigation }) => {
  const [data, setData] = useState([])
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    getVenue()
  }, [])

  const getVenue = () => {
    setLoading(true);
    setTimeout(() => {

      Axios.get(`http://sipurna.com/aplikasi/api/btn/lap_keu_btn`)
        .then((res) => {
          setData(res.data)
          setLoading(false)
        }).catch((err) => {
          setLoading(false);
          console.log(err.response.data)
          if (err.response.data === 'Unauthorized.') {
            navigation.navigate('OnKick')
          }
        })
    }, 100);
  }

  const Dtl = (route) => {
    navigation.navigate('DtlKeuanganBTN', route)
  }

  const renderItem = ({ item }) => (
    <View style={styles.item}>
      <View style={styles.containerflatList}>
        <TouchableOpacity onPress={() => Dtl(item)}>
          <Text style={{ paddingBottom: 10, fontSize: 16, color: '#2C5BA4' }}>{item.nama}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between' }}>

        {loading ? (
          <ActivityIndicator
            visible={loading}
            textContent={'Loading...'}
            style={{ alignSelf: 'center', marginTop: '50%', position: 'absolute' }}
            size="large"
            color="#2C5BA4"
          />
        ) : (
          <>
            <FlatList
              data={data}
              renderItem={renderItem}
              keyExtractor={item => item.id.toString()}
            />
          </>
        )}

      </View>
    </SafeAreaView>
  )
}

export default KeuanganBTN

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: 'white',
    padding: 10,
    marginVertical: 3,
    flexDirection: 'row',
    flex: 1
  },
  title1: {
    fontSize: 16,
    paddingVertical: 10
    // fontFamily:'Montserrat'
  },
  containerflatList: {
    flexDirection: 'column',
    flex: 1,
    margin: 5
  },
  detail: {
    fontSize: 12,
    color: '#669F16'
  },
})
