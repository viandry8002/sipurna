import React from 'react';
import { Image, SafeAreaView, StatusBar, StyleSheet, TouchableOpacity, View } from 'react-native';
import colors from '../../Style/Colors';

const LmnBTN = ({navigation}) => {
    return (
         <SafeAreaView style={styles.container}>
         <Image source={require('../../Assets/drawable-xhdpi/Group253.png')} style={{flex:1,width:'100%',height:'100%',position:'absolute',marginTop:100}}/>
         <View style={{backgroundColor:'rgba(255,255,255,0.8)',flex:1}}>
         <StatusBar translucent backgroundColor='transparent' barStyle="light-content" />
         <View style={{flex:1,flexDirection:'column',justifyContent: 'space-between', alignItems: 'center',paddingHorizontal:10}}>
             {/* box Menu */}
             <View style={{width:'100%',height:300,marginTop:30}}>
             <TouchableOpacity onPress={() => navigation.navigate('Tentang', { name: 'btn',title: 'Bank BTN' })}>
             <Image source={require('../../Assets/drawable-xhdpi/ListMenuBTN/Group298.png')} style={styles.btnMenu}/>
             </TouchableOpacity>

             <TouchableOpacity onPress={() => navigation.navigate('Kepemimpinan')}>
             <Image source={require('../../Assets/drawable-xhdpi/ListMenuBTN/Group139.png')} style={styles.btnMenu}/>
             </TouchableOpacity>
            
             <TouchableOpacity onPress={() => navigation.navigate('KeuanganBTN')}>
             <Image source={require('../../Assets/drawable-xhdpi/ListMenuBTN/Group140.png')} style={styles.btnMenu}/>
             </TouchableOpacity>

             <TouchableOpacity onPress={() => navigation.navigate('InfoBTN')}>
             <Image source={require('../../Assets/drawable-xhdpi/ListMenuBTN/Group297.png')} style={styles.btnMenu}/>
             </TouchableOpacity>
             
             <TouchableOpacity onPress={() => navigation.navigate('KontakBTN')}>
             <Image source={require('../../Assets/drawable-xhdpi/ListMenuBTN/Group141.png')} style={styles.btnMenu}/>
             </TouchableOpacity>
             
             </View>
             {/* box Menu */}
             
         </View>
         </View> 
     </SafeAreaView>
    )
}

export default LmnBTN


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    btnMenu:{
        width:'100%',
        height:70
    }
})


