import React, { useState } from 'react'
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native'

const DtlKepemimpinan = ({route}) => {
    const [data, setData] = useState(route.params)
    return (
        <ScrollView style={{flex:1,backgroundColor: 'white'}}>
        <View style={{paddingHorizontal: 15,paddingVertical:30,flexDirection:'column'}}>

        <Image 
        source={data.file === null ? require('../../Assets/drawable-xhdpi/img-notfound.png')
        : {uri: data.file}}
        style={{
            alignSelf:'center',
            width: '80%',
            // height: 260,
            height: 300
        }}/>
            
        <View style={{marginTop:10}}>
        <Text style={{fontSize: 26,marginTop:10}}>{data.name}</Text>
        <Text style={{fontSize: 16,marginBottom:20,color: '#669F16'}}>{data.position}</Text>
        <Text style={styles.text}>{data.description}</Text>
        <Text style={styles.title2}>Riwayat Pendidikan dan Pelatihan</Text>
        <Text style={styles.text}>{data.educational_background}</Text>
        <Text style={styles.title2}>Perjalanan Karir</Text>
        <Text style={styles.text}>{data.work_experience}</Text>
        </View>

        </View>
        </ScrollView>
    )
}

export default DtlKepemimpinan

const styles = StyleSheet.create({
    title2 : {fontSize: 16,marginVertical:15},
    text: {color:'grey',fontSize:13}
})
