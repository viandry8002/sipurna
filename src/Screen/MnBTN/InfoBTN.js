import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  ScrollView,
  FlatList,
  Image,
  TouchableOpacity,
  useWindowDimensions,
} from 'react-native';
import {Button} from 'react-native-elements';
import api from '../../Api';
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import HTML from 'react-native-render-html';

const InfoBTN = ({navigation}) => {
  const [data, setData] = useState([]);
  const [holder, setHolder] = useState('holder1');
  const [tokens, setTokens] = useState('');
  const {width} = useWindowDimensions();

  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('api_token');
        return getVenue(token);
      } catch (err) {
        console.log(err);
      }
    }
    getToken();
  }, []);

  const getVenue = (token) => {
    Axios.get(`${api}/api/configuration/btn`, {
      headers: {
        Authorization: 'Bearer ' + (tokens === '' ? token : tokens),
        Accept: 'application/json',
      },
    })
      .then((res) => {
        tokens === '' ? setTokens(token) : setTokens(tokens);
        console.log('res data', res.data.data);
        setData(res.data.data);
      })
      .catch((err) => {
        console.log(err.response.data);
        // if(err.response.data === 'Unauthorized.'){
        //     navigation.navigate('OnKick')
        //   }
      });
  };

  const infoKegiatan = () => {
    Axios.get(`${api}/api/activity/btn`, {
      headers: {
        Authorization: 'Bearer ' + tokens,
        Accept: 'application/json',
      },
    })
      .then((res) => {
        console.log('res data1s', res.data.data);
        setData(res.data.data);
      })
      .catch((err) => {
        console.log(err.response.data);
        if (err.response.data === 'Unauthorized.') {
          navigation.navigate('OnKick');
        }
      });
  };

  const DtlInfoKegiatan = (route) => {
    navigation.navigate('DtlInfoKegiatan', route);
  };

  const source = {
    // html:`<p>${item.description === '' ? '-' : item.description}</p>`
  };

  const renderItem = ({item}) => (
    <View style={styles.item}>
      <Image
        source={
          item.file === ''
            ? require('../../Assets/drawable-xhdpi/img-notfound.png')
            : {uri: item.file}
        }
        style={{
          width: 140,
          height: 140,
          borderRadius: 10,
          marginHorizontal: 10,
          marginTop: 10,
        }}
      />
      <View style={styles.containerflatList}>
        <Text style={styles.title1}>{item.title}</Text>
        <HTML
          source={{
            html: `<p>${item.description === '' ? '-' : item.description}</p>`,
          }}
          renderers={{
            p: (_, children) => <Text numberOfLines={1}>{children}</Text>,
          }}
        />
        <TouchableOpacity
          style={{alignItems: 'center', marginVertical: 10}}
          onPress={() => DtlInfoKegiatan(item)}>
          <Text style={styles.detail}>Detail</Text>
        </TouchableOpacity>
      </View>
    </View>
  );

  var typ = holder == 'holder1' ? 'solid' : 'outline';
  var ttl = holder == 'holder1' ? {color: 'white'} : {color: 'black'};
  var btn =
    holder == 'holder1'
      ? {backgroundColor: '#2C5BA4', paddingHorizontal: 35}
      : {paddingHorizontal: 35};

  var typ1 = holder == 'holder2' ? 'solid' : 'outline';
  var ttl1 = holder == 'holder2' ? {color: 'white'} : {color: 'black'};
  var btn1 =
    holder == 'holder2'
      ? {backgroundColor: '#2C5BA4', paddingHorizontal: 35}
      : {paddingHorizontal: 35};

  return (
    <SafeAreaView style={styles.container}>
      <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={styles.item1}>
          {/* <ScrollView horizontal={true}> */}
          <Button
            title="Visi & Misi"
            type={typ}
            titleStyle={ttl}
            buttonStyle={btn}
            onPress={() => {
              setHolder('holder1'), getVenue();
            }}
          />
          <Button
            title="Info Kegiatan"
            type={typ1}
            titleStyle={ttl1}
            buttonStyle={btn1}
            onPress={() => {
              setHolder('holder2'), infoKegiatan();
            }}
          />
          {/* </ScrollView> */}
        </View>

        {holder === 'holder1' ? (
          <ScrollView style={styles.item2}>
            <View style={{paddingVertical: 20}}>
              <Text style={styles.title1}>VISI</Text>
              {data?.vision?.slice(0, 3) == '<p>' ? (
                <HTML
                  contentWidth={width}
                  source={{html: data?.vision}}
                  tagsStyles={{p: {fontSize: 15}}}
                />
              ) : (
                <Text style={styles.fontText}>{data.vision}</Text>
              )}
              <Text style={styles.title1}>Misi</Text>
              {data?.mission?.slice(0, 3) == '<p>' ? (
                <HTML
                  contentWidth={width}
                  source={{html: data?.mission}}
                  tagsStyles={{p: {fontSize: 15}}}
                />
              ) : (
                <Text style={styles.fontText}>{data.mission}</Text>
              )}
            </View>
          </ScrollView>
        ) : (
          <View
            style={{
              flexDirection: 'column',
              width: '100%',
              justifyContent: 'space-between',
              marginBottom: '20%',
            }}>
            <FlatList
              data={data}
              renderItem={renderItem}
              keyExtractor={(item) => item.id.toString()}
            />
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

export default InfoBTN;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item1: {
    backgroundColor: 'white',
    paddingVertical: 20,
    paddingHorizontal: 15,
    flexDirection: 'row',
    width: '100%',
    height: 86,
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  item2: {
    // flex:1,
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingVertical: 5,
    flexDirection: 'column',
    // width:'100%',
    // justifyContent: 'space-between',
  },
  title1: {
    fontSize: 18,
    marginVertical: 10,
  },
  fontText: {
    fontSize: 15,
    // color:'#707070'
  },

  item: {
    backgroundColor: 'white',
    padding: 10,
    marginVertical: 3,
    flexDirection: 'row',
    flex: 1,
  },
  containerflatList: {
    flexDirection: 'column',
    flex: 1,
    margin: 5,
  },
  detail: {
    fontSize: 12,
    color: '#669F16',
  },
});
