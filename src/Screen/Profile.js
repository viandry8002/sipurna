import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Alert, Image, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import api from '../Api';
import OneSignal from 'react-native-onesignal';


const Profile = ({ navigation }) => {
    const [data, setData] = useState([])
    const [KontakPhone, setKontakPhone] = useState([])

    useEffect(() => {
        async function getToken() {
            try {
                const token = await AsyncStorage.getItem("api_token")
                const id = await AsyncStorage.getItem("user_id")
                return getVenue(token, id)
            } catch (err) {
                console.log(err)
            }
        }
        getToken()
    }, [])

    const getVenue = (token, id) => {
        Axios.get(`${api}/api/user/${id}`, {
            headers: {
                'Authorization': 'Bearer ' + token,
                'Accept': 'application/json'
            }
        }).then((res) => {
            console.log('result', res.data.data)
            setData(res.data.data)
            setKontakPhone(res.data.data.phone)

        }).catch((err) => {
            console.log(err.response.data)
            if (err.response.data === 'Unauthorized.') {
                navigation.navigate('OnKick')
            }
        })
    }

    const goOut = async () => {
        try {
            await AsyncStorage.removeItem("api_token")
            await AsyncStorage.removeItem("user_id")
            await AsyncStorage.removeItem("npp")
            await AsyncStorage.removeItem("fullname")
            await AsyncStorage.removeItem("iskorlap")
            OneSignal.removeExternalUserId((results) => {
                // The results will contain push and email success statuses
                console.log('Results of removing external user id');
                console.log(results);
            })
            console.log('remove item success')
            navigation.reset({
                index: 0,
                routes: [{ name: 'Masuk' }]
            })
        } catch (exception) {
            console.log(exception)
        }
    }

    const onLogout = async () => {
        Alert.alert('Peringatan',
            'Apakah anda ingin Keluar Aplikasi ini?',
            [{
                text: 'Tidak',
                onPress: () => console.log('Tidak')
            }, {
                text: 'Ya',
                onPress: () => (
                    goOut()
                )
            }])
    }

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={{ flex: 1, flexDirection: 'column' }}>

                {/* item 1 */}
                <View style={[styles.item1, { height: 260 }]}>

                    <View style={{ alignItems: 'center' }}>
                        {/* <Image source={data.file === null ? require('../Assets/drawable-xhdpi/img-notfound.png')
                : {uri: api +'/'+ data.file}} style={{width:140,height:140,borderRadius:140,marginVertical:15}} /> */}
                        <Image source={data.file === null ? require('../Assets/drawable-xhdpi/img-notfound.png')
                            : { uri: data.file }} style={{ width: 140, height: 140, borderRadius: 140, marginVertical: 15 }} />
                        <Text style={{ fontSize: 18, textAlign: 'center' }} >{data.fullname}</Text>
                        <Text>NPP : {data.npp}</Text>
                    </View>
                </View>
                {/* item 2 */}
                <View style={[styles.item1, { height: 200 }]}>

                    {/* <View style={styles.item}>
            <Text style={styles.container}>NIP ex</Text>
            <Text style={styles.container2}>{data.nip}</Text>
            </View> */}

                    <View style={styles.item}>
                        <Text style={styles.container}>TTL</Text>
                        <Text style={styles.container2}>{data.birth_place}, {data.birth_date}</Text>
                    </View>

                    <View style={styles.item}>
                        <Text style={styles.container}>Jenis Kelamin</Text>
                        <Text style={styles.container2}>{data.gender}</Text>
                    </View>

                    <View style={styles.item}>
                        <Text style={styles.container}>STS YBS</Text>
                        <Text style={styles.container2}>{data.sts_ybs}</Text>
                    </View>

                    <View style={styles.item}>
                        <Text style={styles.container}>No. Telp</Text>
                        {
                            KontakPhone.map((phone, index) => {
                                return <Text style style={styles.container2} key={index} >{phone.value}</Text>
                            })
                        }
                    </View>
                </View>
                {/* item 3 */}
                <View style={[styles.item1, { height: 250 }]}>
                    <View style={styles.item}>
                        <Text style={styles.container}>Provinsi</Text>
                        <Text style={styles.container2}>{data.province_id}</Text>
                    </View>

                    <View style={styles.item}>
                        <Text style={styles.container}>Kota / Kabupaten</Text>
                        <Text style={styles.container2}>{data.city_id}</Text>
                    </View>

                    <View style={styles.item}>
                        <Text style={styles.container}>Kecamatan</Text>
                        <Text style={styles.container2}>{data.district_id}</Text>
                    </View>

                    <View style={styles.item}>
                        <Text style={styles.container}>Kelurahan</Text>
                        <Text style={styles.container2}>{data.subdistrict_id}</Text>
                    </View>

                    <View style={styles.item}>
                        <Text style={styles.container}>Kode POS</Text>
                        <Text style={styles.container2}>{data.postcode}</Text>
                    </View>

                    <View style={styles.item}>
                        <Text style={styles.container}>Alamat Lengkap</Text>
                        {/* <Text style={styles.container2}>{data.address_detail}</Text> */}
                        <Text style={styles.container2}>{data.address_1}</Text>
                    </View>
                </View>
                {/* item 4 */}
                {/* <View style={[styles.item1,{height:200}]}>
            <View style={{alignItems:'center'}}>
            <Text>Rumah Duka</Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>Nama Rumah Duka</Text>
            <Text style={styles.container2}> - </Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>No. Telp</Text>
            {
                KontakPhone.map((phone, index)=>{
                return <Text style style={styles.container2} key={index} >{phone.value}</Text>
                })
            }
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>Alamat</Text>
            <Text style={styles.container2}>{data.address_detail}</Text>
            </View>
        </View> */}
                {/* item 5 */}
                <View style={[styles.item1, { height: 60 }]}>
                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => navigation.navigate('GantiPass')}>
                        <Text style={{ color: 'green' }}>Ubah PIN</Text>
                    </TouchableOpacity>
                </View>
                <View style={[styles.item1, { height: 60, marginBottom: 30 }]}>
                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={() => onLogout()}>
                        <Text style={{ color: 'red' }}>Keluar</Text>
                    </TouchableOpacity>
                </View>

            </ScrollView>
        </SafeAreaView>
    )
}

export default Profile

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    container2: {
        flex: 1,
        textAlign: 'right'
    },
    item1: {
        backgroundColor: 'white',
        padding: 20,
        flexDirection: 'column',
        width: '100%',
        justifyContent: 'space-between',
        marginBottom: 10
    },
    item: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
})
