import React, { useEffect, useRef } from 'react';
import { Animated, Dimensions, Image, SafeAreaView, StatusBar, StyleSheet } from 'react-native';
import colors from '../Style/Colors';

const height = Dimensions.get('window').height

const Splashscreens = () => {

    const fadeIn = useRef(new Animated.Value(0)).current

    useEffect(() => {
        Animated.timing(fadeIn, {
            toValue: 1,
            duration: 3000,
            useNativeDriver: false
        }).start()
    }, [fadeIn])

    const transformY = fadeIn.interpolate({
        inputRange: [0, 0],
        outputRange: [height, height / 50]
    })

    return (
        <SafeAreaView style={styles.container}>
            <Image source={require('../Assets/drawable-xhdpi/Group253.png')} style={{ flex: 1, width: '100%', height: '100%', position: 'absolute', marginTop: 100 }} />
            <StatusBar backgroundColor={'white'} barStyle="dark-content" />
            <Animated.View style={[styles.quotesContainer, { opacity: fadeIn, transform: [{ translateY: transformY }] }]}>
                {/* <Image source={require('../Assets/drawable-xhdpi/LogoIkapurna.png')} 
                    style={{width:120,height:140}}  /> */}
                <Image source={require('../Assets/MainLogo.png')} style={{ width: 150, height: 170, marginTop: 30 }} />
                <Image source={require('../Assets/drawable-xhdpi/Group101.png')} style={{ width: 130, height: 50 }} />
            </Animated.View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: colors.blue
    },
    quotesContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-around',
    },
})

export default Splashscreens