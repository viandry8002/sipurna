import React from 'react'
import { Image, StatusBar, StyleSheet, View } from 'react-native'

const Background = () => {
    return (
        <View>
         <Image source={require('../Assets/drawable-xhdpi/Group253.png')} style={{flex:1,width:'100%',height:'100%',position:'absolute',marginTop:100}}/>
         <StatusBar translucent backgroundColor='transparent' barStyle="light-content" />
        </View>
    )
}

export default Background
