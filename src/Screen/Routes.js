import AsyncStorage from '@react-native-async-storage/async-storage';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import api from '../Api';
import Berita from './Berita';
import Faq from './BtnHeader/Faq';
import Notif from './BtnHeader/Notif';
import NotifDetail from './BtnHeader/NotifDetail';
import Daftar from './Daftar';
import Home from './Home';
import HubungiKami from './HubungiKami';
import Intro from './Intro';
import Masuk from './Masuk';
import DtlKepemimpinan from './MnBTN/DtlKepemimpinan';
import Kepemimpinan from './MnBTN/Kepemimpinan';
import KontakBTN from './MnBTN/KontakBTN';
import GantiPass from './GantiPass';
//import BTN
import Btn from './MnBTN/LmnBTN';
import FinansialDapen from './MnDanaPensiun/FinansialDapen';
import InfoDapen from './MnDanaPensiun/InfoDapen';
import KegiatanDpn from './MnDanaPensiun/KegiatanDpn';
import KontakDapen from './MnDanaPensiun/KontakDapen';
import KeuanganBTN from './MnBTN/KeuanganBTN';
import DtlKeuanganBTN from './MnBTN/DtlKeuanganBTN';
import InfoBTN from './MnBTN/InfoBTN';
//import dapen
import DanaPensiun from './MnDanaPensiun/LmnDanaPensiun';
import ManfaatPasti from './MnDanaPensiun/ManfaatPasti';
import InfoKegiatan from './MnIkapurna/InfoKegiatan';
import PengurusDapen from './MnDanaPensiun/PengurusDapen';
// import ikapurna
import Ikapurna from './MnIkapurna/LmnIkapurna';
import Pengda from './MnIkapurna/Pengda';
import PengurusPusat from './MnIkapurna/PengurusPusat';
import KegiatanYKP from './MnYKP/InfoKegiatan';
import InfoYKP from './MnYKP/InfoYKP';
import JaminanRS from './MnYKP/JaminanRS';
import Keluarga from './MnYKP/Keluarga';
import Kesehatan from './MnYKP/Kesehatan';
import KontakYKP from './MnYKP/KontakYKP';
// import YKP
import Ykp from './MnYKP/LmnYKP';
import Outstanding from './MnYKP/Outstanding';
import PengurusYKP from './MnYKP/PengurusYKP';
import Plafond from './MnYKP/Plafond';
import Reimbus from './MnYKP/Reimbus';
import Rs from './MnYKP/Rs';
import RsDtlBPJS from './MnYKP/RsDtlBPJS';
import RsDtlYKP from './MnYKP/RsDtlYKP';
import MultiDetailKegaitan from './MultiFunct/DtlKegiatan';
import Profile from './Profile';
import SplashScreens from './Splashscreen';
import SuratJaminanRs from './MnYKP/SuratJaminanRs';
import HistorySJRS from './MnYKP/HistorySJRS';
import DtlHistorySJRS from './MnYKP/HistorySJRS/DtlHistorySJRS';
import SjrsPDF from './MnYKP/HistorySJRS/SjrsPDF';
// import Multi Function
import OnKick from './MultiFunct/OnKick';
import Tentang from './MultiFunct/TentangAllMenu';
import FormKorlap from './FormKorlap';
// import multi screen
import AnakPerusahaan from '../Screen/MultipleScreen/AnakPerusahaan';
import DtlAnakPerusahaan from '../Screen/MultipleScreen/AnakPerusahaan/DtlAnakPerusahaan';
import ProfiePerusahaan from '../Screen/MultipleScreen/AnakPerusahaan/ProfilePerusahaan';
import KontakAP from '../Screen/MultipleScreen/AnakPerusahaan/Kontak';
import LapKeuAP from './MultipleScreen/AnakPerusahaan/LapKeuAP';
import DtlLapKeuAP from './MultipleScreen/AnakPerusahaan/DtlLapKeuAP';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const ImageHeader = () => (
  <Image style={{ width: '100%', height: '100%' }}
    source={require('../Assets/drawable-xhdpi/blue-44.png')} />
)

const StyleHeader = (headttl) => ({
  title: headttl,
  headerTitleAlign: 'center',
  headerBackground: () => ImageHeader(),
  headerTintColor: '#fff',
})

function HomeStackScreen({ navigation }) {
  const [names, setNames] = useState('')
  const [datanotif, setDataNotif] = useState(false)

  useEffect(() => {
    function getToken() {
      try {
        var myVar = setInterval(myTimer, 5000);

        async function myTimer(myVar) {
          const token = await AsyncStorage.getItem("api_token")
          const id = await AsyncStorage.getItem("user_id")
          const name = await AsyncStorage.getItem("fullname")
          //  return getVenue(token,id)

          if (token === null) {
            clearInterval(myVar)
          } else {
            setNames(name)
            return getVenue(token, id, name)
          }
        }
      } catch (err) {
        console.log(err)
      }
    }
    getToken()
  }, [])


  const getVenue = (token, id) => {
    Axios.get(`${api}/api/notification/${id}`, {
      headers: {
        'Authorization': 'Bearer ' + token,
        'Accept': 'application/json'
      }
    }).then((res) => {
      res.data.data.map((dats, index) => {
        if (dats.is_read === '0') {
          setDataNotif(true)
        } else {
          setDataNotif(false)
        }
      })
    }).catch((err) => {
      console.log(err.response.data)
      if (err.response.data === 'Unauthorized.') {
        navigation.navigate('OnKick')
      }
    })
  }

  return (
    <Stack.Navigator>
      <Stack.Screen name="Home"
        component={Home}
        options={({ navigation }) => ({
          headerLeft: null,
          headerRight: () =>
          (
            <View style={styles.iconContainer}>
              <TouchableOpacity onPress={() => navigation.navigate('Faq')}>
                <FontAwesome5 name='info-circle' size={30} style={{ color: 'white' }} />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigation.navigate('Notif')}>
                <FontAwesome5 name='bell' size={30} style={{ color: 'white' }} />
                {
                  datanotif === true ? (
                    <View style={{ backgroundColor: 'red', width: 10, height: 10, borderRadius: 15, position: 'absolute', marginLeft: 15 }} />
                  ) : <View style={{ width: 10, height: 10, borderRadius: 10, position: 'absolute', marginLeft: 15 }} />
                }

              </TouchableOpacity>
            </View>
          ),
          title: 'Hai! ' + names,
          headerTitleStyle: { width: 250 },
          headerBackground: () => ImageHeader(),
          headerTintColor: '#fff',
        })} />

      <Stack.Screen name="Berita" component={Berita}
        options={() => StyleHeader('Berita Terbaru')}
      />
      <Stack.Screen name="DtlBerita" component={MultiDetailKegaitan}
        options={() => StyleHeader('Berita Terbaru')}
      />
      <Stack.Screen name="FormKorlap" component={FormKorlap}
        options={() => StyleHeader('Form KORLAP')}
      />
      {/* ikapurna list */}
      <Stack.Screen name="LmnIkapurna" component={Ikapurna}
        options={() => StyleHeader('IKAPURNA Bank BTN')}
      />
      <Stack.Screen name="PengurusPusat" component={PengurusPusat}
        options={() => StyleHeader('Data Pengurus Pusat')}
      />
      <Stack.Screen name="Pengda" component={Pengda}
        options={() => StyleHeader('Data Pengda')}
      />

      <Stack.Screen name="InfoKegiatan" component={InfoKegiatan}
        options={() => StyleHeader('Informasi Kegiatan')}
      />
      <Stack.Screen name="DtlInfoKegiatan" component={MultiDetailKegaitan}
        options={() => StyleHeader('Informasi Kegiatan')}
      />
      {/* ykp list */}
      <Stack.Screen name="LmnYKP" component={Ykp}
        options={() => StyleHeader('YKP Bank BTN')}
      />
      <Stack.Screen name="Kesehatan" component={Kesehatan}
        options={() => StyleHeader('Kesehatan')}
      />
      <Stack.Screen name="Plafond" component={Plafond}
        options={() => StyleHeader('Kesehatan')}
      />
      <Stack.Screen name="Outstanding" component={Outstanding}
        options={() => StyleHeader('Kesehatan')}
      />
      <Stack.Screen name="Reimbus" component={Reimbus}
        options={() => StyleHeader('Reimburse')}
      />
      <Stack.Screen name="JaminanRS" component={JaminanRS}
        options={() => StyleHeader('Jaminan Rumah Sakit')}
      />

      <Stack.Screen name="Rs" component={Rs}
        options={() => StyleHeader('Rumah Sakit')}
      />
      <Stack.Screen name="RsDtlYKP" component={RsDtlYKP}
        options={() => StyleHeader('Rumah Sakit')}
      />
      <Stack.Screen name="RsDtlBPJS" component={RsDtlBPJS}
        options={() => StyleHeader('Rumah Sakit')}
      />

      <Stack.Screen name="Keluarga" component={Keluarga}
        options={() => StyleHeader('Data Keluarga')}
      />
      <Stack.Screen name="InfoYKP" component={InfoYKP}
        options={() => StyleHeader('Informasi YKP Bank BTN')}
      />
      <Stack.Screen name="PengurusYKP" component={PengurusYKP}
        options={() => StyleHeader('Pengurus YKP Bank BTN')}
      />
      <Stack.Screen name="KegiatanYKP" component={KegiatanYKP}
        options={() => StyleHeader('Informasi YKP Bank BTN')}
      />
      <Stack.Screen name="DtlKegaitanYKP" component={MultiDetailKegaitan}
        options={() => StyleHeader('Informasi YKP Bank BTN')}
      />
      <Stack.Screen name="KontakYKP" component={KontakYKP}
        options={() => StyleHeader('Kontak YKP Bank BTN')}
      />

      <Stack.Screen name="SuratJaminanRs" component={SuratJaminanRs}
        options={() => StyleHeader('Input Surat Jaminan RS')}
      />
      <Stack.Screen name="HistorySJRS" component={HistorySJRS}
        options={() => StyleHeader('Histori Input Surat Jaminan RS')}
      />
      <Stack.Screen name="DtlHistorySJRS" component={DtlHistorySJRS}
        options={() => StyleHeader('Histori Input Surat Jaminan RS')}
      />
      <Stack.Screen name="SjrsPDF" component={SjrsPDF}
        options={() => StyleHeader('Histori Input Surat Jaminan RS')}
      />

      {/* dana pensiun list */}
      <Stack.Screen name="LmnDanaPensiun" component={DanaPensiun}
        options={() => StyleHeader('Dana Pensiun')}
      />
      <Stack.Screen name="ManfaatPasti" component={ManfaatPasti}
        options={() => StyleHeader('Manfaat Pasti')}
      />
      <Stack.Screen name="InfoDapen" component={InfoDapen}
        options={() => StyleHeader('Informasi Dapen')}
      />
      <Stack.Screen name="KegiatanDpn" component={KegiatanDpn}
        options={() => StyleHeader('Informasi Dapen')}
      />
      <Stack.Screen name="DtlKegiatanDpn" component={MultiDetailKegaitan}
        options={() => StyleHeader('Informasi Dapen')}
      />
      <Stack.Screen name="FinansialDapen" component={FinansialDapen}
        options={() => StyleHeader('Finansial Dapen')}
      />
      <Stack.Screen name="KontakDapen" component={KontakDapen}
        options={() => StyleHeader('Kontak Dapen')}
      />
      <Stack.Screen name="PengurusDapen" component={PengurusDapen}
        options={() => StyleHeader('Pengurus Dapen')}
      />
      {/* btn list */}
      <Stack.Screen name="LmnBTN" component={Btn}
        options={() => StyleHeader('Bank BTN')}
      />
      <Stack.Screen name="Kepemimpinan" component={Kepemimpinan}
        options={() => StyleHeader('Manajemen')}
      />
      <Stack.Screen name="DtlKepemimpinan" component={DtlKepemimpinan}
        options={() => StyleHeader('Manajemen')}
      />
      <Stack.Screen name="KeuanganBTN" component={KeuanganBTN}
        options={() => StyleHeader('Data Keuangan Bank BTN')}
      />
      <Stack.Screen name="DtlKeuanganBTN" component={DtlKeuanganBTN}
        options={() => StyleHeader('Data Keuangan Bank BTN')}
      />
      <Stack.Screen name="InfoBTN" component={InfoBTN}
        options={() => StyleHeader('Informasi Bank BTN')}
      />
      <Stack.Screen name="KontakBTN" component={KontakBTN}
        options={() => StyleHeader('Kontak Bank BTN')}
      />
      <Stack.Screen name="Faq" component={Faq}
        options={() => StyleHeader('FAQ')}
      />
      {/* Multi funct */}
      <Stack.Screen name="Tentang" component={Tentang}
        options={({ route }) => StyleHeader('Tentang ' + route.params.title)}
      />
    </Stack.Navigator>
  );
}

function CsStackScreen() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="HubungiKami"
        component={HubungiKami}
        options={() => StyleHeader('Customer Service')}
      />
    </Stack.Navigator>
  );
}

function ProfileStackScreen() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Profile"
        component={Profile}
        options={() => StyleHeader('Profile')}
      />
      <Stack.Screen name="GantiPass" component={GantiPass}
        options={() => StyleHeader('Ubah PIN')}
      />
    </Stack.Navigator>
  );
}

const MainTabNavigation = () => {
  return (
    <Tab.Navigator
      initialRouteName={'Homes'}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          let ukuran;

          if (route.name === 'Homes') {
            iconName = require('../Assets/drawable-xhdpi/home.png');
            ukuran = { width: 75, height: 75, marginBottom: 15 }
            return <Image source={iconName} style={ukuran} />;
          } else if (route.name === 'Profile') {
            return <AntDesign name='user' size={30} style={{ color: 'white' }} />
          } else if (route.name === 'CS') {
            return <AntDesign name='customerservice' size={30} style={{ color: 'white' }} />
          }

        },

      })}
      tabBarOptions={{
        activeTintColor: 'white',
        inactiveTintColor: 'white',
        style: {
          backgroundColor: '#2C5BA4',
        }
      }}
    >
      <Tab.Screen name="CS" component={CsStackScreen} options={{ title: 'Customer Services' }} />
      <Tab.Screen name="Homes" component={HomeStackScreen} options={{ title: '' }} />
      <Tab.Screen name="Profile" component={ProfileStackScreen} options={{ title: 'Profile' }} />
    </Tab.Navigator>
  )
}

const MainNavigation = (props) => (
  <Stack.Navigator initialRouteName={props.token !== null ? 'Home' : 'Intro'}>
    <Stack.Screen name="Intro" component={Intro}
      options={{
        headerShown: false
      }}
    />
    <Stack.Screen name="Masuk" component={Masuk}
      options={{
        headerShown: false
      }}
    />
    <Stack.Screen name="Daftar" component={Daftar}
      options={{
        headerShown: false
      }}
    />
    <Stack.Screen name="HubungiKami"
      component={HubungiKami}
      options={() => StyleHeader('Customer Service')}
    />
    <Stack.Screen name="Home"
      component={MainTabNavigation}
      options={{
        headerShown: false
      }}
    />
    <Stack.Screen name="Notif" component={Notif}
      options={() => StyleHeader('Notifikasi')}
    />
    <Stack.Screen name="NotifDetail" component={NotifDetail}
      options={({ navigation }) => ({
        title: 'Notifikasi',
        headerTitleAlign: 'center',
        headerBackground: () => ImageHeader(),
        headerTintColor: '#fff',
        headerLeft: () => (<TouchableOpacity onPress={() => navigation.navigate('Home')}
        ><AntDesign name='arrowleft' size={30} style={{ color: 'white', marginLeft: 10 }} /></TouchableOpacity>)
      })}
    />
    {/* Multi Screen */}
    <Stack.Screen name="AnakPerusahaan" component={AnakPerusahaan}
      options={() => StyleHeader('Anak Perusahaan')}
    />
    <Stack.Screen name="DtlAnakPerusahaan" component={DtlAnakPerusahaan}
      options={() => StyleHeader('')}
    />
    <Stack.Screen name="ProfiePerusahaan" component={ProfiePerusahaan}
      options={() => StyleHeader('Profile Perusahaan')}
    />
    <Stack.Screen name="KontakAP" component={KontakAP}
      options={() => StyleHeader('Kontak')}
    />
    <Stack.Screen name="LapKeuAP" component={LapKeuAP}
      options={() => StyleHeader('Laporan Keuangan')}
    />
    <Stack.Screen name="DtlLapKeuAP" component={DtlLapKeuAP}
      options={() => StyleHeader('Laporan Keuangan')}
    />
    <Stack.Screen name="OnKick" component={OnKick}
      options={() => StyleHeader('Peringatan')}
    />
  </Stack.Navigator>
)

const Routes = () => {
  const [isLoading, setIsLoading] = useState(true)
  const [token, setToken] = useState(null)

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(!isLoading)
    }, 3000)

    async function getToken() {
      const token = await AsyncStorage.getItem('api_token')
      setToken(token)
    }
    getToken()

  }, [])

  if (isLoading) {
    return <SplashScreens />
  }

  return (
    <NavigationContainer>
      <MainNavigation token={token} />
    </NavigationContainer>
  )
}

export default Routes

const styles = StyleSheet.create({
  iconContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    width: 120
  }
})
