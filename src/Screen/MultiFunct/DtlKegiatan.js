import React, { useState } from 'react';
import { Image, SafeAreaView, Text, View,ScrollView } from 'react-native';
import HTML from "react-native-render-html";

const DtlKegiatan = ({route}) => {
    const [data, setData] = useState(route.params)
    return (
        <ScrollView style={{flex:1,backgroundColor: 'white'}}>
        <View style={{padding: 20,flexDirection:'column',width: '100%',height:'100%',marginBottom:10}}>

        <Image 
        source={data.file === '' ? require('../../Assets/drawable-xhdpi/img-notfound.png')
        : {uri: data.file}}
        style={{
            width: '100%',
            height: 160,
            borderRadius:10
        }} />
            
        <View style={{marginTop:20}}>
        <Text style={{fontSize: 16,width:222,height:39,marginVertical:15}}>{data.title}</Text>
        <HTML source={{ html: data.description === '' ? '-' : data.description}}  />
        </View>

        </View>
        </ScrollView>
    )
}

export default DtlKegiatan

