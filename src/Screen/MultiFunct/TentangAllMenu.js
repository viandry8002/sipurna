import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import api from '../../Api';
import HTML from 'react-native-render-html';

const TentangAllMenu = ({route}) => {
  const [parm, setparm] = useState(route.params.name);
  const [data, setData] = useState([]);

  useEffect(() => {
    async function getToken() {
      try {
        const token = await AsyncStorage.getItem('api_token');
        return getVenue(token);
      } catch (err) {
        console.log(err);
      }
    }
    getToken();
  }, []);

  const getVenue = (token) => {
    Axios.get(`${api}/api/configuration/${parm}`, {
      timeout: 20000,
      headers: {
        Authorization: 'Bearer ' + token,
        Accept: 'application/json',
      },
    })
      .then((res) => {
        setData(res.data.data);
      })
      .catch((err) => {
        console.log(err.response.data);
        if (err.response.data === 'Unauthorized.') {
          navigation.navigate('OnKick');
        }
      });
  };

  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <ScrollView>
        <View style={{margin: 20}}>
          <HTML
            source={{html: data.about_us === '' ? '-' : data.about_us}}
            // renderers={{
            //   p: (_, children) => <Text numberOfLines={1}>{children}</Text>,
            // }}
          />
        </View>
        {route.params.name === 'btn' ? (
          <View></View>
        ) : route.params.name === 'ykp' ? (
          <View style={{marginHorizontal: 20, marginBottom: 40}}>
            <Text style={styles.title1}>Maksud dan Tujuan</Text>
            <HTML
              source={{html: data.maksud_tujuan ? data.maksud_tujuan : '-'}}
            />
          </View>
        ) : (
          <View style={{marginHorizontal: 20, marginBottom: 40}}>
            <Text style={styles.title1}>Visi</Text>
            <HTML source={{html: data.vision ? data.vision : '-'}} />
            <Text style={styles.title1}>Misi</Text>
            <HTML source={{html: data.mission ? data.mission : '-'}} />
          </View>
        )}
      </ScrollView>
    </View>
  );
};

export default TentangAllMenu;

const styles = StyleSheet.create({
  title1: {
    fontSize: 18,
    marginVertical: 10,
  },
  fontText: {
    fontSize: 15,
  },
});
