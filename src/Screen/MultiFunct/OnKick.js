import React,{useEffect} from 'react'
import { Alert, View } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage';
import OneSignal from 'react-native-onesignal';

const OnKick = ({navigation}) => {

    useEffect(() => {
            KickLogout()
    }, [])

    const goLogout  = async () => {
        try {
        await AsyncStorage.removeItem("api_token")
        await AsyncStorage.removeItem("user_id")
        await AsyncStorage.removeItem("npp")
        await AsyncStorage.removeItem("fullname")
        OneSignal.removeExternalUserId((results) => {
            // The results will contain push and email success statuses
            console.log('Results of removing external user id');
            console.log(results);
        })
         console.log('remove item success')
        }catch(exception) {
            console.log(exception)
        }

        navigation.reset({
            index : 0,
            routes : [{name : 'Masuk'}]
          })  
    }

    const KickLogout = () => {
        Alert.alert('Pemberitahuan',
        'Maaf akun anda telah terpakai silahkan Logout Terlebih dahulu',
            [{
                text : 'Ya',
                onPress : () => goLogout()
            }]) 
    }

    return (
        <View>
        </View>
    )
}

export default OnKick
