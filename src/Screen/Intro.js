import React from 'react';
import {
  View,
  Text,
  Image,
  StatusBar,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import colors from '../Style/Colors';
import AppIntroSlider from 'react-native-app-intro-slider';
const screenWidth = Dimensions.get('screen').width;

const data = [
  {
    id: 1,
    image: require('../Assets/drawable-xhdpi/onb-1.png'),
    description: 'Sarana komunikasi pemersatu purnakaryawan Bank BTN',
  },
  {
    id: 2,
    image: require('../Assets/drawable-xhdpi/onb-2.png'),
    description:
      'Apapun informasi yang anda butuhkan diIkapurna ada dalam genggaman anda',
  },
  // {
  //     id: 3,
  //     image: require('../Assets/drawable-xhdpi/onb-3.png'),
  //     description: 'Tetap Bahagia dalam semangat kebersamaan di dalam naungan keluarga besar Bank BTN'
  // }
  {
    id: 3,
    image: require('../Assets/drawable-xhdpi/onb3bck.jpeg'),
    description: '',
  },
];

const Intro = ({navigation}) => {
  const renderItem = ({item}) => {
    return (
      <View style={styles.listContainer}>
        <View style={styles.listContent}>
          <Image
            source={item.image}
            style={
              item.id == 3
                ? {
                    width: screenWidth,
                    height: '100%',
                  }
                : styles.imgList
            }
            // resizeMethod="auto"
            resizeMode="contain"
          />
          {/* <Image
            source={item.image}
            style={styles.imgList}
            resizeMethod="auto"
            resizeMode="contain"
          /> */}
        </View>
        <Text style={styles.textList}>{item.description}</Text>
      </View>
    );
  };

  const _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Image
          source={require('../Assets/drawable-xhdpi/Lewati_btn.png')}
          style={{width: 180, height: 70}}
        />
      </View>
    );
  };
  const _renderDoneButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <TouchableOpacity onPress={() => navigation.navigate('Masuk')}>
          <Image
            source={require('../Assets/drawable-xhdpi/Mulai_btn.png')}
            style={{width: 180, height: 70}}
          />
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <Image
        source={require('../Assets/drawable-xhdpi/Group253.png')}
        style={{
          flex: 1,
          width: '100%',
          height: '100%',
          position: 'absolute',
          marginTop: 100,
        }}
      />
      <View style={{backgroundColor: 'rgba(255,255,255,0.8)', flex: 1}}>
        <StatusBar backgroundColor={'white'} barStyle="dark-content" />
        <View style={styles.slider}>
          <AppIntroSlider
            data={data}
            renderItem={renderItem}
            renderNextButton={() => _renderNextButton()}
            renderDoneButton={() => _renderDoneButton()}
            activeDotStyle={styles.activeDotStyle}
            keyExtractor={(item) => item.id.toString()}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  image: {
    flex: 1,
    flexDirection: 'column',
    marginTop: 200,
  },
  slider: {
    paddingTop: 20,
    paddingBottom: 150,
    flex: 1,
  },
  listContainer: {
    margin: 20,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  listContent: {
    marginTop: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgList: {
    width: 280,
    height: 280,
  },
  textList: {
    margin: 20,
    paddingHorizontal: 25,
    textAlign: 'center',
    fontSize: 16,
    color: colors.black,
  },
  activeDotStyle: {
    width: 20,
    backgroundColor: '#2C5BA4',
  },
  buttonCircle: {
    width: 360,
    alignItems: 'center',
    justifyContent: 'center',
    // margin:20,
    // alignItems: 'center',
    // justifyContent: 'center',

    marginTop: 110,
    // paddingTop:120,
    // paddingHorizontal:95,
    // position:'absolute',
    //  paddingBottom:500
  },
});

export default Intro;
