import React, { useState, useEffect } from 'react'
import { FlatList, Image, SafeAreaView, StatusBar, StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import colors from '../../../../Style/Colors';
import { Button } from 'react-native-elements';

const menu = [
    {
        id: 1,
        title: 'Bidang Usaha',
    },
    {
        id: 2,
        title: 'Komisaris',
    },
    {
        id: 3,
        title: 'Direksi',
    },
    {
        id: 4,
        title: 'Kepemilikan',
    },

]

const index = ({ navigation, route }) => {
    const [chose, setChose] = useState(1)
    const [data, setData] = useState([])
    const [dataK, setDataK] = useState([])
    const [dataD, setDataD] = useState([])
    const [dataKepemilikan, setDataKepemilikan] = useState('')
    const [isFetching, setIsFetching] = useState(false)

    useEffect(() => {
        setData(route.params.data.bidang)
        setDataK(route.params.data.komisaris)
        setDataD(route.params.data.direksi)
    }, [])

    const pilih = (idm) => {
        setIsFetching(true),
            setChose(idm),
            setIsFetching(false)
    }

    const renderItem1 = ({ item }) => (
        <View>
            {
                chose === item.id ?
                    <Button title={item.title} buttonStyle={{ backgroundColor: '#2C5BA4', marginRight: 10 }} onPress={() => pilih(item.id)} />
                    :
                    <Button title={item.title} type="outline" titleStyle={{ color: 'black' }} buttonStyle={{ marginRight: 10 }} onPress={() => pilih(item.id)} />
            }
        </View>
    );

    const renderItem2 = ({ item }) => (
        <View style={{ margin: 16 }}>
            <Text>- {item.nama}</Text>
        </View>
    );

    const renderItem3 = ({ item }) => (
        <View style={styles.item}>
            <Image source={item.file === null ? require('../../../../Assets/drawable-xhdpi/img-notfound.png') : item.file} style={{ width: 140, height: 140, borderRadius: 10, marginHorizontal: 10, marginTop: 10 }} />
            <View style={styles.containerflatList}>
                <Text style={styles.title1}>{item.nama}</Text>
                <Text style={styles.title2}>{item.jabatan}</Text>
                {/* <Text style={styles.title3}>Handphone : </Text>
                <Text style={styles.title3}>{item.phone}</Text> */}
            </View>
        </View>
    );

    return (
        <SafeAreaView style={styles.container}>
            <View style={{ flex: 1 }}>

                <View style={{ padding: 16, backgroundColor: colors.white }}>
                    <FlatList
                        data={menu}
                        renderItem={renderItem1}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}
                    />
                </View>

                <View style={{ flex: 1, backgroundColor: colors.white, marginTop: 10 }}>
                    <FlatList
                        data={chose === 1 ? data : (chose === 2 ? dataK : (chose === 3 ? dataD : dataKepemilikan))}
                        renderItem={chose === 1 ? renderItem2 : renderItem3}
                        refreshing={isFetching}
                    />
                </View>

            </View>
        </SafeAreaView>
    )
}

export default index


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    btnMenu: {
        width: '100%',
        height: 70
    },
    item: {
        backgroundColor: 'white',
        padding: 10,
        marginVertical: 3,
        flexDirection: 'row',
        flex: 1
    },
    containerflatList: {
        flexDirection: 'column',
        flex: 1,
        margin: 5
    },
    title1: {
        fontSize: 18,
        marginTop: 10,
        marginBottom: 5
    },
    title2: {
        color: '#669F16',
        marginBottom: 15
    },
    title3: {
        color: '#919191',
    },
})


