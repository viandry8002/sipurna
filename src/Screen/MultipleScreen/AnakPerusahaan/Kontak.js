import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Image, SafeAreaView, StatusBar, Text, View, Linking, TouchableOpacity, ScrollView } from 'react-native';
import api from '../../../Api';
import styles from '../../../Style/Kontak';

const KontakYKP = ({ navigation, route }) => {
    const [data, setData] = useState([])

    useEffect(() => {
        // console.log('aaaaa', route.params)
        setData(route.params)
    }, [])

    return (
        <SafeAreaView style={styles.container}>
            <Image source={require('../../../Assets/drawable-xhdpi/Group253.png')} style={{ flex: 1, width: '100%', height: '100%', position: 'absolute', marginTop: 100 }} />
            <View style={{ backgroundColor: 'rgba(255,255,255,0.8)', flex: 1 }}>
                <StatusBar translucent backgroundColor='transparent' barStyle="light-content" />

                <ScrollView>
                    <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 20, marginBottom: 50 }}>
                        <Image source={require('../../../Assets/drawable-xhdpi/cs.png')} style={styles.cs} />
                        {/* box Kontak */}
                        <View style={styles.contact2}>
                            <Image source={require('../../../Assets/drawable-xhdpi/surface1.png')} style={{
                                width: 25,
                                height: 35
                            }} />
                            <Text style style={styles.font} >{data.alamat}</Text>
                        </View>
                        <View style={styles.contact2}>
                            <Image source={require('../../../Assets/drawable-xhdpi/telephone.png')} style={styles.ImageIcon2} />
                            <View style={{ flexDirection: 'column' }}>
                                <TouchableOpacity onPress={() => Linking.openURL(`tel:${data.tlp}`)}><Text style style={styles.font}>{data.tlp}</Text></TouchableOpacity>
                            </View>
                        </View>

                        <View style={styles.contact2}>
                            <Image source={require('../../../Assets/drawable-xhdpi/fax.png')} style={styles.ImageIcon} />
                            <View style={{ flexDirection: 'column' }}>
                                <TouchableOpacity onPress={() => Linking.openURL(`tel:${data.faks}`)}><Text style style={styles.font}>{data.tlp}</Text></TouchableOpacity>
                            </View>
                        </View>

                        {/* <View style={styles.contact2}>
                            <Image source={require('../../Assets/drawable-xhdpi/phone.png')} style={{width:20,
                            height:35}}/>
                            <View style={{ flexDirection: 'column' }}>
                                <TouchableOpacity onPress={() => Linking.openURL(`tel:${data.tlp}`)}><Text style style={styles.font}>{data.tlp}</Text></TouchableOpacity>
                            </View>
                        </View> */}
                        {/* box Kontak */}
                    </View>
                </ScrollView>
            </View>
        </SafeAreaView>
    )
}

export default KontakYKP
