import React, { useState, useEffect } from 'react'
import { StyleSheet, SafeAreaView, ActivityIndicator } from 'react-native'
import PDFView from 'react-native-view-pdf';

const DtlLapKeuAP = ({ route }) => {
    // const [data, setData] = useState(route.params)
    const [data, setData] = useState(route.params)
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, [])

    const resources = {
        file: Platform.OS === 'ios' ? 'downloadedDocument.pdf' : '/sdcard/Download/downloadedDocument.pdf',
        url: route.params.file,
        base64: 'JVBERi0xLjMKJcfs...',
    };

    const resourceType = 'url';
    return (
        <SafeAreaView style={styles.container}>
            {loading ? (
                <ActivityIndicator
                    visible={loading}
                    textContent={'Loading...'}
                    style={{ alignSelf: 'center', marginTop: '50%', position: 'absolute' }}
                    size="large"
                    color="#2C5BA4"
                />
            ) : (
                <>
                    <PDFView
                        fadeInDuration={250.0}
                        style={{ flex: 1 }}
                        resource={resources[resourceType]}
                        resourceType={resourceType}
                        onLoad={() => console.log(`PDF rendered from ${resourceType}`)}
                        onError={(error) => console.log('Cannot render PDF', error)}
                    />
                </>
            )}
        </SafeAreaView>
    )
}

export default DtlLapKeuAP

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
})
