import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay';
import colors from '../../../Style/Colors';

const SpinnerLoad = ({ loads }) => {
    return (
        <Spinner
            visible={loads}
            textContent={'Loading...'}
            textStyle={{ color: colors.darkblue }}
        />
    )
}

export default SpinnerLoad