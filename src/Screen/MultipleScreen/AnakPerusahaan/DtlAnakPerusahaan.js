import React, { useState, useEffect } from 'react'
import { Image, SafeAreaView, StatusBar, StyleSheet, TouchableOpacity, View } from 'react-native';
import colors from '../../../Style/Colors';

const DtlAnakPerusahaan = ({ navigation, route }) => {
    const [type, settype] = useState('')
    const [kontak, setKontak] = useState('')
    const [data, setdata] = useState([])
    const [lapKeu, setlapKeu] = useState([])

    useEffect(() => {
        navigation.setOptions({ title: route.params.data.title });
        settype(route.params.type)
        setdata(route.params.data)
        console.log(route.params.data.lapKeu)
        setlapKeu(route.params.data.lapKeu)
        setKontak(route.params.data.kontak[0])
    }, [])

    return (
        <SafeAreaView style={styles.container}>
            <Image source={require('../../../Assets/drawable-xhdpi/Group253.png')} style={{ flex: 1, width: '100%', height: '100%', position: 'absolute', marginTop: 100 }} />
            <View style={{ backgroundColor: 'rgba(255,255,255,0.8)', flex: 1 }}>
                <StatusBar translucent backgroundColor='transparent' barStyle="light-content" />
                <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 10 }}>
                    {/* box Menu */}
                    <View style={{ width: '100%', height: 300, marginTop: 30 }}>

                        <TouchableOpacity onPress={() => navigation.navigate('ProfiePerusahaan', { type: type, data: data })}>
                            <Image source={require('../../../Assets/drawable-xhdpi/ListMenuYKP/AnakPerusahaan/Group119.png')} style={styles.btnMenu} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => navigation.navigate('LapKeuAP', lapKeu)}>
                            <Image source={require('../../../Assets/drawable-xhdpi/ListMenuYKP/AnakPerusahaan/Group120.png')} style={styles.btnMenu} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => navigation.navigate('KontakAP', kontak)}>
                            <Image source={require('../../../Assets/drawable-xhdpi/ListMenuYKP/AnakPerusahaan/Group121.png')} style={styles.btnMenu} />
                        </TouchableOpacity>

                    </View>
                    {/* box Menu */}

                </View>
            </View>
        </SafeAreaView>
    )
}

export default DtlAnakPerusahaan


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    btnMenu: {
        width: '100%',
        height: 70
    }
})


