import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
} from 'react-native';

const dataykp = [
  {
    id: 1,
    title: 'PT Pesona Adi Batara',
    bidang: [
      {nama: 'Property'},
      {nama: 'Sewa kendaraan'},
      {nama: 'EO atau fasilitas diklat'},
      {nama: 'Sewa komputer'},
      {nama: 'Supplier'},
      {nama: 'KSO dengan KJPP'},
    ],
    komisaris: [
      {
        nama: 'Maret DS Santosa',
        file: null,
        jabatan: '',
      },
    ],
    direksi: [
      {
        nama: 'Handoko',
        file: null,
        jabatan: 'Direktur Utama',
      },
    ],
    kontak: [
      {
        alamat: 'Wisma Abadi No 31 Petojo Selatan, Gambir',
        tlp: '',
        faks: '',
      },
    ],
  },
  {
    id: 2,
    title: 'PT Asuransi Binagriya Upakara',
    bidang: [
      {nama: 'Asuransi'},
      {nama: 'Asuransi Harta Benda'},
      {nama: 'Asuransi Kendaraan Bermotor'},
      {nama: 'Asuransi Pengangkutan'},
      {nama: 'Asuransi Kecelakaan Diri'},
      {nama: 'Asuransi Uang'},
      {nama: 'Asuransi Hole-in-One'},
      {nama: 'Asuransi Bulglary'},
      {nama: 'Asuransi Rekayasa Proyek'},
      {nama: 'Asuransi Rekayasa Non-Proyek'},
      {nama: 'Surety Bond'},
      {nama: 'Asuransi Standar Pengiriman Uang Indonesia'},
      {nama: 'Asuransi Kecelakaan Diri Family Pro'},
      {nama: 'Asuransi Kecelakaan Diri Edu Pro'},
      {nama: 'Asuransi Kecelakaan Diri Home Pro'},
      {nama: 'Asuransi Kecelakaan Diri SMEs Pro'},
      {nama: 'Asuransi Standar Penyimpanan Uang Indonesia'},
      {nama: 'Asuransi Gempa Bumi'},
      {nama: 'Asuransi Terorisme dan Sabotase'},
    ],
    komisaris: [
      {
        nama: 'Saut Pardede',
        file: require('../../../Assets/drawable-xhdpi/ListMenuYKP/AnakPerusahaan/ptabgu/saut.png'),
        jabatan: 'Komisaris Utama, merangkap Komisaris Independen',
      },
      {
        nama: 'Suryanti Agustinar',
        file: require('../../../Assets/drawable-xhdpi/ListMenuYKP/AnakPerusahaan/ptabgu/suryanti.png'),
        jabatan: 'Komisaris',
      },
      {
        nama: 'Moro Wijono Budhi',
        file: require('../../../Assets/drawable-xhdpi/ListMenuYKP/AnakPerusahaan/ptabgu/moro.png'),
        jabatan: 'Komisaris Independen',
      },
    ],
    direksi: [
      {
        nama: 'Sri RM Sudarsari',
        file: require('../../../Assets/drawable-xhdpi/ListMenuYKP/AnakPerusahaan/ptabgu/sri.png'),
        jabatan: 'Direktur Utama merangkap Direktur Kepatuhan',
      },
      {
        nama: 'Syamsul Bahri',
        file: require('../../../Assets/drawable-xhdpi/ListMenuYKP/AnakPerusahaan/ptabgu/syamsul.png'),
        jabatan: 'Direktur Teknik',
      },
      {
        nama: 'Mohammad Faiz',
        file: require('../../../Assets/drawable-xhdpi/ListMenuYKP/AnakPerusahaan/ptabgu/mohammad.png'),
        jabatan: 'Direktur Keuangan',
      },
      {
        nama: 'Fitri Novianty Ratna Kusuma',
        file: require('../../../Assets/drawable-xhdpi/ListMenuYKP/AnakPerusahaan/ptabgu/fitri.png'),
        jabatan:
          'Direktur Pemasaran, Efektif setelah lulus fit and proper test oleh OJK',
      },
    ],
    kontak: [
      {
        alamat:
          'Wisma Purna Batara Lantai 4-6 Jl. Kesehatan No. 56-58 Tanah Abang, Jakarta 10160 Indonesia',
        tlp: '+62 21 3483 0348',
        faks: '+62 21 3483 0364',
      },
    ],
    lapKeu: [
      {
        title: 'LK Tw II tahun 2021',
        file:
          'https://backend.sipurna.com/file/laporan-keuangan-tw-ii-2021.pdf',
      },
      {
        title: 'LK Tw I Tahun 2021',
        file:
          'https://backend.sipurna.com/file/laporan-triwulan-tw%20i-2021.pdf',
      },
      {
        title: 'LK Publikasi 2020',
        file: 'https://backend.sipurna.com/file/lk-Publikasi-2020.pdf',
      },
    ],
  },
  {
    id: 3,
    title: 'PT Aplikanusa Lintasarta',
    bidang: [{nama: 'Jaringan dan data center'}],
    komisaris: [
      {
        nama: 'Erwin Riyanto',
        file: null,
        jabatan: 'Komisaris Utama',
      },
      {
        nama: 'Dra. Avilani',
        file: null,
        jabatan: 'Komisaris',
      },
      {
        nama: 'Sindhu Rahadian Ardita',
        file: null,
        jabatan: 'Komisaris',
      },
      {
        nama: 'Intan Sari',
        file: null,
        jabatan: 'Komisaris',
      },
      {
        nama: 'Eyas Naif Saleh Assaf',
        file: null,
        jabatan: 'Komisaris',
      },
      {
        nama: 'Irsyad Sahroni',
        file: null,
        jabatan: 'Komisaris',
      },
      {
        nama: 'Vikram Sinha',
        file: null,
        jabatan: 'Komisaris',
      },
    ],
    direksi: [
      {
        nama: 'Arya Damar',
        file: null,
        jabatan: 'Direktur Utama',
      },
      {
        nama: 'Ginanjar',
        file: null,
        jabatan: 'Direktur',
      },
      {
        nama: 'Afif Asman',
        file: null,
        jabatan: 'Direktur',
      },
      {
        nama: 'Bramudja Hadinoto',
        file: null,
        jabatan: 'Direktur',
      },
      {
        nama: 'Zulfi Hadi',
        file: null,
        jabatan: 'Direktur',
      },
    ],
    kontak: [
      {
        alamat:
          'Menara Thamirn 12th Floor, Jl. M.H. Thamrin Kav 3, Jakarta 10250',
        tlp: '',
        faks: '',
      },
    ],
  },
  {
    id: 4,
    title: 'PT Infoarta Pratama ',
    bidang: [{nama: 'Majalah'}],
    komisaris: [
      {
        nama: 'Ny. Evita Sumajouw',
        file: null,
        jabatan: 'Komisaris Utama',
      },
      {
        nama: 'R Hasan Akib',
        file: null,
        jabatan: 'Komisaris',
      },
      {
        nama: 'Toto Suharto',
        file: null,
        jabatan: 'Komisaris',
      },
    ],
    direksi: [
      {
        nama: 'Eko Budi Supriyanto',
        file: null,
        jabatan: 'Direktur Utama',
      },
      {
        nama: 'Dwi Setiawati',
        file: null,
        jabatan: 'Direktur',
      },
      {
        nama: 'Karnoto Mohamad',
        file: null,
        jabatan: 'Direktur',
      },
    ],
    kontak: [
      {
        alamat:
          'Jl. Arteri Pd. Pinang No.12A, RT.5/RW.1, Kby. Lama Utara, Jakarta Selatan',
        tlp: '',
        faks: '',
      },
    ],
  },
];

const datadpn = [
  {
    id: 1,
    title: 'PT Binayasa Putra Batara',
    bidang: [{nama: 'Building Management'}],
    komisaris: [
      {
        nama: 'Feriyanto',
        file: null,
        jabatan: 'Komisaris Utama',
      },
    ],
    direksi: [
      {
        nama: 'Toto Priyohartono',
        file: null,
        jabatan: 'Direktur Utama',
      },
      {
        nama: 'Budi Hartono',
        file: null,
        jabatan: 'Direktur',
      },
    ],
    kontak: [
      {
        alamat: 'Jl Prof Dr Soepomo No 231 Tebet, Jakarta Selatan',
        tlp: '',
        faks: '',
      },
    ],
  },
  {
    id: 2,
    title: 'PT Binayasa Karya Pratama',
    bidang: [{nama: 'Outsourcing'}],
    komisaris: [
      {
        nama: 'Marwanto',
        file: null,
        jabatan: 'Komisaris Utama',
      },
      {
        nama: 'Tito Soetalaksana',
        file: null,
        jabatan: 'Komisaris',
      },
    ],
    direksi: [
      {
        nama: 'Turyanti',
        file: null,
        jabatan: 'Direktur Utama',
      },
      {
        nama: 'Harry Budiono',
        file: null,
        jabatan: 'Direktur',
      },
    ],
    kontak: [
      {
        alamat: 'Wisma Purn Batara Lt 1, Jl Kesehatan No 56-58, Jakarta Pusat',
        tlp: '',
        faks: '',
      },
    ],
  },
  {
    id: 3,
    title: 'PT Binasentra Purna',
    bidang: [{nama: 'Broker Asuransi'}],
    komisaris: [
      {
        nama: 'Hertanta',
        file: null,
        jabatan: 'Komisaris Utama',
      },
      {
        nama: 'Yut Penta',
        file: null,
        jabatan: 'Komisaris',
      },
    ],
    direksi: [
      {
        nama: 'Lia Muliana',
        file: null,
        jabatan: 'Direktur Utama',
      },
      {
        nama: 'Agung Gunadi',
        file: null,
        jabatan: 'Direktur',
      },
    ],
    kontak: [
      {
        alamat: 'Jl. Dr Saharjo No 52 Ps Manggis Setiabudi, Jakarta Selatan',
        tlp: '',
        faks: '',
      },
    ],
  },
  {
    id: 4,
    title: 'PT Binagriya Upakara',
    bidang: [{nama: 'Asuransi'}],
    komisaris: [
      {
        nama: 'Saut Pardede',
        file: null,
        jabatan: 'Komisaris Utama',
      },
      {
        nama: 'Suryanti Agustinar',
        file: null,
        jabatan: 'Komisaris',
      },
      {
        nama: 'Moro Widijono Budhi',
        file: null,
        jabatan: 'Komisaris Independen',
      },
    ],
    direksi: [
      {
        nama: 'Sri RM Sudarsari',
        file: null,
        jabatan: 'Direktur Utama',
      },
      {
        nama: 'Fitri Novianty Ratna Kusuma',
        file: null,
        jabatan: 'Direktur Pemasaran',
      },
      {
        nama: 'Dadang Sukresna',
        file: null,
        jabatan: 'Direktur Teknik',
      },
      {
        nama: 'Mohammad Faiz',
        file: null,
        jabatan: 'Direktur Keuangan',
      },
    ],
    kontak: [
      {
        alamat:
          'Wisma Purn Batara Lt 4-6, Jl Kesehatan No 56-58, Jakarta Pusat',
        tlp: '',
        faks: '',
      },
    ],
  },
  {
    id: 5,
    title: 'PT Metro Alam Selaras',
    bidang: [{nama: 'Property'}],
    komisaris: [
      {
        nama: 'Nurwidi Purboyo',
        file: null,
        jabatan: 'Komisaris',
      },
    ],
    direksi: [
      {
        nama: 'Marfiades',
        file: null,
        jabatan: 'Direktur Utama',
      },
      {
        nama: 'Ikhsan Hamid',
        file: null,
        jabatan: 'Direktur',
      },
    ],
    kontak: [
      {
        alamat:
          'Gedung Extama Garaha Lt 6, Jl Prof Dr Soepomo No 19, Jakarta Selatan',
        tlp: '',
        faks: '',
      },
    ],
  },
  {
    id: 6,
    title: 'PT Hotel Batara Bali Indonesia',
    bidang: [{nama: 'Hotel'}],
    komisaris: [
      {
        nama: 'Chien Lee',
        file: null,
        jabatan: 'Komisaris Utama',
      },
      {
        nama: 'Adi Santoso Budidarma ',
        file: null,
        jabatan: 'Komisaris',
      },
    ],
    direksi: [
      {
        nama: 'Rismaully Silalahi',
        file: null,
        jabatan: 'Presiden Direktur',
      },
      {
        nama: 'Lillian Shin Ping Kiang',
        file: null,
        jabatan: 'Direktur',
      },
    ],
    kontak: [
      {
        alamat: 'Wisma Purna Batara Lt 3, Jl Kesehatan No 56-58, Jakarta Pusat',
        tlp: '',
        faks: '',
      },
    ],
  },
  {
    id: 7,
    title: 'PT Pemeringkat Efek Indonesia',
    bidang: [{nama: 'Peringkat Efek'}],
    komisaris: [
      {
        nama: 'Darsono',
        file: null,
        jabatan: 'Komisaris Utama',
      },
      {
        nama: 'Bambang Indiarto',
        file: null,
        jabatan: 'Komisaris',
      },
      {
        nama: 'Iman Firmansyah',
        file: null,
        jabatan: 'Komisaris',
      },
    ],
    direksi: [
      {
        nama: 'Atep Salyadi Dariah Saputra',
        file: null,
        jabatan: 'Direktur Utama',
      },
      {
        nama: 'Hendro Utomo',
        file: null,
        jabatan: 'Direktur',
      },
      {
        nama: 'Ignatius Girendroheru',
        file: null,
        jabatan: 'Direktur',
      },
    ],
    kontak: [
      {
        alamat:
          'Equity Tower , 30th Floor, Sudirman Central Business Distric Lot 9, Jl Jend. Sudirman Kav 52-53, Jakarta Selatan',
        tlp: '',
        faks: '',
      },
    ],
  },
  {
    id: 8,
    title: 'PT Batara Int`l Finansindo',
    bidang: [{nama: 'Leasing'}],
    komisaris: [
      {
        nama: 'Sihar Pangihutan',
        file: null,
        jabatan: 'Komisaris',
      },
    ],
    direksi: [
      {
        nama: 'Albertus Radityo Wibowo',
        file: null,
        jabatan: 'Direktur Utama',
      },
      {
        nama: 'Joko Apriliando',
        file: null,
        jabatan: 'Direktur',
      },
    ],
    kontak: [
      {
        alamat:
          'Jl Bhakti No 24 Kel Rawa Barat Kec Kebayoran Baru, Senopati, Jakarta Selatan',
        tlp: '',
        faks: '',
      },
    ],
  },
];

const index = ({navigation, route}) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    // getVenue()
    setData(route.params === 'ykp' ? dataykp : datadpn);
  }, []);

  // const getVenue = () => {
  //     setLoading(true);
  //     setTimeout(() => {

  //         Axios.get(`http://sipurna.com/aplikasi/api/btn/lap_keu_btn`)
  //             .then((res) => {
  //                 setData(res.data)
  //                 setLoading(false)
  //             }).catch((err) => {
  //                 setLoading(false);
  //                 console.log(err.response.data)
  //                 if (err.response.data === 'Unauthorized.') {
  //                     navigation.navigate('OnKick')
  //                 }
  //             })
  //     }, 100);
  // }

  const renderItem = ({item}) => (
    <View style={styles.item}>
      <View style={styles.containerflatList}>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate('DtlAnakPerusahaan', {
              type: route.params,
              data: item,
            })
          }>
          <Text style={{paddingBottom: 10, fontSize: 16, color: '#2C5BA4'}}>
            {item.title}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-between',
        }}>
        {loading ? (
          <ActivityIndicator
            visible={loading}
            textContent={'Loading...'}
            style={{
              alignSelf: 'center',
              marginTop: '50%',
              position: 'absolute',
            }}
            size="large"
            color="#2C5BA4"
          />
        ) : (
          <>
            <FlatList
              data={data}
              renderItem={renderItem}
              // keyExtractor={item => item.id.toString()}
            />
          </>
        )}
      </View>
    </SafeAreaView>
  );
};

export default index;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    backgroundColor: 'white',
    padding: 10,
    marginVertical: 3,
    flexDirection: 'row',
    flex: 1,
  },
  title1: {
    fontSize: 16,
    paddingVertical: 10,
    // fontFamily:'Montserrat'
  },
  containerflatList: {
    flexDirection: 'column',
    flex: 1,
    margin: 5,
  },
  detail: {
    fontSize: 12,
    color: '#669F16',
  },
});
