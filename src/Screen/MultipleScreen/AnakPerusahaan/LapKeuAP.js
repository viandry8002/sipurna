import React, { useEffect, useState } from 'react';
import { FlatList, SafeAreaView, StyleSheet, Text, TouchableOpacity, View, ActivityIndicator } from 'react-native';

const LapKeuAP = ({ navigation, route }) => {
    const [data, setData] = useState([])

    useEffect(() => {
        console.log('asd', route.params)
        setData(route.params)
    }, [])

    const renderItem = ({ item }) => (
        <View style={styles.item}>
            <View style={styles.containerflatList}>
                <TouchableOpacity onPress={() => navigation.navigate('DtlLapKeuAP', item)}>
                    <Text style={{ paddingBottom: 10, fontSize: 16, color: '#2C5BA4' }}>{item.title}</Text>
                </TouchableOpacity>
            </View>
        </View>
    );

    return (
        <SafeAreaView style={styles.container}>
            <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between' }}>

                <FlatList
                    data={data}
                    renderItem={renderItem}
                // keyExtractor={item => item.id.toString()}
                />

            </View>
        </SafeAreaView>
    )
}

export default LapKeuAP

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    item: {
        backgroundColor: 'white',
        padding: 10,
        marginVertical: 3,
        flexDirection: 'row',
        flex: 1
    },
    containerflatList: {
        flexDirection: 'column',
        flex: 1,
        margin: 5
    },
})

