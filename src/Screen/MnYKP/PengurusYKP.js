import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View, ActivityIndicator, ScrollView } from 'react-native';
import { Button } from 'react-native-elements';
import api from '../../Api';

const PengurusDapen = ({navigation}) => {
  const [data, setData] = useState()
  const [arrayholder, setArrayholder] = useState('')  
  const [Tokens, setTokens] = useState('')
  const [loading, setLoading] = useState(false);

  useEffect(() => {
        
    async function getToken() {
        try{
            const token = await AsyncStorage.getItem("api_token")
            return getPengurus(token)
        }catch(err){
            console.log(err)
        }
   }
   getToken()
  }, []);

  const getPengurus = (token) =>{
    setLoading(true);
        setTimeout(() => {

      Axios.get(`${api}/api/ppusat/ykp`,{
          headers : {
              'Authorization' : 'Bearer ' + (Tokens === '' ? token : Tokens),
              'Accept' : 'application/json'
          }
      }).then((res) =>{
          setArrayholder('DATA1')
          setData(res.data.data)
          Tokens === '' ?  setTokens(token) : setTokens(Tokens) 
          setLoading(false);

      }).catch((err) =>{
        setLoading(false);
        console.log(err.response.data)
        if(err.response.data === 'Unauthorized.'){
            navigation.navigate('OnKick')
          }
      })

      }, 100);
    }

    const getPengawas = () =>{
      setLoading(true);
        setTimeout(() => {
          
      Axios.get(`${api}/api/ppusat/pengawas_ykp`,{
        headers : {
            'Authorization' : 'Bearer ' + Tokens,
            'Accept' : 'application/json'
        }
    }).then((res) =>{
        setArrayholder('DATA2')
        setData(res.data.data)
        setLoading(false);

    }).catch((err) =>{
      console.log(err.response.data)
            if(err.response.data === 'Unauthorized.'){
                navigation.navigate('OnKick')
              }
    })

      }, 100);
    }

    const getPembina = () =>{
      setLoading(true);
        setTimeout(() => {
          
      Axios.get(`${api}/api/ppusat/pembina_ykp`,{
        headers : {
            'Authorization' : 'Bearer ' + Tokens,
            'Accept' : 'application/json'
        }
    }).then((res) =>{
        setArrayholder('DATA3')
        setData(res.data.data)
        setLoading(false);

    }).catch((err) =>{
      console.log(err.response.data)
            if(err.response.data === 'Unauthorized.'){
                navigation.navigate('OnKick')
              }
    })

      }, 100);
    }

    const renderItem = ({item}) => (
        <View style={styles.item}>
          <Image source={item.file === null ? require('../../Assets/drawable-xhdpi/img-notfound.png') : {uri: item.file}} style={{width: 140,height: 160,borderRadius:10, marginHorizontal:10,marginTop:10}} />
          <View style={styles.containerflatList}>
          <Text style={styles.title1}>{item.name}</Text>
          <Text style={styles.title2}>{item.position}</Text>
          <Text style={styles.title3}>Handphone : </Text>
          <Text style={styles.title3}>{item.phone}</Text>
          </View>
          </View>
    );

      var typ = arrayholder == 'DATA1' ? "solid" : "outline"
      var ttl = arrayholder == 'DATA1' ? {color:'white'} : {color:'black'}
      var btn = arrayholder == 'DATA1' ? {backgroundColor:'#2C5BA4',paddingHorizontal:50,marginHorizontal:5} : {paddingHorizontal:50,marginHorizontal:5}
  
      var typ1 = arrayholder == 'DATA2' ? "solid" : "outline"
      var ttl1 = arrayholder == 'DATA2' ? {color:'white'} : {color:'black'}
      var btn1 = arrayholder == 'DATA2' ? {backgroundColor:'#2C5BA4',paddingHorizontal:20,marginHorizontal:5} : {paddingHorizontal:20,marginHorizontal:5}

      var typ2 = arrayholder == 'DATA3' ? "solid" : "outline"
      var ttl2 = arrayholder == 'DATA3' ? {color:'white'} : {color:'black'}
      var btn2 = arrayholder == 'DATA3' ? {backgroundColor:'#2C5BA4',paddingHorizontal:20,marginHorizontal:5} : {paddingHorizontal:20,marginHorizontal:5}

    return (
        <SafeAreaView style={styles.container}>
        <View style={{flex:1,flexDirection:'column'}}>
        <View style={styles.item1}>
        <ScrollView horizontal={true}>

        <Button title="Pengurus"
        type={typ}
        titleStyle={ttl}
        buttonStyle={btn} 
        onPress={() => {getPengurus()}}/>

        <Button title="Pengawas" 
        type={typ1} 
        titleStyle={ttl1} 
        buttonStyle={btn1} 
        onPress={() => {getPengawas()}}/>

        <Button title="Pembina" 
        type={typ2} 
        titleStyle={ttl2} 
        buttonStyle={btn2} 
        onPress={() => {getPembina()}}/>

        </ScrollView>
        </View>
        
        <View style={{flex:1,flexDirection:'column',justifyContent: 'space-between',backgroundColor:'white'}}>

        {loading ? (
          <ActivityIndicator
            visible={loading}
            textContent={'Loading...'}
            style={{alignSelf:'center',marginTop:'40%',position:'absolute'}}
            size="large" 
            color="#2C5BA4"
          />
        ) : (
          <>
         <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
          />
          </>
        )}

         </View>

        </View>
    </SafeAreaView>
    )
}

export default PengurusDapen


const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
      item1: {
        backgroundColor: 'white',
        paddingVertical: 20,
        paddingHorizontal:10,
        flexDirection : 'row',
        width:'100%',
        height: 86,
        justifyContent: 'space-between',
        marginBottom:10
      },
      item: {
        backgroundColor: 'white',
        padding: 10,
        marginVertical: 3,
        flexDirection : 'row',
        flex: 1
      },
      containerflatList : {
        flexDirection : 'column',
        flex: 1,
        margin:5
      },
      title1: {
        fontSize: 18,
        marginTop:10,
        marginBottom:5
      },
      title2: {
        color:'#669F16',
        marginBottom:15
      },
      title3: {
        color:'#919191',
      },
})


