import React from 'react';
import { Image, SafeAreaView, StatusBar, StyleSheet, TouchableOpacity, View } from 'react-native';
import colors from '../../Style/Colors';

const LmnYKP = ({ navigation }) => {
    return (
        <SafeAreaView style={styles.container}>
            <Image source={require('../../Assets/drawable-xhdpi/Group253.png')} style={{ flex: 1, width: '100%', height: '100%', position: 'absolute', marginTop: 100 }} />
            <View style={{ backgroundColor: 'rgba(255,255,255,0.8)', flex: 1 }}>
                <StatusBar translucent backgroundColor='transparent' barStyle="light-content" />
                <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center', paddingHorizontal: 10 }}>
                    {/* box Menu */}
                    <View style={{ width: '100%', height: 300, marginTop: 30 }}>
                        <TouchableOpacity onPress={() => navigation.navigate('Tentang', { name: 'ykp', title: 'YKP Bank BTN' })}>
                            <Image source={require('../../Assets/drawable-xhdpi/ListMenuYKP/Group300.png')} style={styles.btnMenu} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => navigation.navigate('Kesehatan')}>
                            <Image source={require('../../Assets/drawable-xhdpi/ListMenuYKP/Group119.png')} style={styles.btnMenu} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => navigation.navigate('Keluarga')}>
                            <Image source={require('../../Assets/drawable-xhdpi/ListMenuYKP/Group120.png')} style={styles.btnMenu} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => navigation.navigate('InfoYKP')}>
                            <Image source={require('../../Assets/drawable-xhdpi/ListMenuYKP/Group121.png')} style={styles.btnMenu} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => navigation.navigate('KontakYKP')}>
                            <Image source={require('../../Assets/drawable-xhdpi/ListMenuYKP/Group122.png')} style={styles.btnMenu} />
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => navigation.navigate('AnakPerusahaan', 'ykp')}>
                            <Image source={require('../../Assets/drawable-xhdpi/ListMenuYKP/Group138.png')} style={styles.btnMenu} />
                        </TouchableOpacity>

                    </View>
                    {/* box Menu */}

                </View>
            </View>
        </SafeAreaView>
    )
}

export default LmnYKP


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    btnMenu: {
        width: '100%',
        height: 70
    }
})

