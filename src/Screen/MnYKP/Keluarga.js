import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import api from '../../Api';

const Keluarga = ({navigation}) => {
    const [dataP, setDataP] = useState([])
    const [dataK, setDataK] = useState([])
    const [KontakPhone, setKontakPhone] = useState([])

    useEffect(() => {
        async function getToken() {
             try{
                 const token = await AsyncStorage.getItem("api_token")
                 const id = await AsyncStorage.getItem("user_id")
                 const npp = await AsyncStorage.getItem("npp")
                 return getVenue(token,id,npp)
             }catch(err){
                 console.log(err)
             }
        }
        getToken()
        // getVenue()
    },[])

    const getVenue = (token,id,npp) =>{
        // api profile
        Axios.get(`${api}/api/user/${id}`,{
            timeout : 20000,
            headers : {
                'Authorization' : 'Bearer ' + token,
                'Accept' : 'application/json'
            }
        }).then((res) =>{
            setDataP(res.data.data)
            // console.log(res.data.data)
            setKontakPhone(res.data.data.phone)

        }).catch((err) =>{
            console.log(err.response.data)
            if(err.response.data === 'Unauthorized.'){
                navigation.navigate('OnKick')
              }
        })
        // api keluarga
        Axios.get(`${api}/api/user/parent/${npp}`,{
        // Axios.get(`${api}/api/user/parent/03969`,{
            timeout : 20000,
            headers : {
                'Authorization' : 'Bearer ' + token,
                'Accept' : 'application/json'
            }
        }).then((res) =>{
            setDataK(res.data.data)
        }).catch((err) =>{
            console.log(err.response.data)
            if(err.response.data === 'Unauthorized.'){
                navigation.navigate('OnKick')
              }
        })
    
    }

    return (
        <SafeAreaView style={styles.container}>
        <ScrollView style={{flex:1,flexDirection:'column'}}>
        
        {/* item 1 */}
        <View style={[styles.item1,{height:280}]}>
            <View style={styles.item}>
            <Text style={styles.container}>Nama Lengkap</Text>
            <Text style={styles.container2}>{dataP.fullname}</Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>NPP</Text>
            <Text style={styles.container2}>{dataP.npp}</Text>
            </View>

            {/* <View style={styles.item}>
            <Text style={styles.container}>NIP ex</Text>
            <Text style={styles.container2}>{dataP.nip}</Text>
            </View> */}

            <View style={styles.item}>
            <Text style={styles.container}>TTL</Text>
            <Text style={styles.container2}>{dataP.birth_place}, {dataP.birth_date}</Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>Jenis Kelamin</Text>
            <Text style={styles.container2}>{dataP.gender}</Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>STS YBS</Text>
            <Text style={styles.container2}>{dataP.sts_ybs}</Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>No. Telp</Text>
            {
                KontakPhone.map((phone, index)=>{
                return <Text style style={styles.container2} key={index} >{phone.value}</Text>
                })
            }
            </View>
        </View>
        {/* item 2 */}
        <View style={[styles.item1,{height:250}]}>
            <View style={styles.item}>
            <Text style={styles.container}>Provinsi</Text>
            <Text style={styles.container2}>{dataP.province_id}</Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>Kota / Kabupaten</Text>
            <Text style={styles.container2}>{dataP.city_id}</Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>Kecamatan</Text>
            <Text style={styles.container2}>{dataP.district_id}</Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>Kelurahan</Text>
            <Text style={styles.container2}>{dataP.subdistrict_id}</Text>
            </View>
            
            <View style={styles.item}>
            <Text style={styles.container}>Kode POS</Text>
            <Text style={styles.container2}>{dataP.postcode}</Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>Alamat Lengkap</Text>
            <Text style={styles.container2}>{dataP.address_1}</Text>
            {/* <Text style={styles.container2}>{dataP.address_detail}</Text> */}
            </View>
        </View>
        {/* item 3 */}
        {/* <View style={[styles.item1,{height:420}]}>
            <View style={styles.item}>
            <Text style={styles.container}>Mulai Pensiun</Text>
            <Text style={styles.container2}>01/Januari/2006</Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>Grade</Text>
            <Text style={styles.container2}>10</Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>KLS PRWT</Text>
            <Text style={styles.container2}>TIGA</Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>Jabatan</Text>
            <Text style={styles.container2}>Pesuruh</Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>Masa Kerja</Text>
            <Text style={styles.container2}>15 thn 3 bln</Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>Limitasi</Text>
            <Text style={styles.container2}>75%</Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>Kelompok</Text>
            <Text style={styles.container2}>B</Text>
            </View>

            <View style={styles.item}>
            <Text style={styles.container}>Jenis Pensiun</Text>
            <Text style={styles.container2}>PPD</Text>
            </View>

            <View style={styles.item}>
            <View style={styles.colum}>
            <Text>Diambil</Text>
            <Text style={styles.titlered}>(skaligus pensiun)</Text>
            </View>
            <Text style={styles.container2}>Rp 150,912,900</Text>
            </View>

            <View style={styles.item}>
            <View style={styles.colum}>
            <Text>Pensiun 20%</Text>
            <Text style={styles.titlered}>(2,5 x mk x phdp)</Text>
            </View>
            <Text style={styles.container2}>Rp 1,328,900</Text>
            </View>

            <View style={styles.item}>
            <View style={styles.colum}>
            <Text>Kenaikan / 1 Jan 2018</Text>
            <Text style={styles.titlered}>(2,5 x mk x phdp) + 250.000</Text>
            </View>
            <Text style={styles.container2}>Rp 0</Text>
            </View>
        </View> */}
        {/* item 4 */}
        
        {
            dataK.map((datas, index)=>{
                    return (
                        <View style={[styles.item1,{height:100}]} key={index}>
                        <View style={styles.item}>
                        <Text style={styles.container}>Nama {datas.type}</Text>
                        <Text style={styles.container2}>{datas.fullname}</Text>
                        </View>
    
                        <View style={styles.item}>
                        <Text style={styles.container}>TTL</Text>
                        <Text style={styles.container2}>{datas.date_place}</Text>
                        </View>
                        </View>
                        )
                })
            }
        {/* item 4 */}

        </ScrollView>
    </SafeAreaView>
    )
}

export default Keluarga

const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
      container2: {
        flex: 1,
        textAlign:'right'
      },
      item1: {
        backgroundColor: 'white',
        padding: 20,
        flexDirection : 'column',
        width:'100%',
        justifyContent: 'space-between',
        marginBottom:10
      },
    item: {
        flexDirection:'row', 
        justifyContent: 'space-between'
      },
      colum: {
          flexDirection:'column'
        },
      titlered: {
       color:'red',
       fontSize: 10
      },
})
