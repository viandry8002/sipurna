import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { FlatList, Image, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements';
import api from '../../Api';

const PengurusYKP = ({navigation}) => {
    const [Tokens, setTokens] = useState("")
    const [data, setData] = useState([])
  
      useEffect(() => {
        async function getToken() {
            try{
                const token = await AsyncStorage.getItem("api_token")
                return getKota(token)
            }catch(err){
                console.log(err)
            }
        }
        getToken()
    },[])
  
    const getKota = (token) =>{
        Axios.get(`${api}/api/ppusat/ykp`,{
            timeout : 20000,
            headers : {
                'Authorization' : 'Bearer ' + (Tokens === '' ? token : Tokens),
                'Accept' : 'application/json'
            }
        }).then((res) =>{
            setData(res.data.data)
            Tokens === '' ?  setTokens(token) : setTokens(Tokens)
        }).catch((err) =>{
          console.log(err.response.data)
          if(err.response.data === 'Unauthorized.'){
              navigation.navigate('OnKick')
            }
        })
    }
  
      const renderItem = ({item}) => (
          <View style={styles.item}>
          <Image source={item.file === null ? require('../../Assets/drawable-xhdpi/img-notfound.png') : {uri: item.file}} style={{width: 140,height: 160,borderRadius:10, marginHorizontal:10,marginTop:10}} />
          <View style={styles.containerflatList}>
          <Text style={styles.title1}>{item.name}</Text>
          <Text style={styles.title2}>{item.position}</Text>
          <Text style={styles.title3}>Handphone : </Text>
          <Text style={styles.title3}>{item.phone}</Text>
          </View>
          </View>
      );
  
      return (
        <View style={styles.container}>
        <View style={{flex:1,flexDirection:'column'}}>
        <View style={styles.item1}>
        <ScrollView horizontal={true}>
        <Button title="Maksud dan Tujuan" type="outline" titleStyle={{color:'black'}} buttonStyle={{marginRight:10}}  onPress={() => navigation.navigate('InfoYKP')}/>
        <Button title="Daftar Pengurus" buttonStyle={{backgroundColor:'#2C5BA4',marginRight:10}}  onPress={() => navigation.navigate('PengurusYKP')} />
        <Button title="Info Kegiatan" type="outline" titleStyle={{color:'black'}} onPress={() => navigation.navigate('KegiatanYKP')}/>
        </ScrollView>
        </View>
        
        <SafeAreaView style={{flex:1}}> 
        {/* <View style={styles.item2}> */}
        <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
          />
        {/* </View> */}
        </SafeAreaView>

        </View>
    </View>
      )
  }
  
  export default PengurusYKP
  
  
const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
      item1: {
        backgroundColor: 'white',
        paddingVertical: 20,
        paddingHorizontal:15,
        flexDirection : 'row',
        width:'100%',
        height: 86,
        justifyContent: 'space-between',
        marginBottom:10
      },
      item2: {
        flexDirection : 'column',
        width:'100%',
        justifyContent: 'space-between',
      },
      containerflatList : {
        flexDirection : 'column',
        flex: 1,
        margin:5
      },
      item: {
        backgroundColor: 'white',
        padding: 10,
        marginVertical: 3,
        flexDirection : 'row',
        flex: 1
      },
      title1: {
        fontSize: 18,
        marginTop:10,
        marginBottom:5
      },
      title2: {
        color:'#669F16',
        marginBottom:15
      },
      title3: {
        color:'#919191',
      },
})