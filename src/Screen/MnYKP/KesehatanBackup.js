import React, { useState, useEffect } from 'react';
import { Image, SafeAreaView, StatusBar, StyleSheet, TouchableOpacity, View, Text, PermissionsAndroid, ActivityIndicator, ToastAndroid } from 'react-native';
import colors from '../../Style/Colors';
import Geolocation from '@react-native-community/geolocation';
import { Alert } from 'react-native';

const Kesehatan = ({ navigation }) => {
    const [currentLongitude, setCurrentLongitude] = useState('...');
    const [currentLatitude, setCurrentLatitude] = useState('...');
    const [locationStatus, setLocationStatus] = useState('load');

    useEffect(() => {
        const requestLocationPermission = async () => {
            if (Platform.OS === 'ios') {
                getOneTimeLocation();
                subscribeLocationLocation();
            } else {
                try {
                    const granted = await PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                        {
                            title: 'Location Access Required',
                            message: 'This App needs to Access your location',
                        },
                    );
                    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                        //To Check, If Permission is granted
                        getOneTimeLocation();
                        subscribeLocationLocation();
                    } else {
                        setLocationStatus('Permission Denied');
                    }
                } catch (err) {
                    console.warn(err);
                }
            }
        };

        requestLocationPermission();
        return () => {
            Geolocation.clearWatch(watchID);
        };
    }, []);

    const showToast = () => {
        ToastAndroid.show("Lokasi anda telah terdeteksi !", ToastAndroid.LONG);
    };

    const getOneTimeLocation = () => {
        setLocationStatus('Getting Location ...');
        Geolocation.getCurrentPosition(
            //Will give you the current location
            (position) => {
                // Alert.alert('',
                //     'berhasil mendapatkan lokasi')
                setLocationStatus('You are Here');
                showToast()

                //getting the Longitude from the location json
                const currentLongitude =
                    JSON.stringify(position.coords.longitude);

                //getting the Latitude from the location json
                const currentLatitude =
                    JSON.stringify(position.coords.latitude);

                //Setting Longitude state
                setCurrentLongitude(currentLongitude);
                console.log('long', currentLongitude)

                //Setting Longitude state
                setCurrentLatitude(currentLatitude);
                console.log('lat', currentLatitude)
            },
            (error) => {
                setLocationStatus(error.message);
                console.log(error.message)
                if (error.message === 'No location provider available.') {
                    Alert.alert('Peringatan',
                        'Untuk masuk Ke menu Rumah Sakit,nyalakan GPS terlebih dahulu')
                }
            },
            {
                enableHighAccuracy: true,
                timeout: 30000,
                // maximumAge: 1000
            },
        );
    };

    const subscribeLocationLocation = () => {
        watchID = Geolocation.watchPosition(
            (position) => {
                //Will give you the location on location change

                setLocationStatus('You are Here');
                showToast()
                console.log(position);

                //getting the Longitude from the location json        
                const currentLongitude =
                    JSON.stringify(position.coords.longitude);

                //getting the Latitude from the location json
                const currentLatitude =
                    JSON.stringify(position.coords.latitude);

                //Setting Longitude state
                setCurrentLongitude(currentLongitude);

                //Setting Latitude state
                setCurrentLatitude(currentLatitude);
            },
            (error) => {
                setLocationStatus(error.message);
            },
            {
                enableHighAccuracy: true,
                // maximumAge: 1000
            },
        );
    };

    const navRs = () => {
        if (locationStatus === 'No location provider available.') {
            Alert.alert('Peringatan',
                'Untuk masuk Ke menu Rumah Sakit,nyalakan GPS terlebih dahulu',
                [{
                    text: 'OK',
                    onPress: () => {
                        getOneTimeLocation();
                        subscribeLocationLocation();
                    }
                }])
        } else if (locationStatus === 'Location request timed out') {
            Alert.alert('Peringatan',
                location,
                [{
                    text: 'OK',
                    onPress: () => {
                        getOneTimeLocation();
                        subscribeLocationLocation();
                    }
                }])
        } else {
            navigation.navigate('Rs', { latitude: currentLatitude, longitude: currentLongitude })
        }

    }

    return (
        <SafeAreaView style={styles.container}>
            <Image source={require('../../Assets/drawable-xhdpi/Group253.png')} style={{ flex: 1, width: '100%', height: '100%', position: 'absolute', marginTop: 100 }} />

            <View style={{ backgroundColor: 'rgba(255,255,255,0.8)', flex: 1 }}>
                <StatusBar translucent backgroundColor='transparent' barStyle="light-content" />
                {
                    locationStatus === 'load' || locationStatus === 'Getting Location ...' ?
                        <View style={{
                            justifyContent: 'center', flex: 1, alignItems: 'center'
                        }}>
                            <ActivityIndicator
                                visible={true}
                                textContent={'Loading...'}
                                style={{ alignSelf: 'center' }}
                                size="large"
                                color="#2C5BA4"
                            />
                        </View>
                        :
                        <View style={{
                            flexDirection: 'column', alignItems: 'center'
                            //  ,marginBottom:480
                        }}>
                            {/* box Menu */}
                            <View style={{ margin: 20 }}>
                                <Text
                                    style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        marginTop: 16,
                                    }}>
                                    {locationStatus}
                                </Text>
                                <Text
                                    style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        marginTop: 16,
                                    }}>
                                    Longitude: {currentLongitude}
                                </Text>
                                <Text
                                    style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        marginTop: 16,
                                    }}>
                                    Latitude: {currentLatitude}
                                </Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <TouchableOpacity onPress={() => navigation.navigate('Plafond')}>
                                        <Image source={require('../../Assets/drawable-xhdpi/ListMenuYKP/Kesehatan/Group123.png')} style={styles.btnImageMenu} />
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => navigation.navigate('Reimbus')}>
                                        <Image source={require('../../Assets/drawable-xhdpi/ListMenuYKP/Kesehatan/Group124.png')} style={styles.btnImageMenu} />
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flexDirection: 'row' }}>
                                    <TouchableOpacity onPress={() => navigation.navigate('JaminanRS')}>
                                        <Image source={require('../../Assets/drawable-xhdpi/ListMenuYKP/Kesehatan/Group126.png')} style={styles.btnImageMenu} />
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => navRs()}>
                                        <Image source={require('../../Assets/drawable-xhdpi/ListMenuYKP/Kesehatan/Group125.png')} style={styles.btnImageMenu} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                }
            </View>

            {/* box Menu */}
        </SafeAreaView>
    )
}

export default Kesehatan

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    btnImageMenu: {
        width: 160,
        height: 160,
        margin: 10
    }
})
