import AsyncStorage from '@react-native-async-storage/async-storage';
import { Picker } from '@react-native-picker/picker';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Touchable } from 'react-native';
import { ActivityIndicator, FlatList, SafeAreaView, StyleSheet, Text, TouchableOpacity, View, ScrollView,Modal,Platform } from 'react-native';
import { Button, Input } from 'react-native-elements';
import api from '../../Api';
import { WebView } from 'react-native-webview';
import AntDesign from 'react-native-vector-icons/AntDesign';

const RsYKP = ({ navigation, route }) => {
    // console.log('paraaaaaaaaaaaaaaaaamssssssssss', route.params)
    const [selectedValue, setSelectedValue] = useState('');
    // const [isEnabled, setIsEnabled] = useState(false);
    // const toggleSwitch = () => setIsEnabled(previousState => !previousState);
    const [data, setData] = useState()
    const [datat, setDatat] = useState()
    const [text, setText] = useState('')
    const [arrayholder, setArrayholder] = useState('')
    const [Tokens, setTokens] = useState('')
    const [loading, setLoading] = useState(false);
    const [Dkota, setDkota] = useState([])
    const [page, setPage] = useState(1);
    const [btnTouch, setbtnTouch] = useState(false)
    const [longtitude, setlongtitude] = useState(route.params.longitude)
    const [latitude, setlatitude] = useState(route.params.latitude)

    var typt = arrayholder == 'DATA' ? "solid" : "outline"
    var ttlt = arrayholder == 'DATA' ? { color: 'white' } : { color: 'black' }
    var btnt = arrayholder == 'DATA' ? { backgroundColor: '#2C5BA4', paddingHorizontal: 30, marginHorizontal: 10 } : { paddingHorizontal: 30, marginHorizontal: 10 }

    var typ = arrayholder == 'DATA1' ? "solid" : "outline"
    var ttl = arrayholder == 'DATA1' ? { color: 'white' } : { color: 'black' }
    var btn = arrayholder == 'DATA1' ? { backgroundColor: '#2C5BA4', paddingHorizontal: 30, marginHorizontal: 10 } : { paddingHorizontal: 30, marginHorizontal: 10 }

    var typ1 = arrayholder == 'DATA2' ? "solid" : "outline"
    var ttl1 = arrayholder == 'DATA2' ? { color: 'white' } : { color: 'black' }
    var btn1 = arrayholder == 'DATA2' ? { backgroundColor: '#2C5BA4', paddingHorizontal: 30, marginHorizontal: 10 } : { marginHorizontal: 10, paddingHorizontal: 30 }

    var typ2 = arrayholder == 'DATA3' ? "solid" : "outline"
    var ttl2 = arrayholder == 'DATA3' ? { color: 'white' } : { color: 'black' }
    var btn2 = arrayholder == 'DATA3' ? { backgroundColor: '#2C5BA4', paddingHorizontal: 30, marginHorizontal: 10 } : { paddingHorizontal: 30, marginHorizontal: 10 }

    const [modalVisible, setModalVisible] = useState(false);

    useEffect(() => {
        async function getToken() {
            try {
                const token = await AsyncStorage.getItem("api_token");
                return getRSTerdekat(token, page)
            } catch (err) {
                console.log(err)
            }
        }
        getToken()
    }, []);

    const getFilter = (text, selectedValue) => {
        let dataf = {
            src: text,
            kota: selectedValue
        }
        // if (text === '' && selectedValue === '') {
        //     if (arrayholder === 'DATA1') {
        //         getRSYKP('', 1)
        //     } else {
        //         getRSBPJS(1)
        //     }
        switch (arrayholder) {
            case 'DATA':
                return getRSTerdekat('', 1);
            case 'DATA1':
                return (
                    Axios.post(`${api}/api/rsykp/search`, dataf, {
                        headers: {
                            'Authorization': 'Bearer ' + Tokens,
                            'Accept': 'application/json'
                        }
                    }).then((res) => {
                        getCity(Tokens, 'city')
                        setData(res.data.data.data)
                        setText('')
                        setSelectedValue('')
                    }).catch((err) => {
                        console.log(err)
                    })
                )
            case 'DATA2':
                return (
                    Axios.post(`${api}/api/rsbpjs/search`, dataf, {
                        headers: {
                            'Authorization': 'Bearer ' + Tokens,
                            'Accept': 'application/json'
                        }
                    }).then((res) => {
                        getCity(Tokens, 'city2')
                        setData(res.data.data.data)
                        setText('')
                        setSelectedValue('')
                    }).catch((err) => {
                        console.log(err)
                    })
                )
            default:
                break;
        }
    }

    const getCity = (Tokenz, Lokasi) => {

        Axios.get(`${api}/api/${Lokasi}`, {
            headers: {
                'Authorization': 'Bearer ' + Tokenz,
                'Accept': 'application/json'
            }
        }).then((res) => {
            setDkota(res.data.data)
        }).catch((err) => {
            console.log(err)
        })
    }

    const getRSTerdekat = (token, pages) => {
        // console.log(data)
        setbtnTouch(true);
        setLoading(true);
        setTimeout(() => {
            let body = {
                src: text,
                kota: selectedValue,
                latitude_user: latitude,
                longitude_user: longtitude
            }
            Axios.post(`${api}/api/rs/search-location?page=` + (pages ? pages : page), body, {
                headers: {
                    'Authorization': 'Bearer ' + (Tokens === '' ? token : Tokens),
                    'Accept': 'application/json'
                }
            }).then((res) => {
                Tokens === '' ? setTokens(token) : setTokens(Tokens)
                setArrayholder('DATA')
                // console.log('sssssssssssssssssss', res.data.data)
                // console.log('aaaaaaaa', Tokens)
                if (pages > 1) {
                    setDatat([...datat, ...res.data.data])
                } else {
                    setDatat(res.data.data)
                }
                getCity(Tokens === '' ? token : Tokens, 'city')
                setLoading(false)
                // setOffset(offset + 10);
                setbtnTouch(false);
                setText('')
                setSelectedValue('')
            }).catch((err) => {
                setLoading(false);
                console.log(err.response.data)
                if (err.response.data === 'Unauthorized.') {
                    navigation.navigate('OnKick')
                }
            })

        }, 100);

    }

    const getRSYKP = (pages) => {
        // console.log(data)
        setbtnTouch(true);
        setLoading(true);
        setTimeout(() => {

            Axios.get(`${api}/api/rsykp?page=` + (pages ? pages : page), {
                headers: {
                    'Authorization': 'Bearer ' + Tokens,
                    'Accept': 'application/json'
                }
            }).then((res) => {
                setArrayholder('DATA1')
                if (pages > 1) {
                    setData([...data, ...res.data.data.data])
                } else {
                    setData(res.data.data.data)
                }
                getCity(Tokens === '' ? token : Tokens, 'city')
                setbtnTouch(false);
                setLoading(false)
            }).catch((err) => {
                setLoading(false);
                console.log(err.response.data)
                if (err.response.data === 'Unauthorized.') {
                    navigation.navigate('OnKick')
                }
            })

        }, 100);

    }

    const getRSBPJS = (pages) => {
        setbtnTouch(true);
        setLoading(true);
        setTimeout(() => {
            Axios.get(`${api}/api/rsbpjs?page=` + (pages ? pages : page), {
                headers: {
                    'Authorization': 'Bearer ' + Tokens,
                    'Accept': 'application/json'
                }
            }).then((res) => {
                setArrayholder('DATA2')
                if (pages > 1) {
                    setData([...data, ...res.data.data.data])
                } else {
                    setData(res.data.data.data)
                }
                getCity(Tokens, 'city2')
                setbtnTouch(false);
                setLoading(false)
            }).catch((err) => {
                setLoading(false);
                console.log(err.response.data)
                if (err.response.data === 'Unauthorized.') {
                    navigation.navigate('OnKick')
                }
            })
        }, 100);
    }

    const getRSLainnya = () => {
        setbtnTouch(true);
        setLoading(true);
        setTimeout(() => {

            setArrayholder('DATA3')
            setData([])
            setLoading(false);
            setbtnTouch(false);
        }, 100);
    }

    const Dtlrs = (route) => {
        if (arrayholder === 'DATA1') {
            navigation.navigate('RsDtlYKP', route)
        } else {
            navigation.navigate('RsDtlBPJS', route)
        }
    }

    const Dtlrst = (type, route) => {
        if (type === 'rs_ykp') {
            navigation.navigate('RsDtlYKP', route)
        } else {
            navigation.navigate('RsDtlBPJS', route)
        }
    }

    const renderItem = ({ item }) => (
        <TouchableOpacity onPress={() => Dtlrs(item)}>
            <View style={styles.item}>
                <View style={styles.containerflatList}>
                    <View style={{ flexDirection: 'column' }}>
                        <Text style={[styles.title1, { width: 200, textAlign: 'left' }]}>{item.title}</Text>
                        {/* <Text style={{ color: 'red' }}>({item.distance === 100000000000000000000 ? 'null' : item.distance} km)</Text> */}
                    </View>
                    <Text style={[styles.title1, { position: 'absolute', marginLeft: '60%', width: 100, textAlign: 'right' }]}>{arrayholder === 'DATA1' ? item.address : item.city_id}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );

    const renderItemRST = ({ item }) => (
        <TouchableOpacity onPress={() => Dtlrst(item.type_rs, item.detail[0])}>
            <View style={styles.item}>
                <View style={styles.containerflatList}>
                    <View style={{ flexDirection: 'column' }}>
                        <Text style={[styles.title1, { width: 200, textAlign: 'left' }]}>{item.detail[0].title}
                            <Text style={{ color: 'red' }}>  ({item.distance === 100000000000000000000 ? 'null' : item.distance} km)</Text>
                        </Text>
                        <Text style={item.type_rs === 'rs_ykp' ? { color: 'blue' } : { color: '#669F16' }}>{((item.type_rs).replace(/_|-/g, " ")).toUpperCase()}</Text>
                    </View>
                    <Text style={[styles.title1, { position: 'absolute', marginLeft: '60%', width: 100, textAlign: 'right' }]}>{item.detail[0].address}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );

    const renderFooter = () => (
        loading ?
            (
                <View style={{
                    paddingHorizontal: 10,
                    paddingVertical: 10,
                    height: 70,
                    backgroundColor: 'white'
                }}>
                    <ActivityIndicator
                        visible={loading}
                        textContent={'Loading...'}
                        style={{ alignSelf: 'center' }}
                        size="large"
                        color="#2C5BA4"
                    />
                </View>
            )
            :
            // (<View></View>)
            (
                <View style={{
                    paddingHorizontal: 10,
                    paddingVertical: 10,
                    height: 70,
                    backgroundColor: 'white'
                }}>
                    <TouchableOpacity style={{ marginHorizontal: 15 }}
                        onPress={() => {
                            setPage(page + 1)
                            if (arrayholder === 'DATA') {
                                getRSTerdekat('', page + 1)
                            } else if (arrayholder === 'DATA1') {
                                getRSYKP(page + 1)
                            } else if (arrayholder === 'DATA2') {
                                getRSBPJS(page + 1)
                            }
                        }}
                    >
                        <Text style={[styles.title1, { color: '#2C5BA4' }]}>Click to Load More ...</Text>
                    </TouchableOpacity>
                </View>)
    )

    const renderHeader = () => (
        loading ? (
            <View style={{
                paddingHorizontal: 10,
                paddingVertical: 10,
                height: 80,
                backgroundColor: 'white'
            }}>
                <Text style={{ color: 'blue', textAlign: 'center', marginTop: 20 }}>Sedang mengambil data ...</Text>
            </View>
        )
            :
            (<View></View>)
    )

    let heightTop = '';
    if (arrayholder === 'DATA') {
        heightTop = 135
    } else if (arrayholder === 'DATA3') {
        heightTop = 100
    } else {
        heightTop = 200
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <View style={[styles.item1, { height: heightTop }]}>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        {/* button top */}

                        <ScrollView horizontal={true}>
                            <View style={{ flexDirection: 'row' }}>
                                <Button title="RS Terdekat"
                                    type={typt}
                                    titleStyle={ttlt}
                                    buttonStyle={btnt}
                                    onPress={() => { getRSTerdekat('', 1) }}
                                    disabled={btnTouch}
                                />

                                <Button title="RS YKP"
                                    type={typ}
                                    titleStyle={ttl}
                                    buttonStyle={btn}
                                    onPress={() => { getRSYKP(1) }}
                                    disabled={btnTouch} />

                                <Button title="RS BPJS"
                                    type={typ1}
                                    titleStyle={ttl1}
                                    buttonStyle={btn1}
                                    onPress={() => { getRSBPJS(1) }}
                                    disabled={btnTouch} />

                                <Button title="RS Lainnya"
                                    type={typ2}
                                    titleStyle={ttl2}
                                    buttonStyle={btn2}
                                    onPress={() => { getRSLainnya() }}
                                    disabled={btnTouch} />
                            </View>
                        </ScrollView>

                        {/* button top */}
                    </View>
                    {
                        arrayholder === 'DATA3' ?
                            (<View style={{ height: 20 }}></View>)
                            :
                            (<View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginVertical: 10 }}>
                                    <View style={{ width: '60%', height: 50, borderRadius: 10, borderWidth: 1, borderColor: '#C3C3C3' }}>
                                        <Input
                                            placeholder="Cari Rumah Sakit"
                                            onChangeText={(text) => setText(text)}
                                            value={text}
                                            rightIcon={{ type: 'EvilIcons', name: 'search' }}
                                            inputContainerStyle={{ borderBottomWidth: 0 }}
                                        />
                                    </View>
                                    <Button title="Cari" type='solid' buttonStyle={{ backgroundColor: '#2C5BA4', height: 45, paddingHorizontal: '15%' }} onPress={() => { getFilter(text, '') }}
                                        disabled={btnTouch}
                                    />
                                </View>
                                {
                                    arrayholder === 'DATA' ?
                                        <View></View>
                                        :
                                        <View style={{ borderWidth: 1, borderColor: '#C3C3C3', borderRadius: 10, width: '100%', height: 45,justifyContent:'center' }}>
                                            {Platform.OS === 'ios' ? (
                                            <TouchableOpacity onPress={() => setModalVisible(true)} style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginHorizontal:10}}>
                                            <Text>Cari Lokasi</Text>
                                            <AntDesign name='caretdown' size={10} />
                                            </TouchableOpacity>):
                                            (<Picker
                                                selectedValue={selectedValue}
                                                style={{ height: 40, width: '100%' }}
                                                onValueChange={(itemValue) => { setSelectedValue(itemValue), getFilter('', itemValue) }}>
                                                <Picker.Item label="Cari Lokasi" value="" />
                                                {
                                                    Dkota.map((datak, index) => {
                                                        return <Picker.Item key={index} label={arrayholder === 'DATA1' ? datak.address : datak.city_id} value={arrayholder === 'DATA1' ? datak.address : datak.city_id} />
                                                    })
                                                }
                                            </Picker>)}
                                        </View>

                                }
                            </View>
                            )}
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                        {/* tampilkan kerjasama */}
                        {/* <View style={{width:'50%',flexDirection:'row',paddingLeft:10,height:45}}>
        <View style={{flexDirection:'column',padding:5}}>
        <Text>Tampilkan</Text>
        <Text>Kerjasama</Text>
        </View>
        <Switch
        trackColor={{ false: "#767577", true: "#81b0ff" }}
        thumbColor={isEnabled ? "#f5dd4b" : "#f4f3f4"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={toggleSwitch}
        value={isEnabled}
        style={{padding:10}}
        />
        </View> */}
                    </View>

                </View>



                <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between' }}>

                    {
                        arrayholder === 'DATA3' ?
                            (<WebView
                                source={{ uri: 'http://yankes.kemkes.go.id/app/siranap/wilayah?jenis=2' }}
                                style={{ marginTop: 20 }} />
                            )
                            :
                            arrayholder === 'DATA' ?
                                (<FlatList
                                    data={datat}
                                    renderItem={renderItemRST}
                                    keyExtractor={(item, index) => index.toString()}
                                    ListFooterComponent={renderFooter}
                                    ListHeaderComponent={renderHeader}
                                />)
                                :
                                (<FlatList
                                    data={data}
                                    renderItem={renderItem}
                                    keyExtractor={(item, index) => index.toString()}
                                    ListFooterComponent={renderFooter}
                                    ListHeaderComponent={renderHeader}
                                />)
                    }


                </View>

            </View>

            {/* //modal cari lokasi */}
        <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
      >
        <View style={{
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // marginTop: 22
  }}>
          <View style={{
    // margin: 20,
    backgroundColor: "white",
    borderRadius: 8,
    padding: 16,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    // width:250,
    height:200
  }}>
            <FlatList
      data={Dkota}
      renderItem={({item}) => (
      <TouchableOpacity onPress={() => (setModalVisible(!modalVisible),setSelectedValue(arrayholder === 'DATA1' ? item.address : item.city_id), getFilter('', arrayholder === 'DATA1' ? item.address : item.city_id))} style={{width:200}}>
        <Text>{arrayholder === 'DATA1' ? item.address : item.city_id}</Text>
        <View
  style={{
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    marginVertical:16
  }}
/>
      </TouchableOpacity>
      )}
    />
          </View>
        </View>
      </Modal>
        </SafeAreaView>
    )
}

export default RsYKP


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    item1: {
        backgroundColor: 'white',
        paddingVertical: 20,
        paddingHorizontal: 10,
        flexDirection: 'column',
        width: '100%',
        justifyContent: 'space-between',
        marginBottom: 2
    },
    item: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        flexDirection: 'row',
        flex: 1,
        backgroundColor: 'white',
        marginVertical: 1,
    },
    title1: {
        fontSize: 16,
        paddingVertical: 10
        // fontFamily:'Montserrat'
    },
    containerflatList: {
        flexDirection: 'row',
        flex: 1,
        margin: 5,
        justifyContent: 'space-between',
        // backgroundColor:'red',
    },
})


