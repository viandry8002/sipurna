import React, { useState, useEffect } from 'react';
import { ScrollView, StyleSheet, Text, View, Alert, ToastAndroid } from 'react-native';
import DatePicker from 'react-native-datepicker';
import { Button, Input } from 'react-native-elements';
import DocumentPicker from 'react-native-document-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import api from '../../Api';
// import DateTimePicker from '@react-native-community/datetimepicker';

const Reimbus = ({ navigation }) => {
    // const [date, setDate] = useState(new Date())
    // const [fullname, setFullname] = useState('')
    const [Tokens, setTokens] = useState(null)
    const [colorFile1, setColorFile1] = useState('#E3E3E3')
    const [colorFile2, setColorFile2] = useState('#E3E3E3')
    const [colorFile3, setColorFile3] = useState('#E3E3E3')
    const [colorFile4, setColorFile4] = useState('#E3E3E3')
    const [colorFile5, setColorFile5] = useState('#E3E3E3')

    const [colorFile6, setColorFile6] = useState('#E3E3E3')
    const [colorFile7, setColorFile7] = useState('#E3E3E3')
    const [colorFile8, setColorFile8] = useState('#E3E3E3')
    const [colorFile9, setColorFile9] = useState('#E3E3E3')
    const [colorFile10, setColorFile10] = useState('#E3E3E3')
    const [btnTouch, setbtnTouch] = useState(false)

    const [form, setForm] = useState({
        npp: '',
        nama: '',
        hp: '',
        email: '',
        tanggal: '',
        nominal: '',
        keterangan: '',

        attachment_1: '',
        attachment_2: '',
        attachment_3: '',
        attachment_4: '',
        attachment_5: '',
        attachment_6: '',
        attachment_7: '',
        attachment_8: '',
        attachment_9: '',
        attachment_10: '',
    })

    const onInputChange = (value, input) => {
        setForm({
            ...form,
            [input]: value,
        })
    }

    useEffect(() => {

        async function getToken() {
            try {
                const token = await AsyncStorage.getItem("api_token")
                const onpp = await AsyncStorage.getItem("npp")
                const fullname = await AsyncStorage.getItem("fullname")
                return Venue(onpp, fullname, token);

            } catch (err) {
                console.log(err)
            }
        }
        getToken()
    }, [])

    const Venue = (onpp, fullname, token) => {
        setTokens(token)
        setForm({
            ...form,
            ['npp']: onpp,
            ['nama']: fullname,
        })
    }

    const showToast = () => {
        ToastAndroid.show("Sukses Mengirim Data!", ToastAndroid.LONG);
    };

    const SendForm = () => {
        setbtnTouch(true)
        console.log('aze', form.attachment_1)
        let formdata = new FormData();

        formdata.append("npp", form.npp)
        formdata.append("nama", form.nama)
        formdata.append("hp", form.hp)
        formdata.append("email", form.email)
        formdata.append("tanggal", form.tanggal)
        formdata.append("nominal", form.nominal)
        formdata.append("keterangan", form.keterangan)

        formdata.append("attachment_1", form.attachment_1)
        formdata.append("attachment_2", form.attachment_2)
        formdata.append("attachment_3", form.attachment_3)
        formdata.append("attachment_4", form.attachment_4)
        formdata.append("attachment_5", form.attachment_5)
        formdata.append("attachment_6", form.attachment_6)
        formdata.append("attachment_7", form.attachment_7)
        formdata.append("attachment_8", form.attachment_8)
        formdata.append("attachment_9", form.attachment_9)
        formdata.append("attachment_10", form.attachment_10)

        // console.log(formdata._parts[7])
        if (form.npp !== '' && form.nama !== '' && form.hp !== '' && form.email !== '' && form.tanggal !== '' && form.nominal !== '' && form.keterangan !== '' && form.attachment_1 !== '') {
            Axios.post(`${api}/api/reimburse`, formdata, {
                headers: {
                    // 'Authorization': 'Bearer ' + Tokens,
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data'
                }
            }).then((res) => {
                // console.log('success', res)
                console.log('success', res.data)
                setForm('')
                showToast()
                navigation.goBack()
                setbtnTouch(false)
            }).catch((err) => {
                console.log('gagal', err.response.data)
                setForm('')
                navigation.goBack()
                setbtnTouch(false)
            })
        } else {
            setForm('')
            navigation.goBack()
            setbtnTouch(false)
            Alert.alert('Peringatan',
                'Form input tidak boleh kosong',
                [{
                    text: 'Ya',
                    onPress: () => console.log('Tidak')
                }])
        }
    }

    const OpenDocumentFile1 = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            }).then((res) => {
                // let tmpFiles = {
                //     uri: res.uri,
                //     type: res.type,
                //     name: res.name
                // }

                console.log('as', res);
                // onInputChange(tmpFiles, 'attachment_1')
                setForm({
                    ...form,
                    ['attachment_1']: {
                        uri: res.uri,
                        type: res.type,
                        name: res.name
                    },
                })
                // setForm({
                //     ...form,
                //     ['attachment_1']: {
                //         uri: 'content://com.android.providers.media.documents/document/document%3A40',
                //         type: 'application/pdf',
                //         name: 'finansial_dapen-1.pdf'
                //     },
                // })
                setColorFile1('#2C5BA4')
            })
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    const OpenDocumentFile2 = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            }).then((res) => {
                let tmpFiles = {
                    uri: res.uri,
                    type: res.type,
                    name: res.name
                }

                console.log(tmpFiles);
                onInputChange(tmpFiles, 'attachment_2')
                setColorFile2('#2C5BA4')
            })
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    const OpenDocumentFile3 = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            }).then((res) => {
                let tmpFiles = {
                    uri: res.uri,
                    type: res.type,
                    name: res.name
                }

                console.log(tmpFiles);
                onInputChange(tmpFiles, 'attachment_3')
                setColorFile3('#2C5BA4')
            })
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    const OpenDocumentFile4 = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            }).then((res) => {
                let tmpFiles = {
                    uri: res.uri,
                    type: res.type,
                    name: res.name
                }

                console.log(tmpFiles);
                onInputChange(tmpFiles, 'attachment_4')
                setColorFile4('#2C5BA4')
            })
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    const OpenDocumentFile5 = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            }).then((res) => {
                let tmpFiles = {
                    uri: res.uri,
                    type: res.type,
                    name: res.name
                }

                console.log(tmpFiles);
                onInputChange(tmpFiles, 'attachment_5')
                setColorFile5('#2C5BA4')
            })
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    const OpenDocumentFile6 = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            }).then((res) => {
                let tmpFiles = {
                    uri: res.uri,
                    type: res.type,
                    name: res.name
                }

                console.log(tmpFiles);
                onInputChange(tmpFiles, 'attachment_6')
                setColorFile6('#2C5BA4')
            })
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    const OpenDocumentFile7 = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            }).then((res) => {
                let tmpFiles = {
                    uri: res.uri,
                    type: res.type,
                    name: res.name
                }

                console.log(tmpFiles);
                onInputChange(tmpFiles, 'attachment_7')
                setColorFile7('#2C5BA4')
            })
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    const OpenDocumentFile8 = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            }).then((res) => {
                let tmpFiles = {
                    uri: res.uri,
                    type: res.type,
                    name: res.name
                }

                console.log(tmpFiles);
                onInputChange(tmpFiles, 'attachment_8')
                setColorFile8('#2C5BA4')
            })
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    const OpenDocumentFile9 = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            }).then((res) => {
                let tmpFiles = {
                    uri: res.uri,
                    type: res.type,
                    name: res.name
                }

                console.log(tmpFiles);
                onInputChange(tmpFiles, 'attachment_9')
                setColorFile9('#2C5BA4')
            })
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    const OpenDocumentFile10 = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles],
            }).then((res) => {
                let tmpFiles = {
                    uri: res.uri,
                    type: res.type,
                    name: res.name
                }

                console.log(tmpFiles);
                onInputChange(tmpFiles, 'attachment_10')
                setColorFile10('#2C5BA4')
            })
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }


    return (
        <ScrollView style={[styles.wrap, { backgroundColor: 'white' }]}>
            <View style={{ margin: 20 }}>

                <View >
                    <Text>NPP</Text>
                    <View style={styles.wrapInput}>
                        <Input
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            style={{ padding: 10 }}
                            value={form.npp}
                        />
                    </View>
                </View>

                <View >
                    <Text>Nama</Text>
                    <View style={styles.wrapInput}>
                        <Input
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            style={{ padding: 10 }}
                            value={form.nama}
                        />
                    </View>
                </View>

                <View >
                    <Text>No. Hp</Text>
                    <View style={styles.wrapInput}>
                        <Input
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            style={{ padding: 10 }}
                            value={form.hp}
                            onChangeText={value => onInputChange(value, 'hp')}
                        />
                    </View>
                </View>

                <View >
                    <Text>Email</Text>
                    <View style={styles.wrapInput}>
                        <Input
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            style={{ padding: 10 }}
                            value={form.email}
                            onChangeText={value => onInputChange(value, 'email')}
                        />
                    </View>
                </View>

                <View >
                    <Text>Tanggal Kwitansi</Text>
                    <View style={styles.wrapInput}>
                        <DatePicker
                            style={{ width: '97%', marginTop: 5 }}
                            date={form.tanggal}
                            mode="date"
                            placeholder="select date"
                            format="YYYY-MM-DD"
                            minDate="2020-01-01"
                            maxDate="2050-01-01"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            // iconComponent={<MaterialIcons name={'calendar-today'} size={20} color={colors.grey}/>}

                            customStyles={{
                                dateInput: {
                                    borderWidth: 0,
                                },
                                // ... You can check the source to find the other keys.
                            }}
                            onDateChange={value => onInputChange(value, 'tanggal')}
                        />
                    </View>
                </View>

                <View >
                    <Text>Rupiah</Text>
                    <View style={styles.wrapInput}>
                        <Input
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            style={{ padding: 10 }}
                            value={form.nominal}
                            onChangeText={value => onInputChange(value, 'nominal')}
                        />
                    </View>
                </View>

                <View >
                    <Text>Keterangan</Text>
                    <View style={styles.wrapInput}>
                        <Input
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            style={{ padding: 10 }}
                            value={form.keterangan}
                            onChangeText={value => onInputChange(value, 'keterangan')}
                        />
                    </View>
                </View>

                <View style={{ marginTop: 5 }}>
                    <Text>Attachment</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ margin: 20 }}>1</Text>
                            <Button
                                title="Upload File"
                                buttonStyle={{ marginTop: 10, borderRadius: 10, backgroundColor: colorFile1, width: '70%', height: 50 }}
                                onPress={() => OpenDocumentFile1()}
                            />
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ margin: 20 }}>2</Text>
                            <Button
                                title="Upload File"
                                buttonStyle={{ marginTop: 10, borderRadius: 10, backgroundColor: colorFile2, width: '70%', height: 50 }}
                                onPress={() => OpenDocumentFile2()}
                            />
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ margin: 20 }}>3</Text>
                            <Button
                                title="Upload File"
                                buttonStyle={{ marginTop: 10, borderRadius: 10, backgroundColor: colorFile3, width: '70%', height: 50 }}
                                onPress={() => OpenDocumentFile3()}
                            />
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ margin: 20 }}>4</Text>
                            <Button
                                title="Upload File"
                                buttonStyle={{ marginTop: 10, borderRadius: 10, backgroundColor: colorFile4, width: '70%', height: 50 }}
                                onPress={() => OpenDocumentFile4()}
                            />
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ margin: 20 }}>5</Text>
                            <Button
                                title="Upload File"
                                buttonStyle={{ marginTop: 10, borderRadius: 10, backgroundColor: colorFile5, width: '70%', height: 50 }}
                                onPress={() => OpenDocumentFile5()}
                            />
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ margin: 20 }}>6</Text>
                            <Button
                                title="Upload File"
                                buttonStyle={{ marginTop: 10, borderRadius: 10, backgroundColor: colorFile6, width: '70%', height: 50 }}
                                onPress={() => OpenDocumentFile6()}
                            />
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ margin: 20 }}>7</Text>
                            <Button
                                title="Upload File"
                                buttonStyle={{ marginTop: 10, borderRadius: 10, backgroundColor: colorFile7, width: '70%', height: 50 }}
                                onPress={() => OpenDocumentFile7()}
                            />
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ margin: 20 }}>8</Text>
                            <Button
                                title="Upload File"
                                buttonStyle={{ marginTop: 10, borderRadius: 10, backgroundColor: colorFile8, width: '70%', height: 50 }}
                                onPress={() => OpenDocumentFile8()}
                            />
                        </View>
                    </View>

                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ margin: 20 }}>9</Text>
                            <Button
                                title="Upload File"
                                buttonStyle={{ marginTop: 10, borderRadius: 10, backgroundColor: colorFile9, width: '70%', height: 50 }}
                                onPress={() => OpenDocumentFile9()}
                            />
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ margin: 20 }}>10</Text>
                            <Button
                                title="Upload File"
                                buttonStyle={{ marginTop: 10, borderRadius: 10, backgroundColor: colorFile10, width: '65%', height: 50 }}
                                onPress={() => OpenDocumentFile10()}
                            />
                        </View>
                    </View>
                </View>

                <Button
                    title="Kirim"
                    buttonStyle={{ marginTop: 20, borderRadius: 10, backgroundColor: '#2C5BA4', height: 47 }}
                    disabled={btnTouch}
                    // onPress={() =>  
                    //     Alert.alert('Peringatan',
                    //     'Maaf saat ini data belum bisa di kirim',
                    //     [{
                    //         text : 'Ya',
                    //         onPress : () => console.log('Tidak')
                    //     }])}
                    onPress={() => SendForm()}
                />
            </View>
        </ScrollView>
    )
}

export default Reimbus

const styles = StyleSheet.create({
    wrap: {
        flex: 1
    },
    wrapInput: {
        borderColor: '#C3C3C3',
        borderWidth: 1,
        borderRadius: 10,
        height: 47,
        marginVertical: 10
    },
})
