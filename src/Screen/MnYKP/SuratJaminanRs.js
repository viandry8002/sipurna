import React, { useState, useEffect, useRef } from 'react';
import { Alert, ScrollView, StyleSheet, Text, View, ToastAndroid, FlatList, TouchableOpacity,Modal,Platform } from 'react-native';
import DatePicker from 'react-native-datepicker';
import { Button, colors, Input } from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import api from '../../Api';
import { Picker } from '@react-native-picker/picker';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { Modalize } from 'react-native-modalize';
import Styles from '../../Style/Kontak';

const SuratJaminanRs = ({ navigation }) => {
    const modalizeRef = useRef(null);

    const onOpen = () => {
        modalizeRef.current?.open();
    };

    const onClose = () => {
        modalizeRef.current?.close();
    };

    const [searchRs, setsearchRs] = useState('')
    const [dtRs, setdtRs] = useState([])
    const [tglRawat, settglRawat] = useState('')
    const [Tokens, setTokens] = useState(null)
    const [btnTouch, setbtnTouch] = useState(false)
    const [form, setForm] = useState({
        UId: '',
        nama: '',
        nik: '',
        hkeluarga: '',
        tglMulaiRawat: '',
        penyakit: '',
        IdRS: '',
        NamaRS: '',
        alamatRs: '',
        teleponRs: '',
    })

    const [modalVisible, setModalVisible] = useState(false);
    const hubungan = [
        {
            value : 'peserta'
        },
        {
            value : 'pasangan'
        },
        {
            value : 'anak'
        },
    ]

    const onInputChange = (value, input) => {
        setForm({
            ...form,
            [input]: value,
        })
    }

    useEffect(() => {

        async function getToken() {
            try {
                const token = await AsyncStorage.getItem("api_token")
                const user_id = await AsyncStorage.getItem("user_id")
                return Venue(token, user_id);
            } catch (err) {
                console.log(err)
            }
        }
        getToken()
    }, [])

    const Venue = (tokens, user_id) => {
        setForm({
            ...form,
            ['UId']: user_id,
        })
        setTokens(tokens)
    }

    const getRs = () => {
        let form = {
            src: searchRs,
            kota: ''
        }
        console.log('tokennya', Tokens)

        Axios.post(`${api}/api/rsykp/search`, form, {
            headers: {
                'Authorization': 'Bearer ' + Tokens,
                'Accept': 'application/json'
            }
        }).then((res) => {
            // console.log(res.data.data)
            setdtRs(res.data.data.data)
        }).catch((err) => {
            console.log('gagal', err.response.data)
        })
    }

    const showToast = () => {
        ToastAndroid.show("Sukses Mengirim Data!", ToastAndroid.LONG);
    };

    const SendForm = () => {
        setbtnTouch(true)
        let formdata = new FormData();

        formdata.append("user_id", parseInt(form.UId))
        formdata.append("name", form.nama)
        formdata.append("nik", form.nik)
        formdata.append("type", form.hkeluarga)
        formdata.append("start_date", tglRawat)
        formdata.append("disease", form.penyakit)
        formdata.append("rs_id", form.IdRS)
        // formdata.append("rs_id", 1)

        // console.log('formmmmmmmmm', formdata)

        if (form.nama !== '' && form.nik !== '' && form.hkeluarga !== '' && tglRawat !== '' && form.IdRS !== '') {
            Axios.post(`${api}/api/rs-guarantee`, formdata, {
                headers: {
                    'Authorization': 'Bearer ' + Tokens,
                    'Accept': 'application/json'
                }
            }).then((res) => {
                console.log('success', res.data)
                setForm('')
                showToast()
                navigation.goBack()
                setbtnTouch(false)
            }).catch((err) => {
                setbtnTouch(false)
                console.log('gagal', err.response.data)
                if (err.response.data.code === 404) {
                    Alert.alert('Peringatan',
                        err.response.data.message,
                        [{
                            text: 'Ya',
                            onPress: () => { console.log('Tidak'), setbtnTouch(false) }
                        }])
                } else if (err.response.data.code === 500) {
                    Alert.alert('Perhatian',
                        err.response.data.message,
                        [{
                            text: 'Ya',
                            onPress: () => { console.log('Tidak'), setbtnTouch(false) }
                        }])
                }
            })
        } else {
            Alert.alert('Peringatan',
                'Form input tidak boleh kosong',
                [{
                    text: 'Ya',
                    onPress: () => { console.log('Tidak'), setbtnTouch(false) }
                }])
        }
    }


    return (
        <ScrollView style={[styles.wrap, { backgroundColor: 'white' }]}>
            <View style={{ margin: 20 }}>

                <View>
                    <Text style={styles.fontJudul}>Pasien Dirawat</Text>
                </View>
                <View >
                    <Text>Nama</Text>
                    <View style={styles.wrapInput}>
                        <Input
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            style={{ padding: 10 }}
                            value={form.nama}
                            onChangeText={value => onInputChange(value, 'nama')}
                        />
                    </View>
                </View>

                <View >
                    <Text>NIK</Text>
                    <View style={styles.wrapInput}>
                        <Input
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            style={{ padding: 10 }}
                            value={form.nik}
                            onChangeText={value => onInputChange(value, 'nik')}
                        />
                    </View>
                </View>

                <View >
                    <Text>Hubungan Keluarga</Text>
                    <View style={[styles.wrapInput, { justifyContent: 'center' }]}>
                    {Platform.OS === 'ios' ? (
                        <TouchableOpacity onPress={() => setModalVisible(true)} style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center',marginHorizontal:10}}>
                        <Text>{form.hkeluarga === '' ? 'Pilih Hubungan Keluarga' : form.hkeluarga }</Text>
                        <AntDesign name='caretdown' size={10} />
                        </TouchableOpacity>
                        ):
                        (
                        <Picker
                            style={{ width: '100%', height: 50 }}
                            selectedValue={form.hkeluarga}
                            onValueChange={(itemValue, itemIndex) =>
                                setForm({
                                    ...form,
                                    ['hkeluarga']: itemValue,
                                })
                                // setSelectedLanguage(itemValue)
                            }
                            mode={'dropdown'}
                        >
                            <Picker.Item label="-- Pilih Hubungan Keluarga --" value="" />
                            <Picker.Item label="peserta" value="peserta" />
                            <Picker.Item label="pasangan" value="pasangan" />
                            <Picker.Item label="anak" value="anak" />
                        </Picker>)}

                    </View>
                </View>

                <View >
                    <Text>Tanggal Mulai Dirawat</Text>
                    <View style={styles.wrapInput}>
                        <DatePicker
                            style={{ width: '97%', marginTop: 5 }}
                            date={tglRawat}
                            mode="date"
                            placeholder="select date"
                            format="YYYY-MM-DD"
                            minDate="2020-01-01"
                            maxDate="2050-01-01"
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateInput: {
                                    borderWidth: 0,
                                },
                                // ... You can check the source to find the other keys.
                            }}
                            // onDateChange={value => onInputChange(value, 'tglMulaiRawat')}
                            onDateChange={(value) => { settglRawat(value) }}
                        // onDateChange={value => onInputChange(value, 'tglMulaiRawat')}
                        // onDateChange={value => console.log(value)}
                        />
                    </View>
                </View>

                <View >
                    <Text>Penyakit</Text>
                    <View style={styles.wrapInput}>
                        <Input
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            style={{ padding: 10 }}
                            value={form.rs}
                            onChangeText={value => onInputChange(value, 'penyakit')}
                        />
                    </View>
                </View>

                <View>
                    <Text style={styles.fontJudul}>Rumah Sakit</Text>
                </View>

                <View >
                    <Text>Nama Rumah Sakit</Text>
                    <View style={styles.wrapInput}>
                        <Input
                            placeholder={'Pilih Rumah Sakit'}
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            style={{ padding: 10 }}
                            value={form.NamaRS}
                            rightIcon={() => <AntDesign name='caretdown' size={10} color={'grey'} />}
                            // onChangeText={value => onInputChange(value, 'rs')}
                            onTouchStart={() => onOpen()}
                            // onPressIn={() => { console.log('aszzzzzzzzz') }}
                            showSoftInputOnFocus={false}
                        // disabled={true}
                        />
                    </View>
                </View>

                <View >
                    <Text>Alamat</Text>
                    <View style={styles.wrapInput}>
                        <Input
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            style={{ padding: 10 }}
                            value={form.alamatRs}
                            // onChangeText={value => onInputChange(value, 'alamatRs')}
                            disabled={true}
                        />
                    </View>
                </View>

                <View >
                    <Text>Telepon</Text>
                    <View style={styles.wrapInput}>
                        <Input
                            inputContainerStyle={{ borderBottomWidth: 0 }}
                            style={{ padding: 10 }}
                            value={form.teleponRs}
                            // onChangeText={value => onInputChange(value, 'teleponRs')}
                            disabled={true}
                        />
                    </View>
                </View>

                <Button
                    title="Kirim"
                    buttonStyle={{ marginTop: 20, borderRadius: 10, backgroundColor: '#2C5BA4', height: 47 }}
                    disabled={btnTouch}
                    // onPress={() =>  
                    //     Alert.alert('Peringatan',
                    //     'Maaf saat ini data belum bisa di kirim',
                    //     [{
                    //         text : 'Ya',
                    //         onPress : () => console.log('Tidak')
                    //     }])}
                    onPress={() => SendForm()}
                />

            </View>
            <Modalize
                ref={modalizeRef}
                // scrollViewProps={{ showsVerticalScrollIndicator: false }}
                snapPoint={400}
                handlePosition={'inside'}
                handleStyle={{ backgroundColor: 'grey' }}
            >
                <View style={{ margin: 16 }}>
                    <View >
                        <View>
                            <Text style={{
                                fontWeight: '700',
                                marginTop: 10
                            }}>Pilih Rumah Sakit</Text>
                        </View>
                        <View style={styles.wrapInput}>
                            <Input
                                placeholder={'Cari Rumah Sakit.....'}
                                inputContainerStyle={{ borderBottomWidth: 0 }}
                                style={{ padding: 10 }}
                                value={searchRs}
                                onChangeText={value => setsearchRs(value, 'rs')}
                                rightIcon={() => <TouchableOpacity style={{ backgroundColor: '#2C5BA4', width: 65, height: 30, borderRadius: 5, justifyContent: 'space-around', alignItems: 'center', flexDirection: 'row' }} onPress={() => getRs()}>
                                    {/* <Text style={{ color: colors.white }}>Cari</Text> */}
                                    <AntDesign name='search1' size={17} color={colors.white} />
                                </TouchableOpacity>}
                            />
                        </View>

                        <FlatList
                            data={dtRs}
                            renderItem={({ item }) => (
                                <TouchableOpacity style={{ marginVertical: 10 }} onPress={() => {
                                    setForm({
                                        ...form,
                                        ['IdRS']: item.id,
                                        ['NamaRS']: item.title,
                                        ['alamatRs']: item.area,
                                        ['teleponRs']: item.phone,
                                    })
                                    onClose()

                                }}>
                                    <Text>{item.title}</Text>
                                    <View
                                        style={{
                                            marginTop: 10,
                                            borderBottomColor: 'grey',
                                            borderBottomWidth: 0.5,
                                        }}
                                    />
                                </TouchableOpacity>
                            )}
                            keyExtractor={item => item.id}
                        />
                    </View>
                </View>
            </Modalize>

        {/* //modal hubungan keluarga */}
        <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
      >
        <View style={{
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // marginTop: 22
  }}>
          <View style={{
    // margin: 20,
    backgroundColor: "white",
    borderRadius: 8,
    padding: 16,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    // width:250,
    height:200
  }}>
            <FlatList
      data={hubungan}
      renderItem={({item}) => (
      <TouchableOpacity onPress={() => (setModalVisible(!modalVisible), setForm({
        ...form,
        ['hkeluarga']: item.value,
    }))} style={{width:200}}>
        <Text>{item.value}</Text>
        <View
  style={{
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    marginVertical:16
  }}
/>
      </TouchableOpacity>
      )}
    />
          </View>
        </View>
      </Modal>
        </ScrollView>
    )
}

export default SuratJaminanRs

const styles = StyleSheet.create({
    wrap: {
        flex: 1
    },
    wrapInput: {
        borderColor: '#C3C3C3',
        borderWidth: 1,
        borderRadius: 10,
        height: 47,
        marginVertical: 10
    },
    fontJudul: {
        fontWeight: '700',
        marginVertical: 10
    }
})


