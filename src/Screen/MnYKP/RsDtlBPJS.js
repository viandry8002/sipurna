import MapboxGL from '@react-native-mapbox-gl/maps';
import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, Text, View, TouchableOpacity, Linking } from 'react-native';

MapboxGL.setAccessToken('pk.eyJ1IjoidmlhbjIwOCIsImEiOiJja2hsbjRnZGUwMndiMzNtazRpc3ZrZ2tlIn0.IMZF3r8zV4Eq0CpywQXd3Q')

const RsDtlBPJS = ({ navigation, route }) => {
    const [data, setData] = useState(route.params)
    let Longtitude = route.params.longitude.replace(/,/g, '');
    let Latitude = route.params.latitude.replace(/,/g, '');
    console.log('longtitude',Longtitude);
    console.log('latitude',Latitude);

    return (
        <SafeAreaView style={styles.container}>
            <View style={{ flex: 1, flexDirection: 'column' }}>

                {/* content */}
                <View style={styles.item}>
                    <View style={{ borderWidth: 1, borderColor: '#C3C3C3', padding: 10, margin: 5, flex: 1, borderRadius: 10 }}>
                        <Text style={{ padding: 10, fontSize: 18 }}>{data.title}</Text>
                        <Text style={{ paddingHorizontal: 10, color: '#2C5BA4' }}>{data.type_id} </Text>
                        <Text style={{ paddingHorizontal: 10, color: '#669F16' }}>Kode Faskes : {data.code}</Text>

                        <View style={{ margin: 10 }}>
                            <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                                <Text style={styles.container}>Provinsi</Text>
                                <Text style={{ textAlign: 'right', flex: 2 }}>{data.province_id}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                                <Text style={styles.container}>Kota / Kabupaten</Text>
                                <Text style={{ textAlign: 'right', flex: 2 }}>{data.city_id}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                                <Text style={styles.container}>Kecamatan</Text>
                                <Text style={{ textAlign: 'right', flex: 2 }}>{data.district_id}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                                <Text style={styles.container}>Alamat</Text>
                                <Text style={{ textAlign: 'right', flex: 2 }}>{data.address}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                                <Text style={styles.container}>Kode POS</Text>
                                <Text style={{ textAlign: 'right', flex: 2 }}>{data.postcode}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                                <Text style={styles.container}>No. Telp</Text>
                                <Text style={{ textAlign: 'right', flex: 2 }}>{data.phone}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginVertical: 5 }}>
                                <Text style={styles.container}>Jadwal Praktek</Text>
                                <Text style={{ textAlign: 'right', flex: 2 }}>{data.schedule_open}</Text>
                            </View>
                        </View>

                        {
                            data.latitude === null && data.longitude === null || data.latitude === '' && data.longitude === '' ?
                                <View style={{
                                    justifyContent: 'center', alignItems: 'center', flex: 1
                                }}><Text>Lokasi tidak ditemukan</Text></View>
                                :
                                <View>
                                    <MapboxGL.MapView style={{ width: '95%', height: 186, marginTop: 20, marginHorizontal: 10 }}>
                                        <MapboxGL.Camera
                                            // centerCoordinate={[data.longitude.replace(/,/g, ''), data.latitude.replace(/,/g, '')]}
                                            centerCoordinate={[parseFloat(Longtitude),parseFloat(Latitude)]}
                                            // centerCoordinate={[107.580110, -6.890066]}
                                            zoomLevel={15}
                                        />
                                        <MapboxGL.PointAnnotation
                                            id="pointAnnotation"
                                            // coordinate={[data.longitude.replace(/,/g, ''), data.latitude.replace(/,/g, '')]}
                                            coordinate={[parseFloat(Longtitude),parseFloat(Latitude)]}
                                        // coordinate={[107.580110, -6.890066]}
                                        >
                                            <MapboxGL.Callout
                                                title={data.name}
                                            />
                                        </MapboxGL.PointAnnotation>

                                    </MapboxGL.MapView>
                                    <View style={[styles.containrs, { marginTop: 10 }]}>
                                        <TouchableOpacity onPress={() => Linking.openURL('google.navigation:q=' + data.latitude.replace(/,/g, '') + '+' + data.longitude.replace(/,/g, '') + '')}>
                                            <Text style={{ color: 'blue' }}>Lihat di Maps</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                        }
                        {/* </View> */}

                    </View>
                </View>
                {/* content */}

            </View>
        </SafeAreaView>
    )
}

export default RsDtlBPJS


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    item: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        flexDirection: 'row',
        flex: 1,
        backgroundColor: 'white',
        marginVertical: 1,
    },
})
