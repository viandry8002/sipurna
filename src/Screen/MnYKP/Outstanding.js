import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { FlatList, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements';
import api from '../../Api';
import { TextMask } from 'react-native-masked-text';
import moment from "moment";

const Outstanding = ({navigation}) => {
  const [dtnama, setDtname] = useState('')
  const [dtnpp, setDtnpp] = useState('')
  const [data, setData] = useState([])
  const [pdata, setPdata] = useState('')
  
      useEffect(() => {
        async function getToken() {
            try{
                const token = await AsyncStorage.getItem("api_token")
                const name = await AsyncStorage.getItem("fullname")
                const npp = await AsyncStorage.getItem("npp")
                return getVenue(token,name,npp)
            }catch(err){
                console.log(err)
            }
        }
        getToken()
    },[])

    const getVenue = (token,name,npp) =>{
        setDtname(name)
        setDtnpp(npp)

        // Axios.get(`${api}/api/outstand/13676`,{
        Axios.get(`${api}/api/outstand/${npp}`,{
            headers : {
                'Authorization' : 'Bearer ' + token,
                'Accept' : 'application/json'
            }
        }).then((res) =>{
            setData(res.data.data)
            setPdata(res.data.data[0].position_date)

        }).catch((err) =>{
          console.log(err.response.data)
            if(err.response.data === 'Unauthorized.'){
                navigation.navigate('OnKick')
              }else if(err.response.data.code === 400){
                setData(err.response.data.message)
              }
        })
    }
    
    const renderItem = ({item}) => {
      let bckrnd = '' ;

      if(item.type === 'rawat_inap'){
        bckrnd = 'rgba(47, 128, 237, 0.25)';
      }else if(item.type === 'gawat_darurat'){
        bckrnd = 'rgba(239, 58, 58, 0.25)';
      }else if(item.type === 'perawatan_gigi,_kacamata_&_alat_bantu_dengar'){
        bckrnd = 'rgba(39, 174, 96, 0.25)';
      }else if(item.type === 'penunjang_diagnostik_&_rehabilitasi_medis'){
        bckrnd = 'rgba(255, 199, 0, 0.25)';
      }else if(item.type === 'penyakit_khusus'){
        bckrnd = 'rgba(224, 224, 224, 0.25)';
      }
      
      return (
        <View style={styles.item}>
        <View style={[styles.containerflatList,{backgroundColor: bckrnd}]}>
        <View style={{alignItems:'center'}}>
        <Text style={styles.title1}>{((item.type).replace(/_|-/g, " ")).toUpperCase()}</Text>
        </View>

        <View style={{flexDirection:'row',justifyContent: 'space-between',}}>
        <Text>Maksimum</Text>
        <TextMask
            type={'money'}
            options={{
                precision: 0,
                separator: '.',
                delimiter: '.',
                unit: 'Rp. ',
                suffixUnit: ''
            }}
            value={item.maksimum}
            />
        </View>

        <View style={{flexDirection:'row',justifyContent: 'space-between',}}>
        <Text>Mutasi</Text>
        <TextMask
            type={'money'}
            options={{
                precision: 0,
                separator: '.',
                delimiter: '.',
                unit: 'Rp. ',
                suffixUnit: ''
            }}
            value={item.mutasi}
            />
        </View>

        <View style={{flexDirection:'row',justifyContent: 'space-between',}}>
        <Text>Saldo</Text>
        <TextMask
            type={'money'}
            options={{
                precision: 0,
                separator: '.',
                delimiter: '.',
                unit: 'Rp. ',
                suffixUnit: ''
            }}
            value={item.saldo}
            />
        </View>

        </View>
        </View>
        )
      };

    return (
        <SafeAreaView style={styles.container}>
        <View style={{flex:1,flexDirection:'column'}}>
        <View style={styles.item1}>
        <Button title="Plafond" type="outline" titleStyle={{color:'black'}} buttonStyle={{paddingHorizontal:45}}  onPress={() => navigation.navigate('Plafond')}/>
        <Button title="Outstanding" buttonStyle={{backgroundColor:'#2C5BA4',paddingHorizontal:45}} onPress={() => navigation.navigate('Outstanding')}/>
        </View>
        
        <ScrollView>
        <View style={{flex:1,flexDirection:'column',justifyContent: 'space-between',backgroundColor:'white',padding:10}}>
         <View style={{alignItems:'center',borderWidth:1,borderColor:'#C3C3C3',borderTopLeftRadius:10,borderTopRightRadius:10,padding:20}}>
         <Text style={{fontSize:16}}>{dtnama}</Text>
             <Text style={{color:'#669F16'}}>NPP : {dtnpp}</Text>
             <Text>Posisi Data : {moment(pdata).format("DD-MM-YYYY")}</Text>
         </View>

        <View style={{borderWidth:1,borderColor:'#C3C3C3',padding:5}}>
        {
          data === 'Tidak Ada Data' ?
          (<View><Text style={{textAlign:'center'}}>Tidak Ada Data</Text></View>)
          :
          (
            <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
            />
          )
        }
        </View>

        <View style={{padding:10}}>
        <Text style={{alignSelf:'center',color:'red'}}>CATATAN</Text>
        <Text style={{color:'red'}}>Pensiun Janda limitasi bantuan 75% dari palfond
          Pensiun anak yatim dengan satu anak 25% dari plafond
          Pensiun anak yatim dengan dua anak  50% dari plafond
          Pensiun anak yatim dengan tiga anak  75% dari plafond
        </Text>
        </View>

         </View>
         </ScrollView>
         

        </View>
    </SafeAreaView>
    )
}

export default Outstanding


const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
      item1: {
        backgroundColor: 'white',
        paddingVertical: 20,
        paddingHorizontal:10,
        flexDirection : 'row',
        width:'100%',
        height: 86,
        justifyContent: 'space-between',
        marginBottom:5
      },
    item: {
        paddingHorizontal: 5,
        flexDirection : 'row',
        flex: 1
      },
      title1: {
        fontSize: 16,
        paddingVertical:10,
        textAlign:'center'
        // fontFamily:'Montserrat'
      },
      containerflatList : {
        flexDirection : 'column',
        flex: 1,
        margin:5,
        padding:10,
        borderRadius:10
      },
})
