import React, { useState, useEffect } from 'react';
import { Image, SafeAreaView, StatusBar, StyleSheet, TouchableOpacity, View, Text, PermissionsAndroid, ActivityIndicator, ToastAndroid } from 'react-native';
import colors from '../../Style/Colors';
// import Geolocation from '@react-native-community/geolocation';
import { Alert } from 'react-native';
import GetLocation from 'react-native-get-location'

const Kesehatan = ({ navigation }) => {
    const [currentLongitude, setCurrentLongitude] = useState('...');
    const [currentLatitude, setCurrentLatitude] = useState('...');
    const [locationStatus, setLocationStatus] = useState('load');

    useEffect(() => {
        getloc()
    }, []);

    const getloc = () => {
        // setLocationStatus()
        setLocationStatus('load');
        setTimeout(() => {
            GetLocation.getCurrentPosition({
                enableHighAccuracy: true,
                timeout: 15000,
            })
                .then(location => {
                    // console.log('berhasilllllllllll lokasinyaaa long lat = ', [location.longitude, location.latitude]);
                    setCurrentLongitude(location.longitude);
                    setCurrentLatitude(location.latitude);
                    setLocationStatus('berhasil')
                    showToast()
                })
                .catch(error => {
                    const { code, message } = error;
                    // console.warn(code, message);
                    console.log('error', message)
                    if (message === 'Location not available') {
                        //         Alert.alert('Peringatan',
                        // 'Untuk masuk Ke menu Rumah Sakit,nyalakan GPS terlebih dahulu')
                        setLocationStatus('Location not available')
                    }
                })
        }, 100);
    }

    const showToast = () => {
        ToastAndroid.show("Lokasi anda telah terdeteksi !", ToastAndroid.LONG);
    };

    const navRs = () => {
        if (locationStatus === 'Location not available') {
            Alert.alert('Peringatan',
                'Untuk masuk Ke menu Rumah Sakit,nyalakan GPS terlebih dahulu',
                [{
                    text: 'OK',
                    onPress: () => getloc()
                }])
        } else {
            navigation.navigate('Rs', { latitude: currentLatitude, longitude: currentLongitude })
        }
    }

    return (
        <SafeAreaView style={styles.container}>
            <Image source={require('../../Assets/drawable-xhdpi/Group253.png')} style={{ flex: 1, width: '100%', height: '100%', position: 'absolute', marginTop: 100 }} />

            <View style={{ backgroundColor: 'rgba(255,255,255,0.8)', flex: 1 }}>
                <StatusBar translucent backgroundColor='transparent' barStyle="light-content" />
                {
                    locationStatus === 'load' || locationStatus === 'Getting Location ...' ?
                        <View style={{
                            justifyContent: 'center', flex: 1, alignItems: 'center'
                        }}>
                            <ActivityIndicator
                                visible={true}
                                textContent={'Loading...'}
                                style={{ alignSelf: 'center' }}
                                size="large"
                                color="#2C5BA4"
                            />
                        </View>
                        :
                        <View style={{
                            flexDirection: 'column', alignItems: 'center'
                            //  ,marginBottom:480
                        }}>
                            {/* box Menu */}
                            <View style={{ margin: 20 }}>
                                {/* <Text
                                    style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        marginTop: 16,
                                    }}>
                                    {locationStatus}
                                </Text>
                                <Text
                                    style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        marginTop: 16,
                                    }}>
                                    Longitude: {currentLongitude}
                                </Text>
                                <Text
                                    style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        marginTop: 16,
                                    }}>
                                    Latitude: {currentLatitude}
                                </Text> */}
                                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                    <TouchableOpacity onPress={() => navigation.navigate('Plafond')}>
                                        <Image source={require('../../Assets/drawable-xhdpi/ListMenuYKP/Kesehatan/Group123.png')} style={styles.btnImageMenu} />
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => navigation.navigate('Reimbus')}>
                                        <Image source={require('../../Assets/drawable-xhdpi/ListMenuYKP/Kesehatan/Group124.png')} style={styles.btnImageMenu} />
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                    <TouchableOpacity onPress={() => navigation.navigate('JaminanRS')}>
                                        <Image source={require('../../Assets/drawable-xhdpi/ListMenuYKP/Kesehatan/Group126.png')} style={styles.btnImageMenu} />
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => navRs()}>
                                        <Image source={require('../../Assets/drawable-xhdpi/ListMenuYKP/Kesehatan/Group125.png')} style={styles.btnImageMenu} />
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                    <TouchableOpacity onPress={() => navigation.navigate('SuratJaminanRs')}>
                                        <Image source={require('../../Assets/drawable-xhdpi/ListMenuYKP/Kesehatan/Group332.png')} style={styles.btnImageMenu1} />
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => navigation.navigate('HistorySJRS')}>
                                        <Image source={require('../../Assets/drawable-xhdpi/ListMenuYKP/Kesehatan/Group333.png')} style={styles.btnImageMenu1} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                }
            </View>

            {/* box Menu */}
        </SafeAreaView>
    )
}

export default Kesehatan

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    btnImageMenu: {
        width: 160,
        height: 160,
        margin: 10
    },
    btnImageMenu1: {
        width: 145,
        height: 145,
        margin: 10
    }
})
