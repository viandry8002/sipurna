import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { FlatList, ScrollView, StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements';
import api from '../../Api';
import { TextMask } from 'react-native-masked-text';

const Plafond = ({navigation}) => {
  const [dtnama, setDtname] = useState('')
  const [dtnpp, setDtnpp] = useState('')
  const [data, setData] = useState([])
  
      useEffect(() => {
        async function getToken() {
            try{
                const token = await AsyncStorage.getItem("api_token")
                const name = await AsyncStorage.getItem("fullname")
                const npp = await AsyncStorage.getItem("npp")
                return getVenue(token,name,npp)
            }catch(err){
                console.log(err)
            }
        }
        getToken()
    },[])

    const getVenue = (token,name,npp) =>{
        setDtname(name)
        setDtnpp(npp)

        // Axios.get(`${api}/api/plafond/13676`,{
        Axios.get(`${api}/api/plafond/${npp}`,{
            headers : {
                'Authorization' : 'Bearer ' + token,
                'Accept' : 'application/json'
            }
        }).then((res) =>{
            setData(res.data.data)
        }).catch((err) =>{
          console.log(err.response.data)
          if(err.response.data === 'Unauthorized.'){
              navigation.navigate('OnKick')
            }
        })
    }

    return (
        <View style={styles.container}>
        <View style={{flex:1,flexDirection:'column'}}>
        <View style={styles.item1}>
        <Button title="Plafond" buttonStyle={{backgroundColor:'#2C5BA4',paddingHorizontal:45}} onPress={() => navigation.navigate('Plafond')}/>
        <Button title="Outstanding" type="outline" titleStyle={{color:'black'}} buttonStyle={{paddingHorizontal:45}} onPress={() => navigation.navigate('Outstanding')}/>
        </View>
        
        <ScrollView>
        <View style={{flex:1,flexDirection:'column',justifyContent: 'space-between',backgroundColor:'white',padding:10}}>
         <View style={{alignItems:'center',borderWidth:1,borderColor:'#C3C3C3',borderTopLeftRadius:10,borderTopRightRadius:10,padding:20}}>
             <Text style={{fontSize:16}}>{dtnama}</Text>
             <Text style={{color:'#669F16'}}>NPP : {dtnpp}</Text>
             {/* <Text>Posisi Data : 16/02/2021</Text> */}
         </View>

        <View style={{borderWidth:1,borderColor:'#C3C3C3',padding:5}}>
      
        {/* rawat_inap */}
        <View style={styles.item}>
        <View style={[styles.containerflatList,{
        backgroundColor:'rgba(47, 128, 237, 0.25)'}]}>
        <View style={{alignItems:'center'}}>
        <Text style={styles.title1}>RAWAT INAP</Text>
        </View>
        {
            data.map((datas, index)=>{
            if(datas.type === 'rawat_inap'){
              return (
                <View style={{flexDirection:'row',justifyContent: 'space-between',}}>
                <Text>{datas.type_class.toUpperCase()}</Text>
                <TextMask
                  type={'money'}
                  options={{
                      precision: 0,
                      separator: '.',
                      delimiter: '.',
                      unit: 'Rp. ',
                      suffixUnit: ''
                  }}
                  value={datas.amount}
                  />
                </View>
              )
            }else{

            }
          })
        }
        </View>
        </View>
        {/* gawat_darurat */}
        <View style={styles.item}>
        <View style={[styles.containerflatList,{
        backgroundColor:'rgba(239, 58, 58, 0.25)'}]}>
        <View style={{alignItems:'center'}}>
        <Text style={styles.title1}>GAWAT DARURAT</Text>
        </View>
        {
            data.map((datas, index)=>{
            if(datas.type === 'gawat_darurat'){
              return (
                <View style={{flexDirection:'row',justifyContent: 'space-between',}}>
                <Text>{datas.type_class.toUpperCase()}</Text>
                <TextMask
                  type={'money'}
                  options={{
                      precision: 0,
                      separator: '.',
                      delimiter: '.',
                      unit: 'Rp. ',
                      suffixUnit: ''
                  }}
                  value={datas.amount}
                  />
                </View>
              )
            }else{

            }
          })
        }
        </View>
        </View>
        {/* rehabilitasi_medis */}
        <View style={styles.item}>
        <View style={[styles.containerflatList,{
        backgroundColor:'rgba(39, 174, 96, 0.25)'}]}>
        <View style={{alignItems:'center'}}>
        <Text style={styles.title1}>{'PERAWATAN GIGI, KACAMATA & ALAT BANTU DENGAR'}</Text>
        </View>
        {
            data.map((datas, index)=>{
            if(datas.type === 'perawatan_gigi,_kacamata_&_alat_bantu_dengar'){
              return (
                <View style={{flexDirection:'row',justifyContent: 'space-between',}}>
                <Text>{datas.type_class.toUpperCase()}</Text>
                <TextMask
                  type={'money'}
                  options={{
                      precision: 0,
                      separator: '.',
                      delimiter: '.',
                      unit: 'Rp. ',
                      suffixUnit: ''
                  }}
                  value={datas.amount}
                  />
                </View>
              )
            }else{

            }
          })
        }
        </View>
        </View>
        {/* penunjang_diagnostic */}
        <View style={styles.item}>
        <View style={[styles.containerflatList,{
        backgroundColor:'rgba(255, 199, 0, 0.25)'}]}>
        <View style={{alignItems:'center'}}>
        <Text style={styles.title1}>{'PENUNJANG DIAGNOSTIK & REHABILITASI MEDIS'}</Text>
        </View>
        {
            data.map((datas, index)=>{
            if(datas.type === 'penunjang_diagnostik_&_rehabilitasi_medis'){
              return (
                <View style={{flexDirection:'row',justifyContent: 'space-between',}}>
                <Text>{datas.type_class.toUpperCase()}</Text>
                <TextMask
                  type={'money'}
                  options={{
                      precision: 0,
                      separator: '.',
                      delimiter: '.',
                      unit: 'Rp. ',
                      suffixUnit: ''
                  }}
                  value={datas.amount}
                  />
                </View>
              )
            }else{

            }
          })
        }
        </View>
        </View>
         {/* penyakit_khusus */}
         <View style={styles.item}>
         <View style={[styles.containerflatList,{
        backgroundColor:'rgba(224, 224, 224, 0.25)'}]}>
        <View style={{alignItems:'center'}}>
        <Text style={styles.title1}>PENYAKIT KHUSUS</Text>
        </View>
        {
            data.map((datas, index)=>{
            if(datas.type === 'penyakit_khusus'){
              return (
                <View style={{flexDirection:'row',justifyContent: 'space-between',}}>
                <Text>{datas.type_class.toUpperCase()}</Text>
                <TextMask
                  type={'money'}
                  options={{
                      precision: 0,
                      separator: '.',
                      delimiter: '.',
                      unit: 'Rp. ',
                      suffixUnit: ''
                  }}
                  value={datas.amount}
                  />
                </View>
              )
            }else{

            }
          })
        }
        </View>
        </View>
        

        </View>

         </View>
         </ScrollView>

        </View>
    </View>
    )
}

export default Plafond


const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
      item1: {
        backgroundColor: 'white',
        paddingVertical: 20,
        paddingHorizontal:10,
        flexDirection : 'row',
        width:'100%',
        height: 86,
        justifyContent: 'space-between',
        marginBottom:5
      },
    item: {
        paddingHorizontal: 5,
        flexDirection : 'row',
        flex: 1
      },
      title1: {
        fontSize: 16,
        paddingVertical:10,
        textAlign:'center'
      },
      containerflatList : {
        flexDirection : 'column',
        flex: 1,
        margin:5,
        padding:10,
        borderRadius:10
      },
})


