import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-elements';
import api from '../../Api';

const InfoYKP = ({navigation}) => {
  const [data, setData] = useState([])

    useEffect(() => {
        async function getToken() {
             try{
                 const token = await AsyncStorage.getItem("api_token")
                 return getVenue(token)
             }catch(err){
                 console.log(err)
             }
        }
        getToken()
    },[])

    const getVenue = (token) =>{
        Axios.get(`${api}/api/configuration/ykp`,{
            timeout : 20000,
            headers : {
                'Authorization' : 'Bearer ' + token,
                'Accept' : 'application/json'
            }
        }).then((res) =>{
            setData(res.data.data)

        }).catch((err) =>{
          console.log(err.response.data)
          if(err.response.data === 'Unauthorized.'){
              navigation.navigate('OnKick')
            }
        })
    }
    return (
        <SafeAreaView style={styles.container}>
        <View style={{flex:1,flexDirection:'column'}}>
        <View style={styles.item1}>
        <ScrollView horizontal={true}>
        <Button title="Maksud dan Tujuan" buttonStyle={{backgroundColor:'#2C5BA4',marginRight:10}} onPress={() => navigation.navigate('InfoYKP')}/>
        <Button title="Daftar Pengurus" type="outline" titleStyle={{color:'black'}} buttonStyle={{marginRight:10}} onPress={() => navigation.navigate('PengurusYKP')} />
        <Button title="Info Kegiatan" type="outline" titleStyle={{color:'black'}} onPress={() => navigation.navigate('KegiatanYKP')}/>
        </ScrollView>
        </View>
        
        <View style={styles.item2}>
          <Text style={{fontSize:18,paddingRight:10,marginVertical:10}}>Maksud dan Tujuan</Text>
          <Text>
            {data.maksud_tujuan}
          </Text>

        </View>

        </View>
    </SafeAreaView>
    )
}

export default InfoYKP

const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
      item1: {
        backgroundColor: 'white',
        paddingVertical: 20,
        paddingHorizontal:15,
        flexDirection : 'row',
        width:'100%',
        height: 86,
        justifyContent: 'space-between',
        marginBottom:10
      },
      item2: {
        backgroundColor: 'white',
        padding:20,
        flexDirection : 'column',
        width:'100%',
        justifyContent: 'space-between',
      },
})
