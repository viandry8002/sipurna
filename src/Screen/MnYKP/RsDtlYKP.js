import MapboxGL from '@react-native-mapbox-gl/maps';
import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, Text, View, TouchableOpacity, Linking } from 'react-native';

MapboxGL.setAccessToken('pk.eyJ1IjoidmlhbjIwOCIsImEiOiJja2hsbjRnZGUwMndiMzNtazRpc3ZrZ2tlIn0.IMZF3r8zV4Eq0CpywQXd3Q')

const RsDtlYKP = ({ navigation, route }) => {
    console.log('dataaaaaaaaa', route.params)
    const [data, setData] = useState(route.params)
    let Longtitude = route.params.longitude.replace(/,/g, '');
    let Latitude = route.params.latitude.replace(/,/g, '');
    console.log('longtitude',Longtitude);
    console.log('latitude',Latitude);

    return (
        <SafeAreaView style={styles.container}>
            <View style={{ flex: 1, flexDirection: 'column' }}>

                {/* content */}
                <View style={styles.item}>
                    <View style={{ borderWidth: 1, borderColor: '#C3C3C3', padding: 10, margin: 5, flex: 1, borderRadius: 10 }}>
                        <Text style={{ padding: 10, fontSize: 18 }}>{data.title}</Text>

                        <View style={styles.containrs}>
                            <Text style={styles.containdtl} >Daerah</Text>
                            <Text>: {data.address}</Text>
                        </View>
                        <View style={styles.containrs}>
                            <Text style={styles.containdtl}>No. Telp</Text>
                            <Text>: {data.phone}</Text>
                        </View>
                        <View style={styles.containrs}>
                            <Text style={styles.containdtl}>No. Faks</Text>
                            <Text>: {data.fax}</Text>
                        </View>
                        <View style={styles.containrs}>
                            <Text style={styles.containdtl}>Alamat</Text>
                            <View style={{ paddingRight: 50 }}>
                                <Text>: {data.area}</Text>
                            </View>
                        </View>

                        {
                            // <MapboxGL.MapView style={{ width: '100%', height: 186, marginTop: 20 }}>
                            //     <MapboxGL.Camera
                            //         centerCoordinate={[107.580110, -6.890066]}
                            //         zoomLevel={15}
                            //     />
                            //     <MapboxGL.PointAnnotation
                            //         id="pointAnnotation"
                            //         coordinate={[107.580110, -6.890066]}
                            //     >
                            //         <MapboxGL.Callout
                            //             title={data.name}
                            //         />
                            //     </MapboxGL.PointAnnotation>

                            // </MapboxGL.MapView>

                            data.latitude === null && data.longitude === null || data.latitude === '' && data.longitude === '' ?
                                <View style={{
                                    justifyContent: 'center', alignItems: 'center', flex: 1
                                }}><Text>Lokasi tidak ditemukan</Text></View>
                                :
                                <View>
                                    <MapboxGL.MapView style={{ width: '100%', height: 186, marginTop: 20 }}>
                                        <MapboxGL.Camera
                                        // centerCoordinate={[data.longitude.replace(/,/g, ''), data.latitude.replace(/,/g, '')]}
                                            centerCoordinate={[parseFloat(Longtitude),parseFloat(Latitude)]}
                                            // centerCoordinate={data.longitude && data.latitude == '' || null? [107.580110, -6.890066] : [data.longitude,data.latitude]}
                                            // centerCoordinate={[107.580110, -6.890066]}
                                            zoomLevel={16}
                                        />
                                        <MapboxGL.PointAnnotation
                                            id="pointAnnotation"
                                            // coordinate={[data.longitude.replace(/,/g, ''), data.latitude.replace(/,/g, '')]}
                                            coordinate={[parseFloat(Longtitude),parseFloat(Latitude)]}
                                        // coordinate={[107.580110, -6.890066]}
                                        // coordinate={data.longitude && data.latitude == '' || null? [107.580110, -6.890066] : [data.longitude,data.latitude]}
                                        >
                                            <MapboxGL.Callout
                                                title={data.name}
                                            />
                                        </MapboxGL.PointAnnotation>

                                    </MapboxGL.MapView>
                                    <View style={[styles.containrs, { marginTop: 10 }]}>
                                        <TouchableOpacity onPress={() => Linking.openURL('google.navigation:q=' + data.latitude.replace(/,/g, '') + '+' + data.longitude.replace(/,/g, '') + '')}>
                                            <Text style={{ color: 'blue' }}>Lihat di Maps</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                        }
                    </View>
                </View>
                {/* content */}

            </View>
        </SafeAreaView>
    )
}

export default RsDtlYKP

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    item: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        flexDirection: 'row',
        flex: 1,
        backgroundColor: 'white',
        marginVertical: 1,
    },
    containrs: {
        flexDirection: 'row',
        padding: 10
    },
    containdtl: {
        paddingRight: 20
    }
})
