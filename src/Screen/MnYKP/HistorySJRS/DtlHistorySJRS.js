import React, { useState, useEffect } from 'react';
import { ScrollView, StyleSheet, Text, TouchableOpacity, View, Image, ToastAndroid, Alert } from 'react-native'
import colors from '../../../Style/Colors';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import AntDesign from 'react-native-vector-icons/AntDesign';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import api from '../../../Api';
import RNFetchBlob from 'rn-fetch-blob';
import moment from 'moment';

const DtlHistorySJRS = ({ navigation, route }) => {
    const [data, setData] = useState([])
    const [idPengajuan, setIdPengajuan] = useState('')
    const [code, setCode] = useState('')
    const [Tokens, setTokens] = useState(null)

    useEffect(() => {

        async function getToken() {
            try {
                const token = await AsyncStorage.getItem("api_token")
                return Venue(token, route.params);
            } catch (err) {
                console.log(err)
            }
        }
        getToken()
    }, [])

    const Venue = (tokens, iddtl) => {

        console.log('tokennya', tokens)

        Axios.get(`${api}/api/rs-guarantee/detail/${iddtl}`, {
            headers: {
                'Authorization': 'Bearer ' + tokens,
                'Accept': 'application/json'
            }
        }).then((res) => {
            setIdPengajuan(res.data.data[0].id)
            setData(res.data.data[0])
            setTokens(tokens)
        }).catch((err) => {
            console.log('gagal', err.response.data)
        })

    }

    const showToast = () => {
        ToastAndroid.show("Sukses Mengirim Data!", ToastAndroid.LONG);
    };

    const postConfirm = () => {
        let form = {
            id: idPengajuan,
            code: code
        }

        Axios.post(`${api}/api/rs-guarantee/confirm`, form, {
            headers: {
                'Authorization': 'Bearer ' + Tokens,
                'Accept': 'application/json'
            }
        }).then((res) => {
            navigation.goBack()
            showToast()
            console.log('success', res.data)
            // setdtRs(res.data.data.data)
        }).catch((err) => {
            console.log('gagal', err.response.data)
            Alert.alert('Peringatan',
                err.response.data.message)
        })
    }

    const actualDownload = () => {
        const { dirs } = RNFetchBlob.fs;
        RNFetchBlob.config({
            fileCache: true,
            addAndroidDownloads: {
                useDownloadManager: true,
                notification: true,
                mediaScannable: true,
                title: `lampiran.pdf`,
                path: `${dirs.DownloadDir}/lampiran.pdf`,
            },
        })
            .fetch('GET', data.lampiran, {})
            .then((res) => {
                console.log('The file saved to ', res.path());
                ToastAndroid.show('Lampiran berhasil di simpan', ToastAndroid.LONG);
            })
            .catch((e) => {
                console.log(e)
            });
    }


    return (
        <View style={{ flex: 1 }} >
            <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>

                {/* Pasien Dirawat */}
                <View style={[styles.wrapWhite, { marginTop: 20 }]}>
                    <Text style={styles.title}>Pasien Dirawat</Text>

                    <View style={styles.wraptext}>
                        <Text style={styles.colorTitle}>Nama</Text>
                        <Text>{data.name}</Text>
                    </View>

                    <View style={styles.wraptext}>
                        <Text style={styles.colorTitle}>NIK</Text>
                        <Text>{data.nik}</Text>
                    </View>

                    <View style={styles.wraptext}>
                        <Text style={styles.colorTitle}>Hubungan Keluarga</Text>
                        <Text>{data.type}</Text>
                    </View>

                    <View style={styles.wraptext}>
                        <Text style={styles.colorTitle}>Tanggal Mulai Dirawat</Text>
                        <Text>{moment(data.start_date).format("DD MMMM YYYY")}</Text>
                    </View>

                    <View style={styles.wraptext}>
                        <Text style={styles.colorTitle}>Penyakit</Text>
                        <Text>{data.disease === '' ? '-' : data.disease}</Text>
                    </View>

                    {
                        data.status === "pending" ?
                            <View style={{ backgroundColor: '#EF3A3A', borderRadius: 5, justifyContent: 'center', alignItems: 'center', height: 40, marginVertical: 12 }}>
                                <Text style={{ color: colors.white }}>Belum Terpakai</Text>
                            </View>
                            :
                            <View style={{ backgroundColor: '#27AE60', borderRadius: 5, justifyContent: 'center', alignItems: 'center', height: 40, marginVertical: 12 }}>
                                <Text style={{ color: colors.white }}>Sudah Terpakai</Text>
                            </View>

                    }

                </View>

                {
                    data.status === 'pending' ?
                        <View style={styles.wrapWhite}>
                            <Text style={styles.title}>Verifikasi</Text>
                            <Text style={styles.colorTitle}>Masukan Kode Verifikasi</Text>

                            <OTPInputView
                                style={{ height: 80 }}
                                pinCount={6}
                                keyboardType={'default'}
                                // code={code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                                // onCodeChanged={code => { setCode(code) }}
                                autoFocusOnLoad
                                codeInputFieldStyle={{ borderColor: '#0C63B0', borderWidth: 1, borderRadius: 3, color: 'black' }}
                                codeInputHighlightStyle={{ color: '#0C63B0' }}
                                onCodeFilled={(code) => {
                                    setCode(code)
                                    // console.log(`Code is ${code}, you are good to go!`)
                                }}
                            />

                            <TouchableOpacity style={{ backgroundColor: '#0C63B0', borderRadius: 5, justifyContent: 'center', alignItems: 'center', height: 40, marginVertical: 12 }} onPress={() => { postConfirm() }}>
                                <Text style={{ color: colors.white }}>Confirm</Text>
                            </TouchableOpacity>
                        </View>
                        :
                        false
                }

                {/* Rumah Sakit */}
                <View style={styles.wrapWhite}>
                    <Text style={styles.title}>Rumah Sakit</Text>

                    <View style={styles.wraptext}>
                        <Text style={styles.colorTitle}>Nama Rumah Sakit</Text>
                        <Text>{data.rs_title}</Text>
                    </View>

                    <View style={styles.wraptext}>
                        <Text style={styles.colorTitle}>Alamat</Text>
                        <Text>{data.rs_address}</Text>
                    </View>

                    <View style={styles.wraptext}>
                        <Text style={styles.colorTitle}>Telpon</Text>
                        <Text>{data.rs_phone}</Text>
                    </View>
                </View>

                <TouchableOpacity style={[styles.wrapInput, { marginBottom: 10 }]} onPress={() => { navigation.navigate('SjrsPDF', data.lampiran) }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={require('../../../Assets/drawable-xhdpi/paper.png')} style={{ width: 20, height: 20, marginHorizontal: 10 }} />
                            <Text>Lampiran Surat Jaminan RS</Text>
                        </View>

                        <AntDesign name='right' style={{ marginHorizontal: 10, alignSelf: 'center' }} />
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={styles.wrapInput} onPress={() => { actualDownload() }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <AntDesign name='download' size={20} style={{ width: 20, height: 20, marginHorizontal: 10 }} />
                            <Text>Download Lampiran Surat Jaminan RS</Text>
                        </View>

                        <AntDesign name='right' style={{ marginHorizontal: 10, alignSelf: 'center' }} />
                    </View>
                </TouchableOpacity>
            </ScrollView>
        </View>
    )
}

export default DtlHistorySJRS

const styles = StyleSheet.create({
    wrapWhite: { backgroundColor: colors.white, flex: 1, marginHorizontal: 16, marginVertical: 8, borderRadius: 5, elevation: 1, padding: 10 },
    title: { fontWeight: '600', marginBottom: 7, fontSize: 16 },
    wraptext: { marginVertical: 5 },
    colorTitle: { color: 'grey' },
    wrapInput: {
        backgroundColor: colors.white,
        borderColor: '#C3C3C3',
        borderRadius: 5,
        height: 50,
        marginHorizontal: 16,
        marginTop: 10,
        marginBottom: 50,
        justifyContent: 'center'
    },
})
