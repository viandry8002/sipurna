import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, FlatList, Image, TouchableOpacity } from 'react-native'
import colors from '../../../Style/Colors';
import AntDesign from 'react-native-vector-icons/AntDesign';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import api from '../../../Api';

const index = ({ navigation }) => {
    const [data, setData] = useState([])

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            getToken();
        });
        async function getToken() {
            try {
                const token = await AsyncStorage.getItem("api_token")
                const user_id = await AsyncStorage.getItem("user_id")
                return Venue(token, user_id);
            } catch (err) {
                console.log(err)
            }
        }
        return unsubscribe;
    }, [])

    const Venue = (tokens, user_id) => {

        Axios.get(`${api}/api/rs-guarantee/${user_id}`, {
            headers: {
                'Authorization': 'Bearer ' + tokens,
                'Accept': 'application/json'
            }
        }).then((res) => {
            console.log(res.data.data)
            console.log(tokens)
            setData(res.data.data)
        }).catch((err) => {
            console.log('gagal', err.response.data)
        })

    }

    return (
        <View style={{ flex: 1, backgroundColor: colors.white }} >
            <FlatList
                data={data}
                renderItem={({ item }) => (
                    <TouchableOpacity style={styles.wrapInput} onPress={() => navigation.navigate('DtlHistorySJRS', item.id)}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                                <Image source={require('../../../Assets/drawable-xhdpi/paper.png')} style={{ width: 20, height: 20, marginHorizontal: 10 }} />
                                <Text numberOfLines={1} style={{ flex: 2 }}>{item.name}</Text>
                                {
                                    item.status === 'pending' ?
                                        <View style={{ flex: 1, marginHorizontal: 10, backgroundColor: '#EF3A3A', width: 100, height: 30, alignItems: 'center', borderRadius: 5, justifyContent: 'center' }}>
                                            <Text style={{ color: colors.white }}>{item.status}</Text>
                                        </View>
                                        :
                                        <View style={{ flex: 1, marginHorizontal: 10, backgroundColor: '#27AE60', width: 60, height: 30, alignItems: 'center', borderRadius: 5, justifyContent: 'center' }}>
                                            <Text style={{ color: colors.white }}>{item.status}</Text>
                                        </View>
                                }
                            </View>

                            <AntDesign name='right' style={{ marginHorizontal: 10, alignSelf: 'center' }} />
                        </View>
                    </TouchableOpacity>
                )}
                keyExtractor={item => item.id}
            />
        </View>
    )
}

export default index

const styles = StyleSheet.create({
    wrapInput: {
        borderColor: '#C3C3C3',
        borderWidth: 1,
        borderRadius: 10,
        height: 50,
        marginHorizontal: 16,
        marginVertical: 10,
        justifyContent: 'center'
    },
})
