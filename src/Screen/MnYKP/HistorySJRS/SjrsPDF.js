import React, { useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import PDFView from 'react-native-view-pdf';

const SjrsPDF = ({ route }) => {
    const [dataPdf, setdataPdf] = useState(route.params)

    const resources = {
        file: Platform.OS === 'ios' ? 'downloadedDocument.pdf' : '/sdcard/Download/downloadedDocument.pdf',
        url: dataPdf,
        base64: 'JVBERi0xLjMKJcfs...',
    };

    const resourceType = 'url';

    return (
        <View style={{ flex: 1 }}>
            <PDFView
                fadeInDuration={250.0}
                style={{ flex: 1 }}
                resource={resources[resourceType]}
                resourceType={resourceType}
                onLoad={() => console.log(`PDF rendered from ${resourceType}`)}
                onError={(error) => console.log('Cannot render PDF', error)}
            />
        </View>
    )
}

export default SjrsPDF

const styles = StyleSheet.create({})
