import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  StatusBar,
  SafeAreaView,
  Platform,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Alert,
  ScrollView,
} from 'react-native';
import colors from '../Style/Colors';
import api from '../Api';
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import OneSignal from 'react-native-onesignal';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SpinnerLoad from './MultipleScreen/AnakPerusahaan/SpinnerLoad';

const Masuk = ({navigation}) => {
  const [load, setload] = useState(false);
  const [no_peserta, setNo_peserta] = useState('');
  const [pin, setPin] = useState('');
  const [show, setShow] = useState(true);

  const saveToken = async (token, id, npp, fullname, is_korlap) => {
    try {
      if (token !== null) {
        await AsyncStorage.setItem('api_token', token);
        await AsyncStorage.setItem('user_id', id + '');
        await AsyncStorage.setItem('npp', npp);
        await AsyncStorage.setItem('fullname', fullname);
        await AsyncStorage.setItem('iskorlap', is_korlap);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const isShow = () => {
    setShow(!show);
  };

  const onLoginPress = () => {
    setload(true);
    let data = {
      no_peserta: no_peserta,
      pin: pin,
    };

    Axios.post(`${api}/api/login`, data, {})
      .then((res) => {
        saveToken(
          res.data.data.api_token,
          res.data.data.user_id,
          res.data.data.npp,
          res.data.data.fullname,
          res.data.data.is_korlap,
        );

        // OneSignal.setAppId("69ecf374-9920-494a-9e1a-a7c04bfdb441"); //akun vian
        OneSignal.setAppId('c2e8bee4-b807-48b9-985c-ea1d496dffee'); //akun herry prahu
        OneSignal.setLogLevel(6, 0);
        OneSignal.setExternalUserId(res.data.data.user_id + '', (results) => {
          // The results will contain push and email success statuses
          console.log(
            'Results of setting external user id ' + res.data.data.user_id,
          );
          console.log(results);
        });

        navigation.reset({
          index: 0,
          routes: [{name: 'Home'}],
        });
        setload(false);
      })
      .catch((err) => {
        console.log(err.response.data);
        if (err.response.data.message === 'Belum di verifikasi Admin') {
          Alert.alert('Peringatan', err.response.data.message);
        } else {
          Alert.alert('Peringatan', 'No Peserta atau pin salah');
        }
        setload(false);
      });
  };

  return (
    <View style={styles.container}>
      <Image
        source={require('../Assets/drawable-xhdpi/Group253.png')}
        style={{
          flex: 1,
          width: '100%',
          height: '100%',
          position: 'absolute',
          marginTop: 100,
        }}
      />
      <SpinnerLoad loads={load} />
      <View style={{backgroundColor: 'rgba(255,255,255,0.8)', flex: 1}}>
        <StatusBar backgroundColor={'white'} barStyle="dark-content" />

        {/* <ScrollView></ScrollView> */}
        <ScrollView
          style={{flex: 1, flexDirection: 'column', paddingHorizontal: 20}}
          contentContainerStyle={{
            flexGrow: 1,
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          {/* <Image source={require('../Assets/drawable-xhdpi/LogoIkapurna.png')} style={{ width: 80, height: 90, marginTop: 70 }} /> */}
          <Image
            source={require('../Assets/MainLogo.png')}
            style={{width: 150, height: 170}}
          />
          <View style={{paddingHorizontal: 50}}>
            <Text style={[styles.font, {textAlign: 'center'}]}>
              Sipurna adalah Sistem Informasi IKAPURNA untuk seluruh ANGGOTA
            </Text>
          </View>
          {/* box login */}
          <View
            style={{
              backgroundColor: '#BFCDE3',
              width: '100%',
              height: 250,
              borderRadius: 5,
              paddingVertical: 20,
              paddingHorizontal: 10,
            }}>
            <Text style={styles.font}>NPP</Text>
            <View
              style={{
                backgroundColor: 'white',
                width: '100%',
                height: 45,
                borderRadius: 5,
                paddingHorizontal: 5,
                marginBottom: 10,
                justifyContent: 'center',
              }}>
              <TextInput
                value={no_peserta}
                placeholder=""
                onChangeText={(no_peserta) => setNo_peserta(no_peserta)}
              />
            </View>
            <Text style={styles.font}>PIN</Text>
            <View
              style={{
                backgroundColor: 'white',
                width: '100%',
                height: 45,
                borderRadius: 5,
                paddingHorizontal: 5,
                flexDirection: 'row',
              }}>
              <TextInput
                secureTextEntry={show}
                value={pin}
                placeholder=""
                onChangeText={(pin) => setPin(pin)}
                style={{width: 280}}
              />
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  flex: 1,
                }}>
                <AntDesign name="eye" size={20} onPress={() => isShow()} />
              </View>
            </View>
            <View style={{alignItems: 'center', marginTop: 15}}>
              <TouchableOpacity onPress={() => onLoginPress()}>
                <Image
                  source={require('../Assets/drawable-xhdpi/Masuk.png')}
                  style={{width: 180, height: 70}}
                />
              </TouchableOpacity>
            </View>
          </View>
          {/* box login */}
          {/* cs */}
          <View style={{alignItems: 'center', marginBottom: 20}}>
            <Text style={styles.font}>Lupa PIN?</Text>
            <TouchableOpacity
              onPress={() => navigation.navigate('HubungiKami')}>
              <Text style={[styles.font, {color: 'blue'}]}>Hubungi Kami</Text>
            </TouchableOpacity>
          </View>
          {/* cs */}
          {/* Daftar */}
          {Platform.OS === 'ios' ? (
            <View style={{flexDirection: 'row'}}>
              <Text style={[styles.font, {marginTop: 20}]}>
                Anda belum daftar ?
              </Text>
              <TouchableOpacity onPress={() => navigation.navigate('Daftar')}>
                <Image
                  source={require('../Assets/drawable-xhdpi/Daftar_btn.png')}
                  style={{width: 180, height: 70}}
                />
              </TouchableOpacity>
            </View>
          ) : null}
          {/* Daftar */}
        </ScrollView>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  font: {
    fontSize: 16,
  },
});

export default Masuk;
