import React, { useState, useEffect } from 'react'
import { View, Text, Image, StatusBar, SafeAreaView, StyleSheet, TextInput, TouchableOpacity, Alert, ScrollView, ToastAndroid } from 'react-native'
import colors from '../Style/Colors';
import api from '../Api';
import Axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import OneSignal from 'react-native-onesignal';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SpinnerLoad from './MultipleScreen/AnakPerusahaan/SpinnerLoad';

const Daftar = ({ navigation }) => {
    const [load, setload] = useState(false)
    const [no_peserta, setNo_peserta] = useState('')
    const [pin, setPin] = useState('')
    const [nama, setNama] = useState('')
    const [email, setEmail] = useState('')
    const [show, setShow] = useState(true)

    const isShow = () => {
        setShow(!show)
    }

    const onLoginPress = () => {
        setload(true)
        // setbtnTouch(true)
        let data = {
            npp: no_peserta,
            fullname: nama,
            pin: pin,
            email: email
        }

        console.log('hasilnya', data)
        if (no_peserta === '' || nama === '' || pin === '' || email === '') {
            setload(false)
            Alert.alert(
                "Peringatan",
                "Form input tidak oleh kosong"
            );
        } else {
            Axios.post(`${api}/api/register`, data, {
            }).then((res) => {
                // console.log(res.data)
                setload(false)

                Alert.alert(
                    "Pemberitahuan",
                    "Sukses mendaftar, tunggu verifikasi admin agar bisa melakukan login",
                    [,
                        { text: "OK", onPress: () => navigation.navigate('Masuk') }
                    ]
                );

            }).catch((err) => {
                console.log('err', err)
                setload(false)
            })
        }
    }

    return (
        <View style={styles.container}>
            <Image source={require('../Assets/drawable-xhdpi/Group253.png')} style={{ flex: 1, width: '100%', height: '100%', position: 'absolute', marginTop: 100 }} />
            <SpinnerLoad loads={load} />
            <View style={{ backgroundColor: 'rgba(255,255,255,0.8)', flex: 1 }}>
                <StatusBar backgroundColor={'white'} barStyle="dark-content" />

                {/* <ScrollView></ScrollView> */}
                <ScrollView style={{ flex: 1, flexDirection: 'column', paddingHorizontal: 20 }} contentContainerStyle={{ flexGrow: 1, justifyContent: 'space-between', alignItems: 'center' }}>
                    {/* <Image source={require('../Assets/drawable-xhdpi/LogoIkapurna.png')} style={{ width: 80, height: 90, marginTop: 70 }} /> */}
                    <Image source={require('../Assets/MainLogo.png')} style={{ width: 150, height: 170 }} />
                    <View style={{ paddingHorizontal: 50 }}>
                        <Text style={[styles.font, { textAlign: 'center' }]}>Silahkan daftar di sini
                        </Text>
                    </View>
                    {/* box daftar */}
                    <View style={{ backgroundColor: '#BFCDE3', width: '100%', height: 400, borderRadius: 5, paddingVertical: 20, paddingHorizontal: 10 }}>

                        <Text style={styles.font}>NPP</Text>
                        <View style={{ backgroundColor: 'white', width: '100%', height: 45, borderRadius: 5, paddingHorizontal: 5, marginBottom: 10 }}>
                            <TextInput value={no_peserta} placeholder='' onChangeText={(no_peserta) => setNo_peserta(no_peserta)} />
                        </View>

                        <Text style={styles.font}>Nama Lengkap</Text>
                        <View style={{ backgroundColor: 'white', width: '100%', height: 45, borderRadius: 5, paddingHorizontal: 5, marginBottom: 10 }}>
                            <TextInput value={nama} placeholder='' onChangeText={(nama) => setNama(nama)} />
                        </View>

                        <Text style={styles.font}>Email</Text>
                        <View style={{ backgroundColor: 'white', width: '100%', height: 45, borderRadius: 5, paddingHorizontal: 5, marginBottom: 10 }}>
                            <TextInput value={email} placeholder='' onChangeText={(email) => setEmail(email)} />
                        </View>

                        <Text style={styles.font}>PIN</Text>
                        <View style={{ backgroundColor: 'white', width: '100%', height: 45, borderRadius: 5, paddingHorizontal: 5, flexDirection: 'row' }}>
                            <TextInput secureTextEntry={show} value={pin} placeholder='' onChangeText={(pin) => setPin(pin)}
                                style={{ width: 290 }} />
                            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                                <AntDesign name='eye' size={20} onPress={() => isShow()} />
                            </View>
                        </View>


                        <View style={{ alignItems: 'center', marginTop: 15 }} >
                            <TouchableOpacity onPress={() => onLoginPress()} >
                                <Image source={require('../Assets/drawable-xhdpi/Daftar_btn.png')} style={{ width: 180, height: 70 }} />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {/* box login */}
                    {/* Daftar */}
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={[styles.font, { marginTop: 20 }]}>Sudah punya akun ?</Text>
                        <TouchableOpacity onPress={() => navigation.navigate('Masuk')}>
                            <Image source={require('../Assets/drawable-xhdpi/Masuk.png')} style={{ width: 180, height: 70 }} />
                        </TouchableOpacity>
                    </View>
                    {/* Daftar */}

                </ScrollView>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    font: {
        fontSize: 16
    }
})

export default Daftar

