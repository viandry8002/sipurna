import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { FlatList, Image, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View, ActivityIndicator } from 'react-native';
import { SliderBox } from "react-native-image-slider-box";
import api from '../Api';
import colors from '../Style/Colors';
import HTML from "react-native-render-html";

const Home = ({navigation}) => {
      const [DImages, setDImages] = useState([])
      const [data, setData] = useState()
      const [loading, setLoading] = useState(true);
      const [btnkorlap, setBtnkorlap] = useState('');

      useEffect(() => {

        async function getToken() {
            try{
                const token = await AsyncStorage.getItem("api_token")
                const id = await AsyncStorage.getItem("user_id")
                // const iskorlap = await AsyncStorage.getItem("iskorlap")
                return getVenue(token,id)
            }catch(err){
                console.log(err)
            }
        }
        getToken()
    },[])

    const getVenue = (token,id) =>{
      console.log(btnkorlap)
      setLoading(true);
      setTimeout(() => {
        
        Axios.get(`${api}/api/user/${id}`,{
          headers : {
              'Authorization' : 'Bearer ' + token,
              'Accept' : 'application/json'
          }
      }).then((res) =>{
          setBtnkorlap(res.data.data.is_korlap)
          console.log('hasil', res.data.data.is_korlap)
          console.log(btnkorlap)
      }).catch((err) =>{
          console.log(err.response.data)
          if(err.response.data === 'Unauthorized.'){
              navigation.navigate('OnKick')
            }
      })

        Axios.get(`${api}/api/slider`,{
            headers : {
                'Authorization' : 'Bearer ' + token,
                'Accept' : 'application/json'
            }
        }).then((res) =>{
            setDImages(res.data.data)
            setLoading(false)
      }).catch((err) =>{
        setLoading(false);
        console.log(err.response.data)
        })

        Axios.get(`${api}/api/activity_limit`,{
          headers : {
              'Authorization' : 'Bearer ' + token,
              'Accept' : 'application/json'
          }
        }).then((res) =>{
            setData(res.data.data)
            setLoading(false);
        }).catch((err) =>{
          setLoading(false);
          console.log(err.response.data)
        })

      }, 5000);

    }

    const images = 
        DImages.map((datas, index)=>{
          if (datas.tampilkan === 'YES') {
            return [{uri:  'https://backend.sipurna.com/' + datas.file}]
          }
        })
      
      const DtlInfoKegiatan = (route) => {
        navigation.navigate('DtlBerita',route)
      }

    const renderItem = ({ item}) => (
        <View style={styles.item}>
        <View style={{flexDirection:'row'}}>
        <Image source={item.file === '' || item.file === null ? require('../Assets/drawable-xhdpi/img-notfound.png')
                : {uri: item.file}} style={{width: 140,height: 140,borderRadius:10, marginHorizontal:10,marginTop:10}} style={{width: 140,height: 140,borderRadius:10, marginHorizontal:10,marginTop:10}} />
        <View style={styles.containerflatList}>
        <Text style={styles.title1}>{item.title}</Text>
        <Text style={[styles.font,{paddingBottom:10}]}>{item.type.toUpperCase()}</Text>
        <HTML source={{ html: item.description === '' ? '-' : item.description  }} renderers={{
          p: (_, children) => <Text numberOfLines={1} key={children} >{children}</Text>,
        }} />
        <TouchableOpacity style={{alignItems:'center',marginVertical:10}} onPress={() => DtlInfoKegiatan(item)}>
          <Text style={styles.detail}>Detail</Text>
        </TouchableOpacity>
        </View>
        </View>
        <View
          style={{
            borderBottomColor: '#919191',
            borderBottomWidth: 1,
            marginHorizontal:7,
            marginTop:10
          }}
        />
        </View>
    );

    return (
        <View style={styles.container}>
         <Image source={require('../Assets/drawable-xhdpi/Group253.png')} style={{flex:1,width:'100%',height:'100%',position:'absolute',marginTop:100}}/>
         <View style={{backgroundColor:'rgba(255,255,255,0.8)',flex:1}}>
         <StatusBar translucent backgroundColor='transparent' />
         {
            loading ? (
              <View style={{margin:'50%'}}>
              <ActivityIndicator
              visible={loading}
              textContent={'Loading...'}
              style={{alignSelf:'center'}}
              size="large" 
              color="#2C5BA4"
              />
              </View>
          )
          :
          (

         <ScrollView>
         <View style={{flex:1,flexDirection:'column', alignItems: 'center'}}>
             <View style={{margin:10,height:180}}>
            <SliderBox 
            paginationBoxVerticalPadding={10}
            autoplay
            circleLoop
            images={images}
            dotColor={colors.blue}
            inactiveDotColor={colors.white}
            imageLoadingColor={colors.blue}
            ImageComponentStyle={{width: '92%',borderRadius: 5,height:150}}
              />
            </View>
             {/* box Menu */}
             <View >
             <View style={{flexDirection:'row'}}>
             <TouchableOpacity onPress={() => navigation.navigate('LmnBTN')}>
             <Image source={require('../Assets/drawable-xhdpi/btnmenu4.png')} style={styles.btnImageMenu}/>
             </TouchableOpacity>
             <TouchableOpacity onPress={() => navigation.navigate('LmnIkapurna')}>
             <Image source={require('../Assets/drawable-xhdpi/btnmenu1.png')} style={styles.btnImageMenu}/>
             </TouchableOpacity>
             </View>
             
             <View style={{flexDirection:'row'}}>
             <TouchableOpacity onPress={() => navigation.navigate('LmnYKP')}>
             <Image source={require('../Assets/drawable-xhdpi/btnmenu2.png')} style={styles.btnImageMenu}/>
             </TouchableOpacity>
             <TouchableOpacity onPress={() => navigation.navigate('LmnDanaPensiun')}>
             <Image source={require('../Assets/drawable-xhdpi/btnmenu3.png')} style={styles.btnImageMenu}/>
             </TouchableOpacity>
             </View>
             </View>
             {/* box Menu */}
            <Text style={[styles.font,{alignItems:'center',marginBottom:20}]}>Tetap sehat dan bahagia di usia emas</Text>
            
            {/* artikel */}
            <View>
            <Text style={[styles.font,{paddingLeft:20}]}>Berita Terbaru</Text>
            <View style={{height:720,alignContent:'center'}}>
            <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
            />
            </View >
            <TouchableOpacity onPress={() => navigation.navigate('Berita') } style={{alignItems:'center',marginBottom:40,marginTop:20}}>
            <Text style={{color: '#669F16'}}>Selengkapnya</Text>
            </TouchableOpacity>
            </View>
            {/* artikel */}

         </View>
         </ScrollView>
          )
         }
         {
           btnkorlap === 'true' ?
           (
            <TouchableOpacity onPress={() => navigation.navigate('FormKorlap')} 
            style={{
            position:'absolute',
            alignSelf:'flex-end',
            // backgroundColor: 'red',
            // justifyContent: 'flex-end',
            // width:70
            marginTop:'125%',
            }}>
            <Image source={require('../Assets/drawable-xhdpi/dead.png')} style={{height:73,width:73}}/>
            </TouchableOpacity>
           )
           :
           (
             <View></View>
           )
         }
         
         </View>

     </View>
    )
}

export default Home


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    font:{
        fontSize:16,
        color:'#2C5BA4'
    },
    btnImageMenu:{
        width:156,
        height:156,
        margin:10
    },
    item: {
        padding: 10,
        // marginVertical: 3,
        flexDirection : 'column',
        // flex: 1,
        // marginRight:50
      },
      title1: {
        fontSize: 16,
        paddingTop:10
        // fontFamily:'Montserrat'
      },
      containerflatList : {
        // flexDirection : 'column',
        // flex: 1,
        // marginVertical:5,
        padding:5,
        marginBottom:10,
        marginRight:170
      },
      detail: {
        fontSize: 12,
        color: '#669F16'
      },
})
