import React, { useState, useEffect } from 'react';
import { ScrollView, StyleSheet, Text, View, ToastAndroid, Alert } from 'react-native';
import DatePicker from 'react-native-datepicker';
import { Button, Input } from 'react-native-elements';
import DocumentPicker from 'react-native-document-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';

const FormKorlap = ({navigation}) => {
    const [date, setDate] = useState(new Date())
    const [fullname, setFullname] = useState('')
    const [npp, setNpp] = useState('')
    const [form, setForm] = useState({
        judul : '',
        tanggal : '',
        tempat : '',
        foto : ''
    })

    const onInputChange = (value,input) => {
        setForm({
            ...form,
            [input] : value,
        })
    }

    const SendForm = () => {
        if (form.tanggal !== '') {
            Axios.post(`https://sipurna.com/aplikasi/api/ykp/korlap`,form,{
            }).then((res) => {
                console.log('success',res.data)
                showToast()
                setForm('')
            }).catch((err) => {
                console.log('gagal',err)
            })
        }else{
            Alert.alert('Peringatan',
            'Form input tidak boleh kosong',
                [{
                    text : 'Ya',
                    onPress : () => console.log('Tidak')
                }])
        }
    }

    useEffect(() => {

        async function getToken() {
            try{
                const token = await AsyncStorage.getItem("api_token")
                const npp = await AsyncStorage.getItem("npp")
                const fullname = await AsyncStorage.getItem("fullname")
                setNpp(npp)
                setFullname(fullname)
            }catch(err){
                console.log(err)
            }
        }
        getToken()
    },[])

    const OpenDocumentFile = async() => {
            try {
        const res = await DocumentPicker.pick({
            type: [DocumentPicker.types.allFiles],
        });
        // console.log(res.uri);
        onInputChange(res.uri,'foto')
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
            } else {
                throw err;
            }
        }
    }

    const showToast = () => {
        ToastAndroid.show("Sukses Mengirim Data!", ToastAndroid.LONG);
      };

    return (
        <ScrollView style={[styles.wrap,{backgroundColor:'white'}]}>
            <View style={{margin:20}}>

                <View >
                <Text>Judul Peristiwa</Text>
                <View style={styles.wrapInput}>
                <Input
                inputContainerStyle={{borderBottomWidth:0}}
                style={{padding:10}}
                value={form.judul} 
                onChangeText={value => onInputChange(value,'judul')} 
                />
                </View>
                </View>

                <View >
                <Text>Tanggal Peristiwa</Text>
                <View style={styles.wrapInput}>
                <DatePicker
                    style={{width:'97%',marginTop:5}}
                    // date={date}
                    date={form.tanggal}
                    mode="date"
                    placeholder="select date"
                    format="YYYY-MM-DD"
                    minDate="2020-01-01"
                    maxDate="2050-01-01"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    // iconComponent={<MaterialIcons name={'calendar-today'} size={20} color={colors.grey}/>}
                    
                    customStyles={{
                    dateInput: {
                        borderWidth:0,
                    },
                    // ... You can check the source to find the other keys.
                    }}
                    // onDateChange={(date) => setDate(date)}
                    onDateChange={value => onInputChange(value,'tanggal')}
                    
                />
                </View>
                </View>

                <View >
                <Text>Uraian Peristiwa</Text>
                <View style={styles.wrapInput}>
                <Input
                inputContainerStyle={{borderBottomWidth:0}}
                style={{padding:10}}
                value={form.tempat} 
                onChangeText={value => onInputChange(value,'tempat')} 
                />
                </View>
                </View>

                <View>
                <Text>Foto Peristiwa</Text>
                <Button
                title="Upload File"
                buttonStyle={{marginTop:10,borderRadius:10,backgroundColor:'#E3E3E3',width:'40%', height:50}}
                onPress={() => OpenDocumentFile()}
                />
                </View>

                <Button
                title="Kirim"
                buttonStyle={{marginTop:20,borderRadius:10,backgroundColor:'#2C5BA4', height:47}}
                // onPress={() =>  
                //     Alert.alert('Peringatan',
                //     'Maaf saat ini data belum bisa di kirim',
                //     [{
                //         text : 'Ya',
                //         onPress : () => console.log('Tidak')
                //     }])}
                onPress={() => SendForm()}
                />
            </View>
        </ScrollView>
    )
}

export default FormKorlap

const styles = StyleSheet.create({
    wrap : {
        flex:1
    },
    wrapInput:{
        borderColor:'#C3C3C3',
        borderWidth:1,
        borderRadius:10,
        height:47,
        marginVertical:10
    },
})