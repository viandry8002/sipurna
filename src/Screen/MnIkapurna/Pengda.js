import AsyncStorage from '@react-native-async-storage/async-storage';
import { Picker } from '@react-native-picker/picker';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View,Modal,Platform } from 'react-native';
import api from '../../Api';
import AntDesign from 'react-native-vector-icons/AntDesign';

const Pengda = ({navigation}) => {

  const [selectedValue, setSelectedValue] = useState("");

  const [address, setAddress] = useState("");
  const [position, setPosition] = useState("");

  const [dataPengda, setDataPengda] = useState([]);
  const [Dkota, setDkota] = useState([])
  const [Tokens, setTokens] = useState("")
  const [loading, setLoading] = useState(false);

  const [modalVisible, setModalVisible] = useState(false);

    useEffect(() => {
      async function getToken() {
          try{
              const token = await AsyncStorage.getItem("api_token")
              return getKota(token)
          }catch(err){
              console.log(err)
          }
      }
      getToken()
  },[])

  const getKota = (token) =>{
      Axios.get(`${api}/api/pdaerah/city`,{
          headers : {
              'Authorization' : 'Bearer ' + (Tokens === '' ? token : Tokens),
              'Accept' : 'application/json'
          }
      }).then((res) =>{
          setDkota(res.data.data)
          console.log('azzzzzz',res.data.data)
          Tokens === '' ?  setTokens(token) : setTokens(Tokens)

      }).catch((err) =>{
        console.log(err.response.data)
        if(err.response.data === 'Unauthorized.'){
            navigation.navigate('OnKick')
          }
      })
  }

  const getKotaLst = (selected) =>{
    setLoading(true);
        setTimeout(() => {

        const paramdt = {
          city : selected,
      }
        Axios.post(`${api}/api/pdaerah`,paramdt,{
            headers : {
                'Authorization' : 'Bearer ' + Tokens,
                'Accept' : 'application/json'
            }
        }).then((res) =>{
            setDataPengda(res.data.data)
            setPosition(res.data.data[0].description)
            setAddress(res.data.data[0].address)
            setLoading(false);
            setSelectedValue(selected)

        }).catch((err) =>{
          setLoading(false);
          console.log(err.response.data)
          if(err.response.data === 'Unauthorized.'){
              navigation.navigate('OnKick')
            }
        })

      }, 100);
}

    const renderItem = ({item}) => (
        <View style={styles.item}>
        <Image source={item.file === null ? require('../../Assets/drawable-xhdpi/img-notfound.png') : {uri: item.file}} style={{width: 140,height: 140,borderRadius:10, marginHorizontal:10,marginTop:10}} />
        <View style={styles.containerflatList}>
        <Text style={styles.title1}>{item.name}</Text>
        <Text style={styles.title2}>{item.position}</Text>
        <Text style={styles.title3}>Handphone : </Text>
        <Text style={styles.title3}>{item.phone}</Text>
        </View>
        </View>
    );

    return (
        <SafeAreaView style={styles.container}>
        <View style={{flex:1,flexDirection:'column'}}>
        <View style={styles.item1}>

        {Platform.OS === 'ios' ? (
        <View style={{borderWidth:1,borderColor:'#C3C3C3',borderRadius:10,height:50,justifyContent:'center',paddingHorizontal:16}}>
        <TouchableOpacity onPress={() => setModalVisible(true)} style={{flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
          <Text>Pilih Kota</Text>
          <AntDesign name='caretdown' size={10} />
        </TouchableOpacity>
        </View>):
        (<View style={{borderWidth:1,borderColor:'#C3C3C3',borderRadius:10}}>
        <Picker
        selectedValue={selectedValue}
        style={{ height: 50, width: '100%'}}
        onValueChange={(itemValue, itemIndex) => {getKotaLst(itemValue)}}>
        <Picker.Item label="Pilih Kota" value="" />
        {
          Dkota.map((datak, index)=>{
          return <Picker.Item key={index} label={datak.city} value={datak.city} />
          })
        }
        </Picker>
        </View>)}

        <View style={{alignItems:'center',marginBottom:10}}>
        <Text style={{fontSize:16}}>
            PENGDA {position}
        </Text>
        <Text style={{textAlign:'center'}}>
            {address}
        </Text>
        </View>
       
        </View>
       
        {loading ? (
          <ActivityIndicator
            visible={loading}
            textContent={'Loading...'}
            style={{alignSelf:'center',marginTop:'60%',position:'absolute'}}
            size="large" 
            color="#2C5BA4"
          />
        ) : (
          <>
        <FlatList
            // data={DATA}
            data={dataPengda}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
          />
          </>
        )}

        </View>

          {/* //modal city */}
        <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
      >
        <View style={{
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // marginTop: 22
  }}>
          <View style={{
    // margin: 20,
    backgroundColor: "white",
    borderRadius: 8,
    padding: 16,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    // width:250,
    height:200
  }}>
            <FlatList
      data={Dkota}
      renderItem={({item}) => (
      <TouchableOpacity onPress={() => (setModalVisible(!modalVisible),getKotaLst(item.city))} style={{width:200}}>
        <Text>{item.city}</Text>
        <View
  style={{
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    marginVertical:16
  }}
/>
      </TouchableOpacity>
      )}
    />
          </View>
        </View>
      </Modal>
    </SafeAreaView>
    )
}

export default Pengda

const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
      item1: {
        backgroundColor: 'white',
        paddingVertical: 20,
        paddingHorizontal:15,
        flexDirection : 'column',
        width:'100%',
        height: 170,
        justifyContent: 'space-between',
        marginBottom:10,
      },

      item: {
        backgroundColor: 'white',
        padding: 10,
        marginVertical: 3,
        flexDirection : 'row',
        flex: 1
      },
      title1: {
        fontSize: 18,
        marginTop:10,
        marginBottom:5
      },
      title2: {
        color:'#669F16',
        marginBottom:15
      },
      title3: {
        color:'#919191',
      },
      containerflatList : {
        flexDirection : 'column',
        flex: 1,
        margin:5
      },
})
