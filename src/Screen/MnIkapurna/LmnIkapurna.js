import React from 'react'
import { View, Text, Image, StatusBar, SafeAreaView,StyleSheet,TextInput,TouchableOpacity } from 'react-native'
import colors from '../../Style/Colors';

const LmnIkapura = ({navigation}) => {
    return (
         <SafeAreaView style={styles.container}>
         <Image source={require('../../Assets/drawable-xhdpi/Group253.png')} style={{flex:1,width:'100%',height:'100%',position:'absolute',marginTop:100}}/>
         <View style={{backgroundColor:'rgba(255,255,255,0.8)',flex:1}}>
         <StatusBar translucent backgroundColor='transparent' barStyle="light-content" />
         <View style={{flex:1,flexDirection:'column',justifyContent: 'space-between', alignItems: 'center',paddingHorizontal:10}}>
             {/* box Menu */}
             <View style={{width:'100%',height:300,marginTop:30}}>
             <TouchableOpacity onPress={() => navigation.navigate('Tentang', { name: 'ikapurna',title: 'Ikapurna Bank BTN' })}>
            <Image source={require('../../Assets/drawable-xhdpi/ListMenuIkapurna/Group302.png')} style={styles.btnMenu}/>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => navigation.navigate('PengurusPusat')}>
            <Image source={require('../../Assets/drawable-xhdpi/ListMenuIkapurna/Group116.png')} style={styles.btnMenu}/>
            </TouchableOpacity>
             
             <TouchableOpacity onPress={() => navigation.navigate('Pengda')}>
             <Image source={require('../../Assets/drawable-xhdpi/ListMenuIkapurna/Group117.png')} style={styles.btnMenu}/>
             </TouchableOpacity>
             
             <TouchableOpacity onPress={() => navigation.navigate('InfoKegiatan')}>
             <Image source={require('../../Assets/drawable-xhdpi/ListMenuIkapurna/Group118.png')} style={styles.btnMenu}/>
             </TouchableOpacity>
             
             </View>
             {/* box Menu */}
             
         </View>
          </View> 
      </SafeAreaView>
    )
}

export default LmnIkapura


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    btnMenu:{
        width:'100%',
        height:70
    }
})
