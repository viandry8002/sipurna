import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import React, { useEffect, useState } from 'react';
import { FlatList, Image, SafeAreaView, ScrollView, StyleSheet, Text, View, ActivityIndicator } from 'react-native';
import api from '../../Api';
import { Button } from 'react-native-elements';
import HTML from "react-native-render-html";

const Profile = (fto,name,position) => {
    return (
    <View style={{padding:10,flex:1,marginTop:20,marginBottom:35,alignItems:'center'}}>
        <Image 
        source={fto}
        style={{
            width: 144,
            height: 162,
            marginHorizontal:10
        }}/>
        <View style={styles.wrapProfile}>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.position}>{position}</Text>
        </View>
        </View>
    )
}

const PengurusPusat = ({navigation}) => {
    const [data, setData] = useState([])
    const [dataKetua,setDataKetua] = useState([])
    const [arrayholder, setArrayholder] = useState('')
    const [Tokens, setTokens] = useState('')
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        async function getToken() {
             try{
                 const token = await AsyncStorage.getItem("api_token")
                 return getTujuan(token)
             }catch(err){
                 console.log(err)
             }
        }
        getToken()
    },[])

    const getTujuan = (token) =>{
      setLoading(true);
        setTimeout(() => {
          
      Axios.get(`${api}/api/configuration/ikapurna`,{
        headers : {
            'Authorization' : 'Bearer ' + (Tokens === '' ? token : Tokens),
            'Accept' : 'application/json'
        }
    }).then((res) =>{
        setArrayholder('DATA')
        // console.log(res.data.data)
        setData(res.data.data)
        Tokens === '' ?  setTokens(token) : setTokens(Tokens)
        setLoading(false);

    }).catch((err) =>{
      console.log(err.response.data)
            if(err.response.data === 'Unauthorized.'){
                navigation.navigate('OnKick')
              }
    })

      }, 100);
    }

    const getPengurus = () =>{
        setLoading(true);
            setTimeout(() => {
    
                Axios.get(`${api}/api/ppusat/ikapurna`,{
              headers : {
                  'Authorization' : 'Bearer ' + (Tokens === '' ? token : Tokens),
                  'Accept' : 'application/json'
              }
          }).then((res) =>{
              setArrayholder('DATA1')
              setData(res.data.data)
              setDataKetua(res.data.data[0])
              Tokens === '' ?  setTokens(token) : setTokens(Tokens) 
              setLoading(false);
    
          }).catch((err) =>{
            setLoading(false);
            console.log(err.response.data)
            if(err.response.data === 'Unauthorized.'){
                navigation.navigate('OnKick')
              }
          })
    
          }, 100);
        }
    
        const getPengawas = () =>{
          setLoading(true);
            setTimeout(() => {
              
          Axios.get(`${api}/api/ppusat/badan_pengawas_ikapurna`,{
            headers : {
                'Authorization' : 'Bearer ' + Tokens,
                'Accept' : 'application/json'
            }
        }).then((res) =>{
            setArrayholder('DATA2')
            setData(res.data.data)
            setLoading(false);
    
        }).catch((err) =>{
          console.log(err.response.data)
                if(err.response.data === 'Unauthorized.'){
                    navigation.navigate('OnKick')
                  }
        })
    
          }, 100);
        }

        

    // const getVenue = (token) =>{
    //     Axios.get(`${api}/api/ppusat/ikapurna`,{
    //         headers : {
    //             'Authorization' : 'Bearer ' + token,
    //             'Accept' : 'application/json'
    //         }
    //     }).then((res) =>{
    //         setData(res.data.data)
    //         setDataKetua(res.data.data[0])
    //         // console.log(res.data.data)
    //     }).catch((err) =>{
    //         console.log(err.response.data)
    //         if(err.response.data === 'Unauthorized.'){
    //             navigation.navigate('OnKick')
    //           }
    //     })
    // }

    const renderItem = ({ item, index }) => {
        if (index > 0) {
        return <View style={{
        flex: 1,
        margin: 5,
        minWidth: 170,
        maxWidth: 223,
        height: 260,
        maxHeight:260
        // backgroundColor: '#CCC',
        }}>
            <View style={{alignItems:'center'}}>
            <Image 
            source={item.file === null ? require('../../Assets/drawable-xhdpi/img-notfound.png') : {uri: item.file}}
            style={{
                width: 144,
                height: 162,
                marginHorizontal:10
            }}/>
            <View style={styles.wrapProfile}>
            <Text style={styles.name}>{item.name}</Text>
            <Text style={styles.position}>{item.position}</Text>
            </View>
            </View>
         </View>
        }
    }

    const renderItem2 = ({item}) => (
        <View style={styles.item}>
        <Image source={item.file === null ? require('../../Assets/drawable-xhdpi/img-notfound.png') : {uri: item.file}} style={{width: 140,height: 160,borderRadius:10, marginHorizontal:10,marginTop:10}} />
          <View style={styles.containerflatList}>
          <Text style={styles.title1}>{item.name}</Text>
          <Text style={styles.title2}>{item.position}</Text>
          <Text style={styles.title3}>Handphone : </Text>
          <Text style={styles.title3}>{item.phone}</Text>
          </View>
        </View>
    );

      var typ = arrayholder == 'DATA' ? "solid" : "outline"
      var ttl = arrayholder == 'DATA' ? {color:'white'} : {color:'black'}
      var btn = arrayholder == 'DATA' ? {backgroundColor:'#2C5BA4',marginHorizontal:5} : {marginHorizontal:5}
  
      var typ1 = arrayholder == 'DATA1' ? "solid" : "outline"
      var ttl1 = arrayholder == 'DATA1' ? {color:'white'} : {color:'black'}
      var btn1 = arrayholder == 'DATA1' ? {backgroundColor:'#2C5BA4',marginHorizontal:5} : {marginHorizontal:5}

      var typ2 = arrayholder == 'DATA2' ? "solid" : "outline"
      var ttl2 = arrayholder == 'DATA2' ? {color:'white'} : {color:'black'}
      var btn2 = arrayholder == 'DATA2' ? {backgroundColor:'#2C5BA4',marginHorizontal:5} : {marginHorizontal:5}

    return (
        <View style={{flex:1}}>
        <View style={{flexDirection:'column',width: '100%',height:'100%',marginBottom:10}}>

        <View style={styles.item1}>

        <ScrollView horizontal={true}>
        <Button title="Maksud dan Tujuan"
        type={typ}
        titleStyle={ttl}
        buttonStyle={btn} 
        onPress={() => {getTujuan()}}/>
        
        <Button title="Pengurus"
        type={typ1}
        titleStyle={ttl1}
        buttonStyle={btn1} 
        onPress={() => {getPengurus()}}/>

        <Button title="Pengawas" 
        type={typ2} 
        titleStyle={ttl2} 
        buttonStyle={btn2} 
        onPress={() => {getPengawas()}}/>
        </ScrollView>

        </View>

        {loading ? (
          <ActivityIndicator
            visible={loading}
            textContent={'Loading...'}
            style={{alignSelf:'center',marginTop:'50%',position:'absolute'}}
            size="large" 
            color="#2C5BA4"
          />
        ) : (
          
          
            arrayholder === 'DATA1' ?
            <ScrollView>
                <View style={{flexDirection:'column',alignItems:'center',backgroundColor: 'white'}}>
                {/* title */}
                <View style={{marginVertical:15,alignItems:'center'}}>
                <Text style={styles.name}>IKAPURNA Bank BTN</Text>
                <Text style={styles.titleBlue}>Badan Pengurus Pusat</Text>
                <Text style={styles.titleBlue}>2019 - 2024</Text>
                </View>
                {/* title */}

                {/* Profile 1 */}
                {Profile(
                    dataKetua.file === null ? require('../../Assets/drawable-xhdpi/img-notfound.png') : {uri: dataKetua.file},
                    dataKetua.name,
                    dataKetua.position)}
                {/* Profile 1 */}

                {/* Profile 2 */}

                {/* Profile 2 */}
                </View>

                <FlatList
                    contentContainerStyle={styles.list}
                    data={data}
                    renderItem={renderItem}
                />
            </ScrollView>
            :
            arrayholder === 'DATA' ?
            <View style={styles.item2}>
            <Text style={{fontSize:18,paddingRight:10,marginVertical:10}}>Maksud dan Tujuan</Text>
              <HTML source={{ html: data.maksud_tujuan }}  />
            </View>
            :
            <View style={{flex:1,flexDirection:'column',justifyContent: 'space-between',backgroundColor:'white',paddingBottom:20}}>
                 <FlatList
                    data={data}
                    renderItem={renderItem2}
                    keyExtractor={item => item.id.toString()}
                />
            </View>
          
        )}
        
        

        </View>
        
        </View>
    )
}

export default PengurusPusat

const styles = StyleSheet.create({
    titleBlue:{
        fontSize:20,
        color:'#2C5BA4'
    },
    wrapProfile : {
        padding:5,
        alignItems:'center'
    },
    name :{
        fontSize:16
    },
    position :{
        fontSize:12,
        textAlign:'center',
        width:100
    },
    list: {
        justifyContent: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
        backgroundColor: 'white'
      },
      item1: {
        backgroundColor: 'white',
        paddingVertical: 20,
        paddingHorizontal:10,
        flexDirection : 'row',
        width:'100%',
        height: 86,
        justifyContent: 'space-between',
        marginBottom:10
      },
    //   renderitem2
      item: {
        paddingHorizontal: 5,
        flexDirection : 'row',
        flex: 1
      },
      title1: {
        fontSize: 16,
        paddingVertical:10
        // fontFamily:'Montserrat'
      },
      title: {
        color:'grey',
        fontSize:12
      },
      containerflatList : {
        flexDirection : 'column',
        flex: 1,
        margin:5
      },
      detail: {
        fontSize: 12,
        color: '#669F16'
      },
      title1: {
        fontSize: 18,
        marginTop:10,
        marginBottom:5
      },
      title2: {
        color:'#669F16',
        marginBottom:15
      },
      title3: {
        color:'#919191',
      },
      item2: {
        backgroundColor: 'white',
        padding:20,
        flexDirection : 'column',
        width:'100%',
        justifyContent: 'space-between',
      },
})
