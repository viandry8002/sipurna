import React,{useState} from 'react';
import { StyleSheet, Text, View,SafeAreaView,Image } from 'react-native';
import HTML from "react-native-render-html";

const DtlInfoKegiatan = ({route}) => {
    const [data, setData] = useState(route.params)
    return (
        <SafeAreaView style={{flex:1}}>
        <View style={{padding: 20,flexDirection:'column',backgroundColor: 'white',width: '100%',height:'100%',marginBottom:10}}>

        <Image 
        source={data.file === '' ? require('../../Assets/drawable-xhdpi/img-notfound.png')
        : {uri: data.file}}
        style={{
            width: '100%',
            height: 160,
            borderRadius:10
        }}/>
            
        <View style={{marginTop:20}}>
        <Text style={{fontSize: 16,width:222,height:39,marginVertical:15}}>{data.title}</Text>
        <HTML source={{ html: data.description === '' ? '-' : data.description}}  />
        </View>

        </View>
        </SafeAreaView>
    )
}

export default DtlInfoKegiatan
