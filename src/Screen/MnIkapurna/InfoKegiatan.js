import React, { useEffect, useState } from 'react';
import { FlatList, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import api from '../../Api';
import HTML from "react-native-render-html";

const InfoKegiatan = ({navigation}) => {
      const [data, setData] = useState([])

      useEffect(() => {
          async function getToken() {
              try{
                  const token = await AsyncStorage.getItem("api_token")
                  return getVenue(token)
              }catch(err){
                  console.log(err)
              }
          }
          getToken()
      },[])

      const getVenue = (token) =>{
          Axios.get(`${api}/api/activity/ikapurna`,{
              timeout : 20000,
              headers : {
                  'Authorization' : 'Bearer ' + token,
                  'Accept' : 'application/json'
              }
          }).then((res) =>{
              setData(res.data.data)
              // console.log(res.data.data)
          }).catch((err) =>{
            console.log(err.response.data)
            if(err.response.data === 'Unauthorized.'){
                navigation.navigate('OnKick')
              }
          })
      }

    const DtlInfoKegiatan = (route) => {
        navigation.navigate('DtlInfoKegiatan',route)
      }

    const Item = ({ data}) => (
        <View style={styles.item}>
        <Image source={data.file === '' ? require('../../Assets/drawable-xhdpi/img-notfound.png')
                : {uri: data.file}} style={{width: 140,height: 140,borderRadius:10, marginHorizontal:10,marginTop:10}} />
        <View style={styles.containerflatList}>
        <Text style={styles.title1}>{data.title}</Text>
        <HTML source={{ html: data.description === '' ? '-' : data.description }} renderers={{
          p: (_, children) => <Text numberOfLines={1}>{children}</Text>,
        }} />
        <TouchableOpacity style={{alignItems:'center',marginVertical:10}} onPress={() => DtlInfoKegiatan(data)}>
          <Text style={styles.detail}>Detail</Text>
        </TouchableOpacity>
        </View>
        </View>
    );

    const renderItem = ({ item }) => (
        <Item data={item} />
      );

    return (
        <SafeAreaView style={styles.container}>
         <View style={{flex:1,flexDirection:'column',justifyContent: 'space-between'}}>

         <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={item => item.id.toString()}
          />

         </View>
     </SafeAreaView>
    )
}

export default InfoKegiatan

const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
      item: {
        backgroundColor: 'white',
        padding: 10,
        marginVertical: 3,
        flexDirection : 'row',
        flex: 1
      },
      title1: {
        fontSize: 16,
        paddingVertical:10
        // fontFamily:'Montserrat'
      },
      containerflatList : {
        flexDirection : 'column',
        flex: 1,
        margin:5
      },
      detail: {
        fontSize: 12,
        color: '#669F16'
      },
})
