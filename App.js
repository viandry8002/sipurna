import React from 'react';
import Routes from './src/Screen/Routes';
import firebase from '@react-native-firebase/app';
import GantiPass from './src/Screen/GantiPass';

var firebaseConfig = {
  apiKey: "AIzaSyCbR4XGtAatmhqmaCT1qUpGfHo81WLbIzY",
  authDomain: "sipurnabtn.firebaseapp.com",
  projectId: "sipurnabtn",
  storageBucket: "sipurnabtn.appspot.com",
  messagingSenderId: "301691373915",
  appId: "1:301691373915:web:9535f7ebfdc2417509b27e",
  measurementId: "G-CZX62NSEZL"
};

// Inisialisasi firebase
if(!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

const App = () => {

  return (
    <Routes/>
  );
};

export default App;
